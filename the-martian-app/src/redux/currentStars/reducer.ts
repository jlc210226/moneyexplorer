import {ICurrentStarsState} from './state';
import {ICurrentStarsActions} from './action'
const initCurrentStarsState: ICurrentStarsState = {
    currentStars: 0,
}

export const currentStarsReducer = (state: ICurrentStarsState = initCurrentStarsState, action : ICurrentStarsActions) => {
    switch (action.type) {
        case "@@CURRENTSTAR_LOAD":
            return {
                ...state,
                currentStars: action.currentStars
            }
        case "@@CURRENTSTAR_INCREMENT":
            return {
                ...state,
                currentStars: state.currentStars + 1,
            }
        default:
            return state;
    }
}
