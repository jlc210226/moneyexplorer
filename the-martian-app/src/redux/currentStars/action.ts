import { Dispatch } from "redux";
import { RootState } from "../../store";


const LOADCURRENTSTAR = "@@CURRENTSTAR_LOAD" as const;
const INCREMENT = "@@CURRENTSTAR_INCREMENT" as const;

export function loadCurrentStar(currentStars: number) {
    return {
        type: LOADCURRENTSTAR,
        currentStars: currentStars
    }
}

export function currentStarIncrement() {
    return {
        type: INCREMENT,
    }
}


export function fetchCurrentStar() {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            console.log("fetchCurrentStar1")
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/loadCurrentStar`, {
                    headers: {
                        Authorization: 'Bearer ' + getState().auth.token
                    }
                })
            const data = await res.json();
            console.log(data)
            dispatch(loadCurrentStar(data.star))
        } catch (err) {
            console.log(err)
        }
    }
}




type ActionCreators = typeof currentStarIncrement | typeof loadCurrentStar;
export type ICurrentStarsActions = ReturnType<ActionCreators>