import { RootState } from './../../store';
import { ThunkDispatch } from "../../store";
import { push } from 'connected-react-router';

// Action Creator

declare let navigator: any;


export function loginSuccess(userId: number, token: string) {
    return {
        type: '@@auth/LOGIN_SUCCESS' as const,
        userId: userId,
        token: token
    }
}

export function loginFailure() {
    return {
        type: '@@auth/LOGIN_FAILURE' as const
    }
}

export function logoutSuccess() {
    return {
        type: '@@auth/LOGOUT_SUCCESS' as const
    }
}

export function registrationIsLoading(isLoading: boolean) {
    return {
        type: '@@auth/REGISTRATION_SEND_EMAIL_LOADING' as const,
        isLoading
    }
}

export function sendForgetPasswordEmailIsLoading(isLoading: boolean){
    return {
        type: '@@auth/SEND_FORGET_PASSWORD_EMAIL_LAODING' as const,
        isLoading
    }
}

export type AuthActions = ReturnType<typeof loginSuccess> |
    ReturnType<typeof loginFailure> |
    ReturnType<typeof logoutSuccess> |
    ReturnType<typeof registrationIsLoading> |
    ReturnType<typeof sendForgetPasswordEmailIsLoading>

// Thunk Action

export function login(loginName: string, password: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try{
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    loginName, password
                })
            })
    
            const data = await res.json()
    
            if (data.message === "invalid login name or password") {
                alert("登入名稱或密碼不正確。")
                dispatch(loginFailure());
                return;
            }
    
            if (data.message === "please confirm email before login") {
                const email = data.email;
                alert("請先登入電郵認證。");
                dispatch(push(`/verify?email=${email}`));
                return;
            }
    
            if (data.token) {
                localStorage.setItem('token', data.token);
                dispatch(loginSuccess(data.id, data.token));
    
                if (getState().router.location.pathname === '/login' || getState().router.location.pathname === '/registration') {
                    dispatch(push('/base'))
                }
            } else {
                dispatch(loginFailure());
            }
        }catch(err){
            alert(err)
        }

    }
}

export function checkLogin() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const token = localStorage.getItem('token');

        if (token == null) {
            dispatch(logoutSuccess());
            return;
        }

        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/currentUser`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const result = await res.json();

        if (result.id) {
            dispatch(loginSuccess(result.id, token))
        } else {
            dispatch(logout());
            dispatch(push('/homepage'));
        }
    }
}

export function register(loginName: string, password: string, nickname: string, gender: string, birthday: Date, email: string, pin: number, iconId: number) {
    return async (dispatch: ThunkDispatch) => {

        try {
           
            const formObject = {
                loginName: loginName,
                password: password,
                nickname: nickname,
                gender: gender,
                birthday: birthday,
                email: email,
                pin: pin,
                iconId: iconId
            }

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/register`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formObject)
            })

            const result = await res.json();

            if (result.message === "Login Name Existed") {
                alert("註冊名稱已存在。")
                return;
            }

            if (result.message === "Email Existed") {
                alert("此電郵已使用。")
                return;
            }

            if (result.message === "Register Fail") {
                alert("註冊不成功")
                return;
            } 

            if (result.message === "internal server error") {
                alert("註冊不成功")
                return;
            }       

            if (!result) {
                alert("註冊不成功")
                dispatch(loginFailure());
                return;
            }

            if (result.message === "success") {
               dispatch(sendConfirmationEmail(email));
               return;
            }

        } catch (err) {
            console.error(err);
        } 
    }
}

export function sendConfirmationEmail(email: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(registrationIsLoading(true))

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/sendConfirmationEmail`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({email})
            })

            const result = await res.json();

            if (result.status === "pending") {
                dispatch(push(`/verify/${email}`));
            };

        } catch (err) {
            console.error(err);
        } finally {
            dispatch(registrationIsLoading(false))
        }
    }
}

export function verifyEmail(email: string, code: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(registrationIsLoading(true));

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/verifyEmail`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({email, code})
            })

            const result = await res.json();

            if (result.message === "Account activated") {
                alert("你已經成功驗證，請前往登入頁面。");
                dispatch(push('/login'))
            }

            if (result.message === "User not exist") {
                alert("此用戶不存在。");
                dispatch(push('/homepage'))
            }

            if (result.status === "pending") {
                alert("驗證碼不正確。")
                return;
            };

            if (result.message === "internal server error") {
                alert("驗證碼不正確。");
                return
            }

            if (result.status === "approved") {
               dispatch(activateAccount(email));
            };
            
        } catch (err) {
            console.error(err);
        } finally {
            dispatch(registrationIsLoading(false));
        }
    }
}


export function activateAccount(email: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/activateAccount`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({email})
            })

            const result = await res.json();

            if (result.message === "account activated") {
                alert("驗證成功，可馬上登入");
                dispatch(push('/login'));
            };

        } catch (err) {
            console.error(err);
        }
    }
}

export function logout() {
    return (dispatch: ThunkDispatch, getState: () => RootState) => {
        localStorage.removeItem('token');
        dispatch(logoutSuccess());
        dispatch(push('/homepage'));
    }
}

export function forgetPassword(email:string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {

            const res = await fetch (`${process.env.REACT_APP_API_SERVER}/forgetPasswordEmail`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({email})
            })

            const result = await res.json();

            if (result.message === "user not exist") {
                alert("此用戶不存在。")
                return;
            }

            if (result.message === "internal server error") {
                alert("發送郵件不成功。")
                return;
            }

            if (result.message === "success") {
                const emailURL = result.emailURL;
                const nickname = result.nickname;
                dispatch(sendForgetPasswordEmail(email, emailURL, nickname));
            }

        } catch (err) {
            console.error(err)
        } 
    }
}

export function sendForgetPasswordEmail(email: string, emailURL: string, nickname: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {

            dispatch(sendForgetPasswordEmailIsLoading(true))

            const res = await fetch (`${process.env.REACT_APP_API_SERVER}/sendForgetPasswordEmail`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({email, emailURL, nickname})
            })

            const result = await res.json();

            if (result.message === "email sent successfully") {
                alert("電郵已經寄出，請查看。")
                dispatch(push('/login'))
                return;
            }

            if (result.message === "error sending email") {
                alert("電郵寄出失敗。") 
                dispatch(push('/login'))
                return;
            }

        } catch (err) {
            console.error(err)
        } finally {
            dispatch(sendForgetPasswordEmailIsLoading(false))
        }
    }
}

export function resetPassword(password: string, userId: string, token: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {

            const res = await fetch (`${process.env.REACT_APP_API_SERVER}/resetPassword`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({password, userId, token})
            })

            const result = await res.json();

            if (result.id) {
                alert("密碼已成功更新，請前往登入。");
                dispatch(push('/login'));
                return;
            }

            if (result.message === "error resetting password") {
                alert("密碼未能更新。");
                return;
            }

        } catch (err) {
            console.error(err)
        }
    }
}