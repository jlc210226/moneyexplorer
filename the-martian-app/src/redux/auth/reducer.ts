import { AuthActions } from "./action"

export interface AuthState {
    isAuthenticated: boolean | null,
    userId: number | null,
    token: string,
    isLoading: boolean
}

const initialState: AuthState = {
    isAuthenticated: null,
    userId: null,
    token: '',
    isLoading: false
}

export const authReducer = (state: AuthState = initialState, action: AuthActions): AuthState => {
    switch (action.type) {
        case "@@auth/LOGIN_SUCCESS": 
            return {
                ...state,
                isAuthenticated: true,
                userId: action.userId,
                token: action.token
            }

        case "@@auth/LOGIN_FAILURE": 
            return {
                ...state,
                isAuthenticated: false,
                userId: null,
                token: ''
            }

        case "@@auth/REGISTRATION_SEND_EMAIL_LOADING":
            const newIsLoading = action.isLoading;
            return {
                ...state,
                isLoading: newIsLoading
            }

        case "@@auth/SEND_FORGET_PASSWORD_EMAIL_LAODING":
            const sendEmailIsLoading = action.isLoading;
            return {
                ...state,
                isLoading: sendEmailIsLoading
            }
  
        // case "@@auth/LOGOUT_SUCCESS": {
        //     return {
        //         ...state,
        //         isAuthenticated: false,
        //         userId: null,
        //         token: ''
        //     }
        // };

        case "@@auth/LOGOUT_SUCCESS": 
            return {
                isAuthenticated: false,
                userId: null,
                token: '',
                isLoading: false
            }

        default:
            return state
    }
}