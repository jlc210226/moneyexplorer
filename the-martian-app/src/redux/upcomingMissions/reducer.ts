import {IUpcomingMissionsState} from './state';
import {IUpcomingMissionsActions} from './action'


const initUpcomingMissions: IUpcomingMissionsState = {
    // upComingMissions:[
    //         {
    //             id: 0,
    //             mission_image: "/img/astronaut.png",
    //             mission_name: "收拾房間",
    //             reward_stars: 3, 
    //             mission_exp_date: 813 ,
    //             mission_status: "in progress"
    //         },
    //         {
    //             id: 1,
    //             mission_image: "/img/astronaut.png",
    //             mission_name: "溫習",
    //             reward_stars: 10, 
    //             mission_exp_date: 1234,
    //             mission_status: "in progress"
    //         },
    //         {
    //             id: 2,
    //             mission_image: "/img/astronaut.png",
    //             mission_name: "溫習",
    //             reward_stars: 10, 
    //             mission_exp_date: 1234,
    //             mission_status: "in progress"
    //         },
    //     ]
        upComingMissions:[],
        isLoading: false
}

export const upComingMissionsReducer = (state: IUpcomingMissionsState = initUpcomingMissions, action: IUpcomingMissionsActions)=> {
    switch (action.type) {
        case '@@mission/UPCOMING_MISSION_LOADING':
            const newIsLoading = action.isLoading
            console.log(newIsLoading)
            return {
                ...state,
                isLoading: newIsLoading
            }
        case "@@UPCOMINGMISSIONS_LOAD" : {
            return {
                ...state,
                upComingMissions: action.upcomingMissions
            }
        }
        default:
            return state;
    }
}