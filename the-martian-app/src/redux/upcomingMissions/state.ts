export interface IUpcomingMissionsState {
    upComingMissions: {
        id: number
        mission_image: string,
        mission_name: string,
        reward_stars: number,
        mission_exp_date: string,
        mission_status: string,
        expired: boolean,
    }[],
    isLoading: boolean
}