import { push } from "connected-react-router";
import { Dispatch } from "redux";
import { RootState, ThunkDispatch } from "../../store";
import { fetchCurrentStar } from "../currentStars/action";



const LOADUPCOMINGMISSIONS = "@@UPCOMINGMISSIONS_LOAD" as const;
const CREATE = "@@UPCOMINGMISSIONS_CREATE" as const;
const UPCOMINGMISSIONLOADER = '@@mission/UPCOMING_MISSION_LOADING' as const

export function upcomingMissionLoading(isLoading: boolean) {
    return {
        type: UPCOMINGMISSIONLOADER,
        isLoading
    }
}

export function loadUpcomingMissions(upcomingMissions: any) {
    return {
        type: LOADUPCOMINGMISSIONS,
        upcomingMissions
    }
}

export function createUpcomingMissions(upcomingMissions: object[]) {
    return {
        type: CREATE,
        upcomingMissions
    }
}


export function fetchUpcomingMissions() {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            dispatch(upcomingMissionLoading(true))
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/getUpcomingMissions`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                },
            })
            const data = await res.json();
            const dateNow = new Date()
            const formattedDateNow = (dateNow.getFullYear()) + "-" + ((dateNow).getMonth() + 1) + "-" + (dateNow).getDate()
            // console.log("date:", dateNow)
            const upComingMissions = data.map((i: any) => {
                //    let formattedDate = (i.mission_exp_date).substring(0, 10)
                const temp = Date.parse(i.mission_exp_date);
                const dateToFormat = new Date(temp);
                const formattedDate = (dateToFormat.getFullYear()) + "-" + ((dateToFormat).getMonth() + 1) + "-" + (dateToFormat).getDate()
                console.log("formattedDateNow", formattedDateNow)
                console.log("formattedDate", formattedDate)
                if (formattedDateNow === formattedDate) {
                    return { id: i.assigned_mission_id, mission_image: i.mission_image, mission_name: i.mission_name, reward_stars: i.reward_stars, mission_exp_date: formattedDate, mission_status: i.mission_status, expired: false }
                }
                if (dateNow > dateToFormat) {
                    return { id: i.assigned_mission_id, mission_image: i.mission_image, mission_name: i.mission_name, reward_stars: i.reward_stars, mission_exp_date: formattedDate, mission_status: i.mission_status, expired: true }
                } else {
                    return { id: i.assigned_mission_id, mission_image: i.mission_image, mission_name: i.mission_name, reward_stars: i.reward_stars, mission_exp_date: formattedDate, mission_status: i.mission_status, expired: false }
                }
            })
            console.log("missions", upComingMissions)
            dispatch(loadUpcomingMissions(upComingMissions))
            dispatch(upcomingMissionLoading(false))
        } catch (err) {
            console.log(err)
        }
    }
}

export function changeMissionStatus(reward_stars: number, changeStatusString: string, missionId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            console.log("changeMissionStatus")
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/changeMissionStatus`, {
                method: 'POST',
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    mission_id: missionId,
                    changeStatus: changeStatusString,
                    reward_stars: reward_stars
                })
            })
            const data = await res.json();

            if (data.message === "success") {
                await dispatch(fetchCurrentStar())
                await dispatch(fetchUpcomingMissions())
            }
        } catch (err) {
            console.log(err)
        }
    }
}

export function addNewUpcomingMission(mission_id: number, mission_name: string, exp_date: Date, reward_star: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/addNewUpcomingMission`, {
                method: 'POST',
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    mission_template_id: mission_id,
                    mission_exp_date: exp_date,
                    reward_star: reward_star,
                    mission_status: "in progress"
                })
            })
            const result = await res.json()
            console.log(result)
            if (result.message === "success") {
                alert("成功新增任務")
                await dispatch(fetchUpcomingMissions())
                dispatch(push('/mission'))
            }
        } catch (err) {
            console.log(err)
        }
    }
}

export function addCustomizedMission(mission_name: string, exp_date: Date, reward_star: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/addCustomizedMissions`, {
                method: 'POST',
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    mission_name: mission_name,
                    mission_exp_date: exp_date,
                    reward_star: reward_star,
                    mission_status: "in progress"
                })
            })
            const result = await res.json()
            console.log(result)
            if (result.message === "success") {
                alert("成功新增自訂任務")
                await dispatch(fetchUpcomingMissions())
            }

        } catch (err) {
            console.log(err)
        }
    }
}

type ActionCreators = typeof loadUpcomingMissions | typeof createUpcomingMissions | typeof upcomingMissionLoading;
export type IUpcomingMissionsActions = ReturnType<ActionCreators>