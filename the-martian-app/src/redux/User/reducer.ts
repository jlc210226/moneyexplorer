import { BaseActions } from './action';

export interface UserInfo {
    id: number | null;
    nickname: string;
    gender: string;
    date_of_birth: string;
    email: string;
    user_id: number | null;
    avatar_id: number | null;
    current_star: number | null;
    total_star: number | null;
    total_saving: number | null;
    created_at: Date;
    updated_at: Date;
}

export interface UserInfoState {
    userInfo: UserInfo;
}

const initialState: UserInfoState = {
    userInfo: {
        id: null,
        nickname: "",
        gender: "",
        date_of_birth: "",
        email: "",
        user_id: null,
        avatar_id: null,
        current_star: null,
        total_star: null,
        total_saving: null,
        created_at: new Date(),
        updated_at: new Date(),
    }
}

export const userInfoReducer = (state: UserInfoState = initialState, action: BaseActions): UserInfoState => {

    switch (action.type) {

        case "@@base/GET_USER_INFO_FOR_CARD": {
            return {
                ...state,
                userInfo: action.userInfo
            }
        }

        case "@@base/CLEAR_USER_INFO_STATE":
            return {
                userInfo: {
                    id: null,
                    nickname: "",
                    gender: "",
                    date_of_birth: "",
                    email: "",
                    user_id: null,
                    avatar_id: null,
                    current_star: null,
                    total_star: null,
                    total_saving: null,
                    created_at: new Date(),
                    updated_at: new Date(),
                }
            }

        default:
            return state
    }
}