import { push } from "connected-react-router";
import { RootState, ThunkDispatch } from "../../store"
import { UserInfo } from './reducer';
import { Dispatch } from 'redux';

// Action Creator

export function loadUserInfo(userInfo: UserInfo) {
    return {
        type: "@@base/GET_USER_INFO_FOR_CARD" as const,
        userInfo: userInfo
    }
}

export function clearUserInfoState() {
    return {
        type: "@@base/CLEAR_USER_INFO_STATE" as const
    }
}

export type BaseActions = ReturnType<typeof loadUserInfo> |
                          ReturnType<typeof clearUserInfoState>



// Thunk Action

export function getUserInfo() {
    return async (dispatch: Dispatch, getState:() => RootState) => {
        try {

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/getUserInfo`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                  } 
            });

            const data = await res.json();

            if (!data) {
                console.log("Invalid user")
            }

            dispatch(loadUserInfo(data));

        } catch (err) {
            console.error(err);
        }
    }
}

export function editProfile(nickname: string, gender: string, birthday: Date, iconId: number) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const formObject = {
                nickname: nickname,
                gender: gender,
                birthday: birthday,
                iconId: iconId
            }

            console.log(formObject)
            
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/editProfile`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getState().auth.token
                },
                body: JSON.stringify(formObject)
            })

            const result = await res.json();

            if (result.message === "update fail") {
                alert("用戶資料修改不成功。");
                return
            }

            if (result.message === "internal server error"){
                alert("用戶資料修改不成功 internal server error。");
                return
            }

            if (result.message === "success") {
                alert("用戶資料修改成功。");
                dispatch(push('/base'));
            }         
            
        } catch (err) {
            console.error(err);
        }
    }
}

export function editPassword(oldPassword:string, password: string) {
    return async(dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const formObject = {
                oldPassword: oldPassword,
                password: password
            }

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/editPassword`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getState().auth.token
                },
                body: JSON.stringify(formObject)
            })

            const result = await res.json();

            if (result.message === "invalid login name or password") {
                alert("舊密碼不正確");
            }

            if (result.message === "update fail"){
                alert("修改不成功。");
            }

            if (result.message === "internal server error"){
                alert("修改不成功。");
            }

            if (result.message === "success") {
                alert("更新成功。");
                dispatch(push('/base'));
            }   

        } catch (err) {
            console.error(err);
        }
    }
}

export function editPin(oldPassword:string, password: string) {
    return async(dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            
            const formObject = {
                oldPassword: oldPassword,
                password: password
            }

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/editPin`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getState().auth.token
                },
                body: JSON.stringify(formObject)
            })

            const result = await res.json();

            if (result.message === "invalid pin") {
                alert("舊密碼不正確");
            }

            if (result.message === "update fail"){
                alert("不成功。");
            }

            if (result.message === "internal server error"){
                alert("不成功。");
            }

            if (result.message === "success") {
                alert("更新成功。");
                dispatch(push('/base'));
            }   

        } catch (err) {
            console.error(err);
        }
    }
}