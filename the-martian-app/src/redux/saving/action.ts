import { push } from "connected-react-router";
import { Dispatch } from "redux";
import { RootState, ThunkDispatch } from "../../store";

// action creator

export function markSavingLoading(isLoading: boolean) {
  return {
      type: '@@saving/SAVING_LOADING' as const,
      isLoading
  }
}

export function clearSavingItemData() {
  return {
      type: '@@saving/CLEAR_SAVING_ITEM_DATA' as const,
  }
}

export function removeSaving(id: number, category: any) {
    return {
      type: '@@saving/REMOVE_SAVING' as const,
      id: id,
      category: category,
    }
  }

export function addSaving(id: number, category: string, newDate: any, newItem:string, newAmount: number) {
    return {
        type: '@@saving/ADD_SAVING' as const,
        id: id,
        category: category,
        date: newDate,
        item: newItem,
        amount: newAmount
    }
}

// allIncomeItem: object[]
export function loadAllFinanceItem(allFinanceItem: any) {
  return {
      type: '@@saving/LOAD_ALL_INCOME_ITEM' as const,
      allFinanceItem,
  }
}

// fetch total amount
export function loadTotalIncomeByMonth(totalIncome: any) {
  return {
      type: '@@saving/LOAD_TOTAL_INCOME_BY_MONTH' as const,
      totalIncome,
  }
}

export function loadTotalExpenseByMonth(totalExpense: any) {
  return {
      type: '@@saving/LOAD_TOTAL_EXPENSE_BY_MONTH' as const,
      totalExpense,
  }
}

export function loadTotalSavingByMonth(totalSaving: any) {
  return {
      type: '@@saving/LOAD_TOTAL_SAVING_BY_MONTH' as const,
      totalSaving,
  }
}

export type SavingActions = ReturnType<typeof removeSaving> |
                            ReturnType<typeof addSaving> |
                            ReturnType<typeof loadTotalIncomeByMonth> |
                            ReturnType<typeof loadTotalExpenseByMonth> |
                            ReturnType<typeof loadTotalSavingByMonth> |
                            ReturnType<typeof loadAllFinanceItem> |
                            ReturnType<typeof markSavingLoading> |
                            ReturnType<typeof clearSavingItemData>



// thunk action creator
export function createSaving(id: number, category: string, newDate: any, newItem:string, newAmount: number) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {

      // dispatch(addSaving(id, category, newDate, newItem, newAmount))

      if (!category || !newDate || !newAmount || !newItem) {
        alert('請填妥所有欄目');
        return;
      }

      if (category === 'income') {
        console.log('[info] fetch income')
        await fetch(`${process.env.REACT_APP_API_SERVER}/createincome`, {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + getState().auth.token,
            "Content-type": "application/json; charset=utf-8",
        },
          body: JSON.stringify({
            income_date: newDate, 
            income_category: newItem, 
            income_amount: newAmount
          })
        })
      }

      if (category === 'expense') {

        await fetch(`${process.env.REACT_APP_API_SERVER}/createexpense`, {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + getState().auth.token,
            "Content-type": "application/json; charset=utf-8",
        },
          body: JSON.stringify({
            expense_date: newDate, 
            expense_category: newItem, 
            expense_amount: newAmount
          })
        })
      }

      if (category === 'saving') {

         await fetch(`${process.env.REACT_APP_API_SERVER}/createsaving`, {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + getState().auth.token,
            "Content-type": "application/json; charset=utf-8",
        },
          body: JSON.stringify({
            saving_date: newDate, 
            saving_amount: newAmount
          })
        })
      }

      alert('成功新增！');
      dispatch(push('/saving'))

    } catch (e) {
      console.error(e);
    }
  }
}


export function fetchAllFinanceItem(order: boolean) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {

      dispatch(markSavingLoading(true))

      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/allfinanceitem`, {
        headers: {
            Authorization: 'Bearer ' + getState().auth.token,
        },
    })
      const json = await res.json();
      const incomeArr = (json.income).map((i: any) => ({ ...i, category: 'income'}))
      const expenseArr = (json.expense).map((e: any) => ({ ...e, category: 'expense'}))
      const savingArr = (json.saving).map((s: any) => ({ ...s, category: 'saving', item: '儲蓄'}))
      const combineFinanceArr = incomeArr.concat(expenseArr).concat(savingArr)

      if (!json) {
        console.log('invalid user')
      }

      if (order === true) {
        var compareDateAsc = function(a: any, b: any) {  
          var aDate = new Date(a.date).getTime();  
          var bDate = new Date(b.date).getTime();  
          return aDate > bDate ? 1 : -1;    
        }  
        var sortedArrayAsc = combineFinanceArr.sort(compareDateAsc);
        dispatch(loadAllFinanceItem(sortedArrayAsc))
      }

      else if (order === false) {
        var compareDatedsc = function(a: any, b: any) {  
          var aDate = new Date(a.date).getTime();  
          var bDate = new Date(b.date).getTime();  
          return aDate > bDate ? -1 : 1;    
        }  
        var sortedArrayDsc = combineFinanceArr.sort(compareDatedsc);
        dispatch(loadAllFinanceItem(sortedArrayDsc))
      }
       
      // const combineFinanceDateArr = combineFinanceArr.map((c:any) => Date.parse(c.date)).sort((a:any, b:any) => b - a)
      // console.log(combineFinanceDateArr.sort((a:any, b:any) => b.date - a.date))

    } 
    
    catch (e) {
      console.error(e);
    } 
    
    finally {
      dispatch(markSavingLoading(false))
    }
  }
}

// export function fetchAllExpenseItem() {
//   return async (dispatch: ThunkDispatch) => {
//     try {
//       const res = await fetch(`${process.env.REACT_APP_API_SERVER}/allexpenseitem`)
//       const json = await res.json();
//       dispatch(loadAllIncomeItem(json))
//     } catch (e) {
//       console.error(e);
//     }
//   }
// }

// fetch total amount
export function fetchTotalIncomeByMonth() {
  return async (dispatch: Dispatch, getState: () => RootState) => {

    try {
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/totalincome`, {
        headers: {
            Authorization: 'Bearer ' + getState().auth.token,
        },
    })
      const json = await res.json();
      // console.log(json['getTotalIncome'])
      dispatch(loadTotalIncomeByMonth(json['getTotalIncome']))
    } 
    
    catch (e) {
      console.error(e);
    }

  }
}

export function fetchTotalExpenseByMonth() {
  return async (dispatch: Dispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/totalexpense`, {
        headers: {
            Authorization: 'Bearer ' + getState().auth.token,
        },
    })
      const json = await res.json();
      dispatch(loadTotalExpenseByMonth(json))
    } catch (e) {
      console.error(e);
    }
  }
}

export function fetchTotalSavingByMonth() {
  return async (dispatch: Dispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/totalsaving`, {
        headers: {
            Authorization: 'Bearer ' + getState().auth.token,
        },
    })
      const json = await res.json();
      // console.log(json)
      dispatch(loadTotalSavingByMonth(json))
    } catch (e) {
      console.error(e);
    }
  }
}

export function deleteIncome(id: number, category: any) {
  return async (dispatch: Dispatch, getState: () => RootState) => {
    try {

      if (category === 'income') {
     await fetch(`${process.env.REACT_APP_API_SERVER}/deleteincome/${id}`, {
        method: 'DELETE',
        headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                },
        });
      }
    
      if (category === 'expense') {
       await fetch(`${process.env.REACT_APP_API_SERVER}/deleteexpense/${id}`, {
          method: 'DELETE',
          headers: {
            Authorization: 'Bearer ' + getState().auth.token,
        },
          });
        }

      if (category === 'saving') {
        await fetch(`${process.env.REACT_APP_API_SERVER}/deletesaving/${id}`, {
            method: 'DELETE',
            headers: {
              Authorization: 'Bearer ' + getState().auth.token,
          },
            });
        }

      dispatch(removeSaving(id, category))
      // alert('成功刪除！');

    } catch (e) {
      console.error(e);
    }
  }
}


