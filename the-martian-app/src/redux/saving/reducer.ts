import { SavingActions } from "./action"

export interface SavingState {
  saving: {
    id: number,
    category: string,
    date: any,
    item: string,
    amount: number,
  } [],
  isLoading?: boolean;
}

const initialState: SavingState = {
  saving: [
  //   {
  //   'id': 0,
  //   'date': '2021-04-07',
  //   'item': '食物',
  //   'amount': 500
  // },
  // {
  //   'id': 1,
  //   'date': '2021-04-04',
  //   'item': '娛樂',
  //   'amount': 200
  // },
  // {
  //   'id': 2,
  //   'date': '2021-04-02',
  //   'item': '娛樂',
  //   'amount': 188
  // },
  // {
  //   'id': 3,
  //   'date': '2021-03-29',
  //   'item': '交通',
  //   'amount': 50
  // }
],
  isLoading: false
}

export const savingReducer = (state: SavingState = initialState, action: SavingActions) => {

  if (action.type === '@@saving/SAVING_LOADING') {

    const loadingStatus = action.isLoading

    return {
      ...state,
      isLoading: loadingStatus,
    }
  }

  if (action.type === '@@saving/CLEAR_SAVING_ITEM_DATA') {

    return {
      ...state,
      saving: [],
    }
  }

  if (action.type === '@@saving/LOAD_ALL_INCOME_ITEM') {

    const fetchCombineItem = action.allFinanceItem

    return {
      ...state,
      saving: fetchCombineItem,
    }
  }

  if (action.type === '@@saving/REMOVE_SAVING') {

    let filterItem: any;

    let newSavingList = state.saving.filter((item) => item.id === action.id)

    if (newSavingList.length > 1) {
      filterItem = newSavingList.filter((item) => item.category === action.category)
    } else if (newSavingList.length === 1) {
      filterItem = newSavingList
    }
    
    let newSavingListFinal = state.saving.filter((item) => item !== filterItem[0])

    return {
      // ...state,
      saving: newSavingListFinal
    }
  }
  // else if (action.type === '@@saving/ADD_SAVING') {
  //   const newSaving = {
  //       id: action.id,
  //       category: action.category,
  //       date: action.date,
  //       item: action.item,
  //       amount: action.amount
  //   }
  //   return {
  //     // ...state,
  //     saving: [newSaving, ...state.saving],
  //     // item: state.saving.item.concat(action.item),
  //     // amount: state.saving.amount.concat(action.amount)
  //   }
  // }
  return state
}

export interface TotalIncomeState {
  totalIncome: {
    date_trunc?: any,
    totalIncome?: any,
  } []
}

const initialTotalIncomeState: TotalIncomeState = {
  totalIncome: [
    {
    // 'date_trunc': '2021-03-01',
    'totalIncome': '0',
    }
]
}

export const incomeTotalReducer = (state: TotalIncomeState = initialTotalIncomeState, action: SavingActions) => {
  if (action.type === '@@saving/LOAD_TOTAL_INCOME_BY_MONTH') {
    return {
      ...state,
      totalIncome: action.totalIncome
    }
  }
  return state
}

///

export interface TotalExpenseState {
  totalExpense: {
    date_trunc?: any,
    totalExpense?: any,
  } []
}

const initialTotalExpenseState: TotalExpenseState = {
  totalExpense: [
    {
      // 'date_trunc': '2021-03-01',
      'totalExpense': '0',
      }
]
}

export const expenseTotalReducer = (state: TotalExpenseState = initialTotalExpenseState, action: SavingActions) => {
  if (action.type === '@@saving/LOAD_TOTAL_EXPENSE_BY_MONTH') {
    return {
      ...state,
      totalExpense: action.totalExpense
    }
  }
  return state
}

///

export interface TotalSavingState {
  totalSaving: {
    date_trunc?: any,
    totalSaving?: any,
  } []
}

const initialTotalSavingState: TotalSavingState = {
  totalSaving: [
    // {
    //   'date_trunc': '2020-12-31T16:00:00.000Z',
    //   'totalSaving': '0',
    // },
    // {
    //   'date_trunc': '2021-01-31T16:00:00.000Z',
    //   'totalSaving': '0',
    // },
    // {
    //   'date_trunc': '2021-02-28T16:00:00.000Z',
    //   'totalSaving': '0',
    // },
]
}

export const savingTotalReducer = (state: TotalSavingState = initialTotalSavingState, action: SavingActions) => {
  if (action.type === '@@saving/LOAD_TOTAL_SAVING_BY_MONTH') {
    // console.log(action.totalSaving)
    return {
      ...state,
      totalSaving: action.totalSaving
    }
  }

  return state
}