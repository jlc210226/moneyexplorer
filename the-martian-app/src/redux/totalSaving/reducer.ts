export interface TotalSavingState {
  totalSaving: {
    totalSaving: number,
    totalIncome: number,
    totalExpense: number,
  }
}

const initialTotalState: TotalSavingState = {
  totalSaving: {
    'totalSaving': 0,
    'totalIncome': 0,
    'totalExpense': 0
  }
}

export const savingTotalReducer = (state: TotalSavingState = initialTotalState, action: any):TotalSavingState => {
  return state
}