import { RootState, ThunkDispatch } from './../../store'
import { SavingPlan } from './reducer';

// Action Creator

export function loadSavingPlan(savingPlans: SavingPlan[]) {
    return {
        type: '@@savingPlan/LOAD_SAVING_PLAN' as const,
        savingPlans: savingPlans
    }
}

export function loadTotalSaving(totalSaving: number) {
    return {
        type: '@@savingPlan/LOAD_TOTAL_SAVING' as const,
        totalSaving: totalSaving
    }
}

export function loadCompletedSavingPlan(completedPlans: SavingPlan[]) {
    return {
        type: '@@savingPlan/LOAD_COMPLETE_SAVING_PLAN' as const,
        completedPlans: completedPlans
    }
}

export function clearSavingPlanData() {
    return {
        type: '@@savingPlan/CLEAR_SAVING_PLAN_DATA' as const
    }
}

export function savingPlanIsLoading(isLoading: boolean) {
    return {
        type: '@@savingPlan/SAVING_PLAN_IS_LOADING' as const,
        isLoading
    }
}

export type SavingPlanActions = ReturnType<typeof loadSavingPlan> |
    ReturnType<typeof loadTotalSaving> |
    ReturnType<typeof loadCompletedSavingPlan> |
    ReturnType<typeof clearSavingPlanData> |
    ReturnType<typeof savingPlanIsLoading>


// Thunk Action

export function getSavingPlans() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {

        try {
            dispatch(savingPlanIsLoading(true));

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/getSavingPlans`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });

            const data = await res.json();

            if (!data) {
                console.log("Invalid user")
            };

            dispatch(loadSavingPlan(data));

        } catch (err) {
            console.error(err);
        } finally {
            dispatch(savingPlanIsLoading(false));
        }
    }
}

export function getTotalSaving() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {

        try {

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/getTotalSaving`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });

            const data = await res.json();

            if (!data) {
                console.log("Invalid user")
            };

            dispatch(loadTotalSaving(data.total_saving));

        } catch (err) {
            console.error(err);
        }
    }
}

export function createSavingGoal(goalName: string, goalAmount: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {

            const formObject = {
                goalName: goalName,
                goalAmount: parseInt(goalAmount),
            }

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/createSavingGoal`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getState().auth.token
                },
                body: JSON.stringify(formObject)
            })

            const result = await res.json();
           
            if (!result) {
                return;
            }

            if (result === "success") {
               
                dispatch(getSavingPlans());
                alert("新增成功");
            }

        } catch (err) {
            console.error(err);
        }
    }
}

export function getCompletedSavingPlans() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {

        try {

            dispatch(savingPlanIsLoading(true));

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/getCompletedSavingPlans`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });

            const data = await res.json();

            if (!data) {
                console.log("Invalid user")
            };

            dispatch(loadCompletedSavingPlan(data));

        } catch (err) {
            console.error(err);
        } finally {
            dispatch(savingPlanIsLoading(false));
        }
    }
}

export function markGoalComplete(userId: number, goalId: number, goalAmount: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {

        try {

            // Update TOTAL SAVING AMOUNT
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/updateTotalSaving`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getState().auth.token
                },
                body: JSON.stringify({ userId, goalAmount })
            });

            const result = await res.json(); // get new saving

            if (result.message === "not enough saving") {
                alert ("儲蓄不足");
                return;
            }

            if (result.status === 400 && result.message === "invalid user id or goal id") {
                alert("儲蓄目標不存在。")
                return;
            }

            // Mark Goal Complete
            const res2 = await fetch(`${process.env.REACT_APP_API_SERVER}/markGoalComplete`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getState().auth.token
                },
                body: JSON.stringify({ userId, goalId })
            });

            const response = await res2.json();

            if (!res2) {
                console.log("Invalid User")
            };

            if (res2.status === 400 && result.message === "invalid user id or goal id") {
                alert("儲蓄目標不存在。")
                return;
            } 

            if (res2.status === 200 && response.count == "1") {
                dispatch(getTotalSaving());
                dispatch(getSavingPlans());
                return;
            }        
            
        } catch (err) {
            console.error(err);
        }
    }
}


export function deleteGoal(userId: number, goalId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {

        try {

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/deleteGoal`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getState().auth.token
                },
                body: JSON.stringify({ userId, goalId })
            });

            const result = await res.json();

            if (res.status === 400 && result.message === "invalid user id or goal id") {
                alert("儲蓄目標不存在。")
                return;
            } 

            if (res.status === 200 && result.count == "1") {
                dispatch(getSavingPlans());
                dispatch(getCompletedSavingPlans());
                return;
            }        
            
        } catch (err) {
            console.error(err);
        }
    }
}

