import { SavingPlanActions } from "./action"

export interface SavingPlan {
    id: number
    user_id: number,
    goal_name: string,
    goal_amount: number,
    is_complete: boolean,
    created_at: Date,
    updated_at: Date,
}

export interface SavingPlanState {
    totalSaving: null | number,
    savingPlans: {
        id: number
        user_id: number,
        goal_name: string,
        goal_amount: number,
        is_complete: boolean,
        created_at: Date,
        updated_at: Date,
    }[],
    completedPlans: {
        id: number
        user_id: number,
        goal_name: string,
        goal_amount: number,
        is_complete: boolean,
        created_at: Date,
        updated_at: Date,
    }[],
    isLoading: boolean
}

const initialState: SavingPlanState = {
    totalSaving: null,
    savingPlans: [],
    completedPlans: [],
    isLoading: false
}

export const savingPlanReducer = (state: SavingPlanState = initialState, action: SavingPlanActions): SavingPlanState => {

    switch (action.type) {

        case "@@savingPlan/LOAD_SAVING_PLAN": 
            return {
                ...state,
                savingPlans: action.savingPlans
            };

        case "@@savingPlan/LOAD_TOTAL_SAVING":
            return {
                ...state,
                totalSaving: action.totalSaving
            };
    

        case "@@savingPlan/LOAD_COMPLETE_SAVING_PLAN": 
            return {
                ...state,
                completedPlans: action.completedPlans
            };
    
        case "@@savingPlan/CLEAR_SAVING_PLAN_DATA":
            return {
                ...state,
                totalSaving: null,
                savingPlans: [],
                completedPlans: []
            }

        case "@@savingPlan/SAVING_PLAN_IS_LOADING":
            const newIsLoading = action.isLoading;
            return {
                ...state,
                isLoading:  newIsLoading
            }

        default:
            return state
    }

}