import { KnowledgeActions } from "./action"

export interface KnowledgeState {
    quizzes: {
        quiz_id: number,
        quiz_name: string
    }[] | undefined;
    completedQuizzes: {
        quiz_id: number,
        quiz_name: string,
        score: number
    }[] | undefined;
    completedQuizzesCount: number;
    questions: {
        question_id: number;
        question: string;
        choices: { choice: string, choice_id: number }[];
        selectedOption: { choice: string, choice_id: number };
        image: string
    }[] | undefined;
    startQuiz: boolean;
    userResponse: {
        question: string;
        choice: string;
        image: string;
        is_correct: boolean;
    }[] | undefined;
    modelAnswers: {
        choice: string;
    }[] | undefined;
    isLoading: boolean;
}


const initialState: KnowledgeState = {
    quizzes: [],
    completedQuizzes: [],
    questions: [],
    completedQuizzesCount: 1,
    startQuiz: false,
    userResponse: [],
    modelAnswers: [],
    isLoading: false
}

export const knowledgeReducer = (state: KnowledgeState = initialState, action: KnowledgeActions) => {
    switch (action.type) {

        case "@@knowledge/MARK_KNOWLEDGE_LOADING":
            const newIsLoading = action.isLoading
            console.log(newIsLoading)

            return {
                ...state,
                isLoading: newIsLoading
            }

        case "@@knowledge/LOAD_COMPLETED_QUIZZES_COUNT":
            const newCompletedQuizzesCount = action.completedQuizzesCount

            return {
                ...state,
                completedQuizzesCount: newCompletedQuizzesCount
            }

        case "@@knowledge/LOAD_QUIZZES":
            const newQuizzes = action.quizzes

            return {
                ...state,
                quizzes: newQuizzes
            }

        case "@@knowledge/LOAD_COMPLETED_QUIZZES":
            const newCompletedQuizzes = action.completedQuizzes

            return {
                ...state,
                completedQuizzes: newCompletedQuizzes
            }

        case "@@knowledge/LOAD_QUESTIONS":
            const newQuestions = action.questions
            return {
                ...state,
                questions: newQuestions
            }

        case "@@knowledge/LOAD_REVISIONS":
            const newModelAnswers = action.modelAnswers
            const newUserResponse = action.userResponse

            return {
                ...state,
                modelAnswers: newModelAnswers,
                userResponse: newUserResponse,
            }

        case "@@knowledge/QUIZ_SELECT_ANSWER":
            const newAnswers = state.questions

            const { question_id, choice } = action

            if (newAnswers) {
                const answerIndex = newAnswers.findIndex(obj => { return obj.question_id === question_id })
                newAnswers[answerIndex].selectedOption = choice
            }

            return {
                ...state,
                questions: newAnswers
            }

        // case "@@knowledge/SUBMIT_QUIZ":
        //     // const newAnswers = state.answers

        //     return {
        //         ...state,
        //         // answers: newAnswers
        //     }

        default:
            return state
    }
}