import { Dispatch } from "redux"
import { RootState, ThunkDispatch } from "../../store"

// action creator

export function markKnowledgeLoading(isLoading: boolean) {
    return {
        type: '@@knowledge/MARK_KNOWLEDGE_LOADING' as const,
        isLoading
    }
}

export function loadCompletedQuizzesCount(completedQuizzesCount: number) {
    return {
        type: '@@knowledge/LOAD_COMPLETED_QUIZZES_COUNT' as const,
        completedQuizzesCount
    }
}

export function loadQuizzes(quizzes: {
    quiz_id: number,
    quiz_name: string
}[] | undefined) {
    return {
        type: '@@knowledge/LOAD_QUIZZES' as const,
        quizzes
    }
}

export function loadCompletedQuizzes(completedQuizzes: {
    quiz_id: number,
    quiz_name: string,
    score: number
}[] | undefined) {
    return {
        type: '@@knowledge/LOAD_COMPLETED_QUIZZES' as const,
        completedQuizzes
    }
}

export function loadQuestions(questions: {
    question_id: number;
    question: string;
    choices: { choice: string, choice_id: number }[];
    selectedOption: { choice: string, choice_id: number };
    image: string
}[] | undefined) {
    return {
        type: '@@knowledge/LOAD_QUESTIONS' as const,
        questions
    }
}

export function loadRevisions(userResponse: {
    question: string;
    choice: string;
    image: string;
    is_correct: boolean;
}[] | undefined, modelAnswers: {
    choice: string;
}[] | undefined) {
    return {
        type: '@@knowledge/LOAD_REVISIONS' as const,
        userResponse,
        modelAnswers
    }
}

export function selectAnswer(question_id: number, choice: { choice: string, choice_id: number }) {
    return {
        type: '@@knowledge/QUIZ_SELECT_ANSWER' as const,
        question_id,
        choice
    }
}

//Thunk

//Thunk functions
const { REACT_APP_API_SERVER } = process.env

export function fetchQuizzes() {
    return async (dispatch: Dispatch<KnowledgeActions>, getState: () => RootState) => {
        try {
            dispatch(markKnowledgeLoading(true))

            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/quiz`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            })
            const result = await res.json();

            console.log("fetch quizzes: ", result)

            if (result.message === "success") {
                const completedQuizzesCount = parseInt(result.quizzes.completedQuizzesCount)

                dispatch(loadCompletedQuizzesCount(completedQuizzesCount))

                if (completedQuizzesCount === 0) {
                    dispatch(loadQuizzes(result.quizzes.quizzes))
                }

                dispatch(loadCompletedQuizzes(result.quizzes.completedQuizzes))
            }

        } catch (e) {
            console.error(e)
        } finally{
            dispatch(markKnowledgeLoading(false))
        }
    }
}

export function fetchQuestions(quizId: number) {
    return async (dispatch: Dispatch<KnowledgeActions>, getState: () => RootState) => {

        try {
            const res = await fetch(`${REACT_APP_API_SERVER}/quiz/${quizId}`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });

            const result = await res.json();

            if (result.message === "success") {
                const fetchedQuestions = result.questions

                let questions = []
                for (let fetchedQuestion of fetchedQuestions) {
                    let question = {
                        ...fetchedQuestion,
                        selectedOption: fetchedQuestion.choices[0]
                    }
                    questions.push(question)
                }

                dispatch(loadQuestions(questions))

            }

        } catch (e) {
            console.error(e);
        }
    }
}

export function fetchRevisions(quizId: number) {
    return async (dispatch: Dispatch<KnowledgeActions>, getState: () => RootState) => {

        try {
            dispatch(markKnowledgeLoading(true))

            const res = await fetch(`${REACT_APP_API_SERVER}/revision/${quizId}`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });

            const result = await res.json();

            if (result.message === "success") {
                const fetchRevisions = result

                console.log(fetchRevisions)
                dispatch(loadRevisions(result.userResponse, result.modelAnswers))
            }

        } catch (e) {
            console.error(e);
        }finally{
            dispatch(markKnowledgeLoading(false))
        }
    }
}

export function submitQuizToServer(quizId: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const state = getState()

            console.log(state.auth.token)
            console.log(state.knowledge.questions)

            const res = await fetch(`${REACT_APP_API_SERVER}/quiz/${quizId}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + state.auth.token
                },
                body: JSON.stringify({
                    questions: state.knowledge.questions
                })
            })

            const data = await res.json()

            //clear quizzes first
            dispatch(loadQuizzes([]))

            //fectch completed quizzes
            await dispatch(fetchQuizzes())

            return data
        } catch (e) {
            console.error(e);
        }
    }
}

type KnowledgeActionCreators =
    typeof loadQuizzes |
    typeof loadCompletedQuizzes |
    typeof loadQuestions |
    typeof selectAnswer |
    typeof loadRevisions |
    typeof loadCompletedQuizzesCount |
    typeof markKnowledgeLoading

export type KnowledgeActions = ReturnType<KnowledgeActionCreators>