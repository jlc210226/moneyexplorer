import { Dispatch } from "redux";
import { RootState, ThunkDispatch } from "../../store";
import { fetchCurrentStar } from "../currentStars/action";


const LOADMISSIONTARGETS = "@@MISSIONTARGETS_LOAD" as const;
// const MISSIONTARGETLOADER = '@@mission/MISSION_TARGET_LOADING'

// export function missionTargetLoading(isLoading: boolean) {
//     return {
//         type: MISSIONTARGETLOADER,
//         isLoading
//     }
// }

export function loadMissionTargets(missionTargets: any) {
    return {
        type: LOADMISSIONTARGETS,
        missionTargets
    }
}

export function fetchMissionTargets() {
    return async (dispatch: Dispatch , getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/getMissionTargets`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                },
            })
            const data = await res.json()
            const missionTargets = data.map((target: any) => {
                return { id: target.id, target_name: target.target_name, needed_star: target.needed_star, is_complete: target.is_complete }
            })
            dispatch(loadMissionTargets(missionTargets))
        } catch (err) {
            console.log(err)
        }
    }
}

export function addNewMissionTarget(target_name: string, needed_star: number) {
    return async (dispatch: ThunkDispatch , getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/addNewMissionTarget`, {
                method: 'POST',
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    target_name: target_name,
                    needed_star: needed_star,
                })
            })
            const result = await res.json()
            if(result.message === "success") {
                alert("成功新增目標")
                await dispatch(fetchMissionTargets())
            }
        } catch (err) {
            console.log(err)
        }
    }
}

export function redeemTarget(target_id: number, needed_star: number) {
    return async (dispatch: ThunkDispatch , getState: () => RootState) => {
        try {
            console.log(target_id)
            const res = await fetch(`${process.env.REACT_APP_API_SERVER}/redeemMissionTarget`, {
                method: 'POST',
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                    'Content-Type': 'application/json'
                }, body: JSON.stringify({
                    target_id: target_id,
                    needed_star: needed_star
                })
            })
            const result = await res.json()
            if(result.message === "success") {
                console.log(result.message)
                await dispatch(fetchCurrentStar())
                await dispatch(fetchMissionTargets())
                alert("👏成功兌換目標！")
            }
        } catch (err) {
            console.log(err)
        }
    }
}

type ActionCreators = typeof loadMissionTargets 
export type IMissionTargetsActions = ReturnType<ActionCreators>