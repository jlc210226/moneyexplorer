export interface IMissionTargetsState {
    missionTargets: {
        id: number
        target_name: string,
        needed_star: number,
        is_complete: boolean,
    }[];
}