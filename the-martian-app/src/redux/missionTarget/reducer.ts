import {IMissionTargetsState} from './state'
import {IMissionTargetsActions} from './action'


const initMissionTargets: IMissionTargetsState = {
        missionTargets:[]
}

export const missionTargetsReducer = (state: IMissionTargetsState = initMissionTargets, action: IMissionTargetsActions) => {
    switch (action.type) {
        case '@@MISSIONTARGETS_LOAD' : {
            let newmissionTargets = action.missionTargets
            return {
                ...state,
                missionTargets: newmissionTargets
            }
        }
        default: 
        return state
    }
}
