export interface IMissionTemplateState {
    missionTemplate: {
        id: number,
        mission_name: string,
        mission_image: string
    }[]
}