import { IMissionTemplateActions } from "./action";
import { IMissionTemplateState } from "./state";


const initMissionTemplate: IMissionTemplateState = {
    missionTemplate: [
        // {
        //     id: 1,
        //     mission_name: "溫習",
        //     mission_image: "/img/astronaut.png"
        // },
        // {
        //     id: 2,
        //     mission_name: "housework",
        //     mission_image: "/img/astronaut.png"
        // }
    ]
}

export const missionTemplateReducer = (state: IMissionTemplateState = initMissionTemplate, action: IMissionTemplateActions): IMissionTemplateState | any => {
    switch (action.type) {
        case "@@MISSIONTEMPLATE_LOAD":
            return {
                ...state,
                missionTemplate: action.missionTemplate
            }
        default:
            return state;
    }

}