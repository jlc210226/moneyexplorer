import { Dispatch } from "redux";
import { RootState } from "../../store";

const LOADMISSIONTEMPLATES = "@@MISSIONTEMPLATE_LOAD" as const;

export function loadMissionTemplate (missionTemplate: object[]) {
    return {
        type: LOADMISSIONTEMPLATES,
        missionTemplate: missionTemplate
    }
}

export function fetchMissionTemplate () {
    return async (dispatch: Dispatch , getState: () => RootState) => {
        try{
            const res = await fetch (`${process.env.REACT_APP_API_SERVER}/getAllMissionTemplates`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token,
                },
            })
            const data = await res.json();
            console.log(data)
            dispatch(loadMissionTemplate(data))
        }catch(err) {
            console.log(err)
        }
    }
}

type ActionCreators = typeof loadMissionTemplate
export type IMissionTemplateActions = ReturnType<ActionCreators>