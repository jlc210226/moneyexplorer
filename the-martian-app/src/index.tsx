import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './store';
import { history } from './store';
import { ConnectedRouter } from 'connected-react-router';
import { HashRouter } from 'react-router-dom';

declare let window: any;

const renderReactDom = () => {

  ReactDOM.render(

    <Provider store={store}>
      <HashRouter>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </HashRouter>
    </Provider>,
    document.getElementById('root')
    
  );
};

if (window.cordova) {
  document.addEventListener('deviceready',renderReactDom, false);
  window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);

  if ('addEventListener' in document) {
    document.addEventListener('DOMContentLoaded', function() {
        FastClick.attach(document.body);
    }, false);
  }

} else {
  renderReactDom();
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();