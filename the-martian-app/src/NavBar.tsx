import styles from "./Navbar.module.css"
// import img1 from "./../public/img/navbar-1.png"
// import img2 from "./../public/img/navbar-2.png"
// import img3 from "./../public/img/navbar-3.png"
// import img4 from "./../public/img/navbar-4.png"
// import img5 from "./../public/img/navbar-5.png"
import { NavLink } from "react-router-dom"

export function Navbar() {
    return (
        <div className={styles.navbar} onTouchStart={() => {
            setTimeout(function () {
                window.scrollTo(0, 0);
                document.body.scrollTop = 0;
            }, 200);
        }}>
            <NavLink to="/saving">
                <div className="navbar-option">
                    <div className={styles.navbarText}>
                        <img className={styles.rotate} src={"./img/navbar-1.png"} alt="navbar-img宇宙金庫"></img>
                        宇宙金庫
                     </div>
                </div>
            </NavLink>
            <NavLink to="/mission">
                <div className="navbar-option"><div className={styles.navbarText}><img className={styles.rotate} src={"./img/navbar-2.png"} alt="navbar-img任務星球"></img>任務星球</div></div>
            </NavLink>

            <NavLink to="/home">
                <div className="navbar-option"><div className={styles.navbarText}><img className={styles.rotate} src={"./img/navbar-3.png"} alt="navbar-img太空總署"></img>太空總署</div></div>
            </NavLink>

            <NavLink to="/knowledge">
                <div className="navbar-option"><div className={styles.navbarText}><img src={"./img/navbar-4.png"} alt="navbar-img知識寶藏"></img>知識寶藏</div></div>
            </NavLink>

            <NavLink to="/base">
                < div className="navbar-option"><div className={styles.navbarText}><img className={styles.rotate} src={"./img/navbar-5.png"} alt="navbar-img我的基地" ></img>我的基地</div></div>
            </NavLink>
            <div className={styles.navBackground}></div>
        </div>
    )
}