import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Knowledge from './components/Knowledge';
import { Navbar } from './NavBar';
import { Route, Switch, Redirect } from 'react-router-dom';
import MissionBoard from './components/MissionBoard';
import { Login } from './components/Login';
import { Base } from './components/Base'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './store';
import { Registration } from './components/Registration';
import { useEffect } from 'react';
import { checkLogin } from './redux/auth/action';
import { Homepage } from './components/Homepage'
import { SavingPlan } from './components/SavingPlan';
import { CreateSavingGoal } from './components/CreateSavingGoal';
import { EditProfile } from './components/EditProfile';
import { SavingBoard } from './components/SavingBoard';
import { EditPassword } from './components/EditPassword';
import { Home } from './components/Home'
import { SavingChart } from './components/SavingChart';
import { VerifyEmail } from './components/VerifyEmail';
import { ForgetPassword } from './components/ForgetPassword';
import { ResetPassword } from './components/ResetPassword';
import { EditMissionPin } from './components/EditMissionPin';
// import Loader from "react-loader-spinner";
import SavingCreate from './components/SavingCreate';
import AddMissionBoard from './components/AddMissionBoard';
import { Spinner } from 'reactstrap';


function App() {

  const dispatch = useDispatch();
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)

  useEffect(() => {
    dispatch(checkLogin())
  }, [dispatch, isAuthenticated])


  if (isAuthenticated === null) {
    return (

      <div className="GuestMain">
        <div >
          <Spinner size="l" color="secondary" />
        </div>
      </div>
    )
  }

  return (
    <div className="App">

      {
        !isAuthenticated &&
        <div className="GuestMain">
          <Switch>
            <Route path="/homepage"><Homepage /></Route>
            <Route path="/login"><Login /></Route>
            <Route path="/registration"><Registration /></Route>
            <Route path="/verify/:email"><VerifyEmail /> </Route>
            <Route path="/forgetPassword"><ForgetPassword /></Route>
            <Route path="/resetpassword/:id/:token"><ResetPassword /></Route>
            <Route path="/"><Homepage /></Route>
          </Switch>
        </div>
      }

      {
        isAuthenticated &&
        <>
          <div className="Main">
            <Switch>
              <Route path="/home"><Home /></Route>
              <Route path="/saving/chart"><SavingChart /></Route>
              <Route path="/saving/form"><SavingCreate /></Route>
              <Route path="/saving"><SavingBoard /></Route>
              <Route path="/addMission"><AddMissionBoard /></Route>
              <Route path="/knowledge"><Knowledge /></Route>
              <Route path="/mission"><MissionBoard /></Route>
              <Route path="/base"><Base /></Route>
              <Route path="/savingPlan"><SavingPlan /></Route>
              <Route path="/createSavingGoal"><CreateSavingGoal /></Route>
              <Route path="/editProfile"><EditProfile /></Route>
              <Route path="/editPassword"><EditPassword />  </Route>
              <Route path="/editMissionPin"><EditMissionPin /></Route>
              <Route path="/login"><Redirect to="/home" /></Route>
              <Route path="/registration"><Redirect to="/home" /></Route>
              <Route path="/"><Home /></Route>
              <div> 404 not found...</div>
            </Switch>
          </div>
          { isAuthenticated && <Navbar />}
        </>
      }

    </div>
  );
}

export default App;