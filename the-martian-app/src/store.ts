import { combineReducers, compose, createStore, AnyAction } from 'redux'
// import { IRootState, rootReducer } from './redux/board/reducer';
import { expenseTotalReducer, incomeTotalReducer, savingReducer, SavingState, savingTotalReducer, TotalExpenseState, TotalIncomeState, TotalSavingState } from './redux/saving/reducer'
// import state, actions and reducers from './redux/currentStars';
import { currentStarsReducer } from './redux/currentStars/reducer'
import { ICurrentStarsState } from './redux/currentStars/state'
import { applyMiddleware } from "redux";
import { createHashHistory } from 'history';
import {
  RouterState,
  connectRouter,
  routerMiddleware,
  // CallHistoryMethodAction
} from 'connected-react-router';
import { knowledgeReducer, KnowledgeState } from './redux/knowledge/reducer';
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
import { IUpcomingMissionsState } from './redux/upcomingMissions/state';
import { upComingMissionsReducer } from './redux/upcomingMissions/reducer';
import { IMissionTemplateState } from './redux/mission_template/state';
import { missionTemplateReducer } from './redux/mission_template/reducer';
import logger from 'redux-logger';
import { AuthState, authReducer } from './redux/auth/reducer';
// import { KnowledgeActions } from './redux/knowledge/action';
// import { SavingActions } from './redux/saving/action';
import { userInfoReducer, UserInfoState } from './redux/User/reducer';
import { IMissionTargetsState } from './redux/missionTarget/state';
import { missionTargetsReducer } from './redux/missionTarget/reducer';
import { SavingPlanState, savingPlanReducer } from './redux/savingPlan/reducer';
export const history = createHashHistory()

export type ThunkDispatch = OldThunkDispatch<RootState, null, AnyAction>;

//IRootState
export interface RootState {
  saving: SavingState,
  totalIncome: TotalIncomeState,
  totalExpense: TotalExpenseState,
  totalSaving: TotalSavingState,
  knowledge: KnowledgeState,
  currentStar: ICurrentStarsState,
  upcomingMissions: IUpcomingMissionsState
  missionTemplate: IMissionTemplateState,
  missionTargets: IMissionTargetsState
  router: RouterState,
  auth: AuthState,
  userInfo: UserInfoState,
  savingPlan: SavingPlanState,
}

//rootReducer
export const reducer = combineReducers<RootState>({
  saving: savingReducer,
  totalIncome: incomeTotalReducer,
  totalExpense: expenseTotalReducer,
  totalSaving: savingTotalReducer,
  knowledge: knowledgeReducer,
  currentStar: currentStarsReducer,
  upcomingMissions: upComingMissionsReducer,
  missionTemplate: missionTemplateReducer,
  missionTargets: missionTargetsReducer,
  router: connectRouter(history),
  auth: authReducer,
  userInfo: userInfoReducer,
  savingPlan: savingPlanReducer,
})

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// export default createStore<IRootState, IRootActions, {}, {}>(rootReducer);
export default createStore(reducer, composeEnhancers(
  applyMiddleware(thunk),
  applyMiddleware(routerMiddleware(history)),
  applyMiddleware(logger)
));