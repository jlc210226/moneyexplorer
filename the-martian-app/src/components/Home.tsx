import style from './Home.module.css';
import { Container, Row, Col } from 'reactstrap';
import { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { fetchTotalExpenseByMonth, fetchTotalSavingByMonth } from '../redux/saving/action';
import { push } from 'connected-react-router';
import { getUserInfo } from '../redux/User/action';
import { fetchUpcomingMissions } from '../redux/upcomingMissions/action';
import { getSavingPlans } from '../redux/savingPlan/action';

export function Home() {

    //Test

    // Welcome Message
    const welcomeMsg = {
        1: "你今天記賬了嗎？",
        2: "記得先儲蓄後消費！",
        3: "量入為出！",
        4: "貨比三家笑哈哈",
    }

    const [msg, setMsg] = useState(welcomeMsg[1]);

    useEffect(() => {
        const x = (Math.floor(Math.random() * 3) + 1)
        setMsg((welcomeMsg as any)[x])

    }, []);

    // Animate Robot
    const [botImage, setBotImage] = useState("./img/bot.png");
    const [x, setX] = useState(0)

    useEffect(() => {
        const interval = setInterval(() => {
            if (x % 2 === 0) {
                setBotImage("./img/bot.png")
                setX(x => x + 1)
            } else {
                setBotImage("./img/bot2.png")
                setX(x => x + 1)
            }

        }, 1000);

        return () => clearInterval(interval);

    }, [x, botImage]);

    const dispatch = useDispatch();
    const today = new Date();
    const thisMonth = today.getMonth();
    const thisYear = today.getFullYear();

    const userInfo = useSelector((state: RootState) => state.userInfo.userInfo);

    const missionList = useSelector((state: RootState) => state.upcomingMissions.upComingMissions)
    const missions = missionList.filter(m => m.mission_status === "in progress" && m.expired === false);

    const savingPlanList = useSelector((state: RootState) => state.savingPlan.savingPlans);
    const totalSavingArr = useSelector((state: RootState) => state.totalSaving.totalSaving);
    const totalExpenseArr = useSelector((state: RootState) => state.totalExpense.totalExpense);

    const [saving, setSaving] = useState(0);
    const [expense, setExpense] = useState(0);

    useEffect(() => {
        dispatch(fetchTotalSavingByMonth());
        dispatch(fetchTotalExpenseByMonth());
        dispatch(getUserInfo());
        dispatch(fetchUpcomingMissions());
        dispatch(getSavingPlans());
    }, []);

    useEffect(() => {

        if (totalSavingArr.length > 0) {
            const thisMonthSavingObj = totalSavingArr.find(o => new Date(Date.parse(o.date_trunc)).getMonth() === thisMonth && new Date(Date.parse(o.date_trunc)).getFullYear() === thisYear);
            setSaving(thisMonthSavingObj?.totalSaving);
        } else {
            setSaving(0)
        }

        if (totalExpenseArr.length > 0) {
            const thisMonthExpenseObj = totalExpenseArr.find(e => new Date(Date.parse(e.date_trunc)).getMonth() === thisMonth && new Date(Date.parse(e.date_trunc)).getFullYear() === thisYear);
            setExpense(thisMonthExpenseObj?.totalExpense)
        } else {
            setExpense(0)
        }

    }, [totalSavingArr, totalExpenseArr]);

    return (
        <Container className={style.container}>
            <Row>
                <div className={style.mainTitle}><h3>太空總署</h3></div>
            </Row>

            <Row >
                <Col >
                    <div className={style.block + " " + style.botBox} onClick={() => {
                        dispatch(push('/saving'))
                    }}>
                        <img className={style.botImage} src={botImage} alt="botImg" />
                        <div className={style.title}>歡迎回來，{userInfo.nickname}！</div>
                        <div className={style.botContent}>{msg}</div>
                    </div>
                    <div className={style.cardBottom + " " + style.cardBottomBot}></div>
                </Col>
            </Row>


            <Row noGutters>

                {saving >= 0 &&
                    <Col xs="6" >
                        <div className={style.block}>
                            <div className={style.title}>本月儲蓄</div>
                            <div className={style.content}>${saving}</div>
                        </div>
                        <div className={style.cardBottom}></div>
                    </Col>
                }

                {saving === undefined &&
                    <Col xs="6" >
                        <div className={style.block}>
                            <div className={style.title}>本月儲蓄</div>
                            <div className={style.content}>$0</div>
                        </div>
                        <div className={style.cardBottom}></div>
                    </Col>
                }

                {expense === undefined &&
                    <Col xs="6" >
                        <div className={style.block}>
                            <div className={style.title}>本月支出</div>
                            <div className={style.content}>$0</div>
                        </div>
                        <div className={style.cardBottom}></div>
                    </Col>
                }

                {expense >= 0 &&
                    <Col xs="6" >
                        <div className={style.block}>
                            <div className={style.title}>本月支出</div>
                            <div className={style.content}>${expense}</div>
                        </div>
                        <div className={style.cardBottom}></div>
                    </Col>
                }
            </Row>


            <Row noGutters>
                <Col xs="8" >
                    <div className={style.block + " " + style.savingPlan} onClick={() => {
                        dispatch(push('/savingPlan'))
                    }}>
                        <img className={style.rocketImage} src={"./img/rocket.png"} alt="rocket" />
                        <div className={style.title}>儲蓄大計</div>
                        <div className={style.content}>{savingPlanList.length}個</div>
                    </div>
                    <div className={style.cardBottom}></div>
                </Col>

                <Col xs="4" >
                    <div className={style.block} onClick={() => {
                        dispatch(push('/mission'))
                    }}>
                        <div className={style.title}>待辦任務</div>
                        <div className={style.content}>{missions.length}個</div>
                    </div>
                    <div className={style.cardBottom}></div>
                </Col>


            </Row>
        </Container>
    )
}