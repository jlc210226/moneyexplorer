import style from './Quiz.module.css'
import Question from './Questions'
import Revision from './Revisions'
import { RootState } from '../store'
import React, { useEffect, useState } from 'react';
import { Button, Spinner } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux'
import { selectAnswer, fetchQuestions, submitQuizToServer, fetchQuizzes, fetchRevisions } from '../redux/knowledge/action'
import PopUp from './QuizPopUp';

export default function Quiz() {

    const dispatch = useDispatch()
    const [quizObj, setQuizObj] = useState<{ quizName: string, quizId: number }>()
    const completedQuizzesCount = useSelector((state: RootState) => state.knowledge.completedQuizzesCount)
    const quizzes = useSelector((state: RootState) => state.knowledge.quizzes)
    // const [completedQuizzes, setCompletedQuizzes] = useState<Array<{ quiz_id: number, quiz_name: string, score: number }> | null>()
    const completedQuizzes = useSelector((state: RootState) => state.knowledge.completedQuizzes)
    const questions = useSelector((state: RootState) => state.knowledge.questions)
    const modelAnswers = useSelector((state: RootState) => state.knowledge.modelAnswers)
    const userResponse = useSelector((state: RootState) => state.knowledge.userResponse)
    const isLoading = useSelector((state: RootState) => state.knowledge.isLoading)

    const [page, setPage] = useState("quizzes")

    console.log(modelAnswers)

    useEffect(() => {
        dispatch(fetchQuizzes())
    }
        , [dispatch])

    const handleSelect = (questionId: number, choiceJSON: string) => {
        const choice = JSON.parse(choiceJSON)

        dispatch(selectAnswer(questionId, choice))
    }

    const selectQuiz = (quizId: number, quizName: string) => {
        dispatch(fetchQuestions(quizId))
        setQuizObj({ quizName, quizId })
        setPage("questions")
    }

    const revision = (quizId: number, quizName: string) => {
        dispatch(fetchRevisions(quizId))
        setQuizObj({ quizName, quizId })
        setPage("revision")
    }

    const handleSubmit = () => {
        //event.preventDefault();
        if (quizObj?.quizId) {
            dispatch(submitQuizToServer(quizObj.quizId))
        }
        setPage("quizzes")
    }

    const renderQuestion = (questionId: number, question: string, choices: { choice: string, choice_id: number }[], image: string, selectedOption: string) =>
        <Question key={questionId} onChange={(e) => handleSelect(questionId, e.target.value)} question={question} options={choices} image={image} selectedOption={selectedOption} />

    const renderRevision = (index: number, question: string, image: string, choice: string, isCorrect: boolean, modelAnswers: { choice: string; }[]) =>
        <Revision index={index} question={question} image={image} choice={choice} isCorrect={isCorrect} key={index} modelAnswers={modelAnswers} />


    return (
        <>  {
            isLoading && <div className={style.loading}>
                <Spinner size="l" color="secondary" />
            </div>
        }
            {
                page === "quizzes" &&

                <div className={style.quizzes}>
                    <div>
                        {completedQuizzesCount === 0 ? <div className={style.title}>🧠 挑戰測驗 🧠
                        </div> : <div className={style.title}>🎉 你已完成今天的測驗 🎉</div>}
                        {completedQuizzesCount === 0 && quizzes?.length && quizzes.length === 0 ? <div>🎉 你已完成所有的測驗 🎉</div> : quizzes?.map((quiz) => {
                            return <div key={quiz.quiz_id} className={style.quiz} onClick={() => { selectQuiz(quiz.quiz_id, quiz.quiz_name) }}>
                                <div className={style.quizName}>{quiz.quiz_name}</div>
                            </div>
                        })}
                    </div>
                    <div>
                        <div>🤓重溫測驗🤓</div>
                        {completedQuizzes?.map((completedQuiz) => {
                            return <div key={completedQuiz.quiz_id} className={style.completedQuiz} onClick={() => { revision(completedQuiz.quiz_id, completedQuiz.quiz_name) }} >
                                <div className={style.quizName}>{completedQuiz.quiz_name}</div>
                                <div className={style.score}> ⭐️  {completedQuiz.score}/5</div>
                            </div>
                        })
                        }
                    </div>

                </div>

            }

            {
                page === "questions" &&
                <div className={style.questions}>
                    <div>
                        <div className={style.quizTitle}>{quizObj?.quizName}</div>
                    </div>
                    <form>

                        <div className={style.questions}>
                            {questions?.map((item) => {
                                return renderQuestion(item.question_id, item.question, item.choices, item.image, item.selectedOption.choice)
                            })}

                            {/* <input className="btn btn-primary" type="submit" value="提交" /> */}
                        </div>

                    </form>
                    <div className={style.buttons}>
                        <PopUp buttonClassName={style.button} buttonLabel={"提交"} quizName={quizObj?.quizName} onSubmit={() => {
                            handleSubmit();
                            window.scrollTo(0, 0);
                            document.body.scrollTop = 0;
                        }} />
                        <Button className={style.button} onClick={() => { setPage('quizzes') }}>返回</Button>{' '}
                    </div>
                </div>
            }

            {
                page === "revision" &&
                <div className={style.revision}>
                    <div>
                        <div className={style.quizTitle}>{quizObj?.quizName}</div>
                    </div>
                    <div className={style.modelAnwsers}>
                        {userResponse?.map((item, i) => {
                            return renderRevision(i, item.question, item.image, item.choice, item.is_correct, modelAnswers!)
                        })}
                    </div>
                    <div className={style.buttons}>
                        <Button className={style.button} onClick={() => { setPage('quizzes') }}>返回</Button>{' '}
                    </div>
                </div>
            }


        </>
    )
}

