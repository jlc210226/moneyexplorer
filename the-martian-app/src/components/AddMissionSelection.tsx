import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
} from 'reactstrap';
import styles from './AddMissionSelection.module.css'
import { fetchMissionTemplate } from '../redux/mission_template/action';


export default function AddMissionSelection(props: any) {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchMissionTemplate())
    }, [dispatch])
    let allMissionTemplate = useSelector((state: RootState) => state.missionTemplate.missionTemplate)
    let items: { missionTemplateId: number, src: string, altText: string, caption: string }[] = allMissionTemplate.map((missionTemplate, idx) => ({
        missionTemplateId: missionTemplate.id,
        src: `./img/${missionTemplate.mission_image}`,
        altText: missionTemplate.mission_name,
        caption: missionTemplate.mission_name,
    }))

    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex: number) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    const slides = items.map((item, idx) => {
        return (
            <CarouselItem className={styles.carouselItem}
                onExiting={() => setAnimating(true)}
                onExited={() => setAnimating(false)}
                key={idx}
            >
                <div className={styles.carouselImgDiv}>
                    <img className={styles.carouselImg} src={item.src} alt={item.altText} onClick={() => {
                        props.updateIdEvent(item.missionTemplateId)
                    }} />
                </div>
                <p className={styles.carouselCaption}>{item.caption}</p>
                {/* <CarouselCaption captionText={item.caption} captionHeader={item.caption} /> */}
            </CarouselItem>
        );
    });

    return (
        <div className={styles.carouselDiv} >
            <Carousel
                activeIndex={activeIndex}
                next={next}
                previous={previous}
                interval={false}
                className={styles.carouselSlides}>
                <CarouselIndicators className={styles.carouselIndicators} items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
                {slides}
                <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
                <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
            </Carousel>
        </div>
    );
}


