import { push } from 'connected-react-router';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import style from './Homepage.module.css'
import { HomepageInfo } from './HomepageInfo';

export function Homepage() {

    const dispatch = useDispatch();

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    return (
        <div>
            <div><img className={style.homepageIcon} src={"./img/homepageIcon-2.png"} alt="homepageIcon" /></div>

            <span className={style.infobox} onClick={toggle}>ⓘ</span>
                            {modal && <HomepageInfo modal={modal} onChange={() => setModal(false)} />}

            <div className={style.buttons} onTouchStart={() => {
                window.scrollTo(0, 0);
                document.body.scrollTop = 0;
            }}>
                <div><input className={style.submitButton} type="submit" value={"登陸"} onClick={() => dispatch(push('/login'))}></input></div>
                <div><input className={style.submitButton} type="submit" value={"註冊"} onClick={() => dispatch(push('/registration'))}></input></div>
            </div>

        </div>

    )
}