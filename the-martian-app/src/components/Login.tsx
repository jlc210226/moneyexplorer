import { push } from "connected-react-router";
// import { eventNames } from "node:process";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { login, logout } from '../redux/auth/action'
import style from "./Login.module.css";


export function Login() {

    const [loginName, setLoginName] = useState("")
    const [password, setPassword] = useState("")
    const dispatch = useDispatch();

    return (
        <div className={style.loginPage}>
            <h1>用戶登陸</h1>

            <form onSubmit={(event) => {
                event.preventDefault();
                dispatch(login(loginName, password))

            }}>

                <div className={style.loginForm}>
                    <img className={style.rocketImage} src={"./img/rocket.png"} alt="rocket" />
                    <div>登入名稱</div>
                    <input onBlur={() => {
                        window.scrollTo(0, 0);
                        document.body.scrollTop = 0;
                    }} value={loginName} required onChange={event => {
                        setLoginName(event.currentTarget.value);
                    }} />
                    <div>密碼</div>
                    <input onBlur={() => {
                        window.scrollTo(0, 0);
                        document.body.scrollTop = 0;
                    }} value={password} type="password" required onChange={event => {
                        setPassword(event.currentTarget.value);
                    }} />

                </div>
                <div><input className={style.submitButton} type="submit" value={"登陸"} ></input></div>
            </form>

            <div><input onClick={() => {
                dispatch(push('/homepage'))
            }} className={style.submitButton} value={"返回"} readOnly></input></div>

            <div className={style.forgetPasswordLink} onClick={() => {dispatch(logout()); dispatch(push('/forgetPassword'))}} >忘記密碼</div>

        </div>
    )
}