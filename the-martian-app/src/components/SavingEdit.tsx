import React from 'react'
import style from './Saving.module.css';

export function SavingEdit(props: {
  onDelete: () => void
}) {
  
  return (
    <>
    <div>
        <button className={style.deletes} onClick={() => props.onDelete()} >✕</button>
    </div>
    </>
  )
} 