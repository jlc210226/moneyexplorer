import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { addCustomizedMission } from "../redux/upcomingMissions/action";
import styles from "./CustomizeMission.module.css"

interface customizeMissionData {
    mission_image: string,
    mission_name: string,
    exp_date: Date,
    reward_star: number
}


export default function CustomizeMission() {
    const dispatch = useDispatch()
    const { register, handleSubmit, errors } = useForm<customizeMissionData>();
    const onSubmitHandler = async (data: customizeMissionData) => {

        dispatch(addCustomizedMission(data.mission_name, data.exp_date, data.reward_star))
    }
    const currentDate = new Date()
    const stringDate = (currentDate.getFullYear()) + "-" + ((currentDate).getMonth() + 1) + "-" + (currentDate).getDate()
    return (
        <div className={styles.board}>
            <div className={styles.customizedDiv}><img src={"./img/astronautlyingdown.jpg"} alt="astronautLyingDown"></img></div>
            <form className={styles.customizeMissionForm} onSubmit={handleSubmit(onSubmitHandler)}>
                <label>獎勵星星<input name="reward_star" type="number" ref={register({ required: true , max: 50, min: 1})} /></label>
                {errors.reward_star && <p className={styles.errorMessage}>請輸入正確獎勵星星數目</p>}
                <label>完成日期<input name="exp_date" type="date" ref={register({ required: true , min: stringDate})} /></label>
                {errors.exp_date && <p className={styles.errorMessage}>請選擇正確日子</p>}
                <label>任務內容<input name="mission_name" type="text" ref={register({ required: true })} /></label>
                {errors.mission_name && <p className={styles.errorMessage}>請填寫任務內容</p>}
                <input type="submit" value="提交"></input>
            </form>
        </div>
    )
}
