import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Spinner } from 'reactstrap'
import { markKnowledgeLoading } from '../redux/knowledge/action'
import { RootState } from '../store'
import style from './Financial.module.css'

export default function Financial() {

    const dispatch = useDispatch()
    const isLoading = useSelector((state: RootState) => state.knowledge.isLoading)
    const { REACT_APP_API_SERVER } = process.env
    const [page, setPage] = useState("knowledgeList")
    const token = useSelector((state: RootState) => state.auth.token)
    const [knowledge, setKnowledge] = useState<Array<{ title: string, knowledge_image: string, content: string, source: string, url: string, knowledge_category: string }> | undefined>([])
    const [content, setContent] = useState<{
        title: string,
        knowledge_image: string,
        content: string,
        source: string,
        url: string,
        knowledge_category: string
    } | undefined>()

    useEffect(() => {
        async function fetchFinancialKnowledge() {
            try {
                dispatch(markKnowledgeLoading(true))
                const res = await fetch(`${REACT_APP_API_SERVER}/knowledge/financial`, {
                    headers: {
                        Authorization: 'Bearer ' + token
                    }
                });
                const fetchedKnowledge = await res.json()

                setKnowledge(fetchedKnowledge.financialKnowledge)
            } catch (err) {
                console.error(err.message)
            } finally {
                dispatch(markKnowledgeLoading(false))
            }

        };

        fetchFinancialKnowledge();

    }
        , [])

    let contentHTML = ''
    if (content?.content) {
        contentHTML = content.content
        console.log(contentHTML)
    }


    return (
        <>
            { isLoading && <div className={style.loading}>
                <Spinner size="l" color="secondary" />
            </div>
            }
            {
                page === "knowledgeList" &&

                <div>
                    <div className={style.knowledgeTitle}>理財知識</div>
                    {
                        knowledge?.map((item) => {
                            return <div key={item.title} className={style.knowledgeRow} onClick={() => { setPage("content"); setContent(item) }}>{item.title}</div>
                        })
                    }
                </div>
            }

            {
                page === "content" &&

                <div>
                    <div className={style.title}>{content?.title}</div>
                    <img className={style.image} src={"./img/knowledge/" + content?.knowledge_image} alt="" />
                    <div className={style.content} dangerouslySetInnerHTML={{ __html: contentHTML }}>{ }</div>
                    <a className={style.source} >{content?.source}</a>
                    <div><Button className={style.button} onClick={() => { setPage('knowledgeList') }}>返回</Button>{' '}</div>
                </div>
            }
        </>
    )
}

