import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import style from './SavingChart.module.css';
import { SavingPieChart } from './SavingPieChart';
import { SavingBalance } from './SavingBalance';
import { SavingBarChart } from './SavingBarChart'
import { push } from 'connected-react-router';
import { RootState } from '../store';

export function SavingChart(props: {
    // onBack: () => void;
}) {

    const dispatch = useDispatch();

    // const totalIncomeObj = useSelector((state: RootState) => state.totalIncome.totalIncome)
    // const totalExpenseObj = useSelector((state: RootState) => state.totalExpense.totalExpense)
    // const totalSavingObj = useSelector((state: RootState) => state.totalSaving.totalSaving)

    const [tab, setTab] = useState('pie')

    return (
        <>
            <div>
                <div className={style.container}>
                    <h3>圖表分析</h3>

                    {<>
                        <p className={style.chartButtonContainter}>

                            <button className={ tab === 'pie' 
                            ? style.chartButtonActive 
                            : style.chartButton }
                                onClick={() => setTab('pie')}>開支分佈</button>

                            <button className={ tab === 'bar' 
                            ? style.chartButtonActive 
                            : style.chartButton }
                                onClick={() => setTab('bar')}>收支統計</button>

                            <button className={ tab === 'balance' 
                            ? style.chartButtonActive 
                            : style.chartButton }
                                onClick={() => setTab('balance')}>結餘計算</button>

                        </p>
                    </>}

                    {tab === 'pie'
                        ? 
                        <>
                        
                            <div className={style.canvas}>
                                <SavingPieChart />
                                <div className={style.white}></div>
                            </div>
                        
                        </>
                        : ''}

                    {tab === 'bar'
                        ? 
                        <>
                        
                            <div className={style.canvas}>
                                <SavingBarChart />
                                <div className={style.white}></div>
                            </div>
                        
                        </>
                        : ''}

                    {tab === 'balance'
                        ? 
                        <>
                    
                            <div className={style.canvasTable}>
                                <SavingBalance />
                            </div>
                        
                        </>
                        : ''}

                    {/* <p><button onClick={props.onBack} className="button">返回</button></p> */}

                    {<p><button onClick={()=> {
                        dispatch(push('/saving'))
                    }} className="button">返回</button>
                    </p>}
                </div>
            </div>
        </>
    )
}


