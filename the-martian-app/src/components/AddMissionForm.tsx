// import React from "react";
import styles from './AddMissionForm.module.css';
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store";
import { addNewUpcomingMission } from "../redux/upcomingMissions/action";
import React from 'react';

interface addMissionData {
  reward_star: number,
  exp_date: Date,
  mission_name: string,
}
interface IAddMissionFormProps {
  id: number
}
export default function AddMissionForm(props: IAddMissionFormProps) {
  const dispatch = useDispatch()
  let allMissionTemplate = useSelector((state: RootState) => state.missionTemplate.missionTemplate)
  let currentMissionTemplate = allMissionTemplate.filter((missionTemplate) => missionTemplate.id === props.id)
  const currentDate = new Date()
  const stringDate = (currentDate.getFullYear()) + "-" + ((currentDate).getMonth() + 1) + "-" + (currentDate).getDate()
  console.log("currentDate", currentDate)
  const { register, handleSubmit, errors } = useForm<addMissionData>();
  const onSubmitHandler = async (data: addMissionData) => {
    dispatch(addNewUpcomingMission(currentMissionTemplate[0].id, data.mission_name, data.exp_date, data.reward_star));
  }

  return (
    <div className={styles.board}>
      <div className={styles.missionBoard}>
        <img src={`./img/${currentMissionTemplate[0].mission_image}`} alt={`currentMissionTemplate${currentMissionTemplate[0].mission_image}`}></img>
      </div>
      <h6>{currentMissionTemplate[0].mission_name}</h6>
      <div className={styles.form}>
        <form className={styles.addMissionForm} onSubmit={handleSubmit(onSubmitHandler)}>
          <label>獎勵星星<input name="reward_star" type="number" ref={register({ required: true, min: 1 })} onBlur={() => {
            window.scrollTo(0, 0);
            document.body.scrollTop = 0;
          }} /></label>
          {errors.reward_star && <span className={styles.errorMessage}>請輸入正確獎勵星星數目</span>}
          <label>完成日期<input name="exp_date" type="date" ref={register({ required: true, min: stringDate })} onBlur={() => {
            window.scrollTo(0, 0);
            document.body.scrollTop = 0;
          }} /></label>
          {errors.exp_date && <span className={styles.errorMessage}>請選擇正確日子</span>}
          <input type="submit" value="提交"></input>
        </form>
      </div>
    </div>
  )
}
