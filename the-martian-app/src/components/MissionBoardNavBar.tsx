
import { NavLink } from 'react-router-dom';
import styles from './MissionBoardNavBar.module.css';
export default function MissionBoardNavBar() {
    return (<nav className={styles.missionBoardNavbar}>
        <NavLink to="/mission" exact activeClassName={styles.missionBoardActive}><span className={styles.title}>待辦任務</span></NavLink>
        <NavLink to="/mission/historymissions" activeClassName={styles.missionBoardActive}><span className={styles.title}>歷史任務</span></NavLink>
    </nav>)
}