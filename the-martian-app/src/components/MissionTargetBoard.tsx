import CurrentStars from "./CurrentStars"
import styles from "./MissionTargetBoard.module.css"
import MissionTargets from "./MissionTargets"
import AddMissionTarget from './AddMissionTarget'
import { useEffect } from "react"
import { fetchMissionTargets } from "../redux/missionTarget/action"
import { useDispatch } from "react-redux"
export default function MissionTargetBoard(props: {
    onBack: () => void;
}) {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchMissionTargets())
    }, [dispatch])
    return (
        <div className={styles.board}>
            <div className={styles.header}>
                <h3 className={styles.page_title}>獎品換領</h3>
                <CurrentStars />
            </div>
            <div className={styles.missionTargetBody}>
                <div className={styles.missionTargetsDiv}>
                    <MissionTargets />
                </div>
                <div className={styles.AddMissionTargetDiv}>
                    <AddMissionTarget />
                </div>
            </div>
            <p><button onClick={props.onBack} className="button"  onTouchStart={() => {
            setTimeout(function () {
              window.scrollTo(0, 0);
              document.body.scrollTop = 0;
            }, 200);
          }}>返回</button></p>
        </div>
    )
}