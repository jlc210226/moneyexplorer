import { push } from 'connected-react-router';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col, Spinner } from 'reactstrap';
import { forgetPassword } from '../redux/auth/action';
import style from './ForgetPassword.module.css';
import { RootState } from '../store';

export function ForgetPassword() {

    const dispatch = useDispatch();
    const [email, setEmail] = useState("");
    const isLoading = useSelector((state: RootState) => state.auth.isLoading)

    return (
        <Container>

            {isLoading &&
                <div className={style.loading}>
                    <Spinner size="l" color="secondary" />
                </div>
            }

            {!isLoading &&

                <Row>
                    <Col className={style.main}>
                        <div className={style.mainTitle}><h3>忘記密碼</h3></div>

                        <div>請輸入你登錄的電子郵件信箱。</div>
                        <div>我們將寄送設定密碼的郵件。</div>

                        <form className={style.passwordForm} onSubmit={(event) => {
                            event.preventDefault();

                            dispatch(forgetPassword(email))
                        }}>

                            <div className={style.formContent}>
                                <label className={style.formLabel}>電子郵件信箱</label>
                                <input className={style.formInput} type="email" required value={email} onChange={event => { setEmail(event.currentTarget.value) }}></input>
                            </div>

                            <div><input className={style.submitButton} type="submit" value={"送出"}></input></div>
                            <div><input onClick={() => {
                                dispatch(push('/login'))
                            }} className={style.submitButton} value={"返回"}></input></div>

                        </form>

                    </Col>
                </Row>
            }

        </Container>
    )
}