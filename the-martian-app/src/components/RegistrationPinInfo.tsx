import React, { useEffect, useState } from 'react'
import style from './MissionInfo.module.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default function RegistrationPinInfo(props: {
    modal: boolean;
    onChange: () => void;
}) {

    const [modal] = useState(false);
    const [content, setContent] = useState(1);

    
    useEffect(() => {
    }, [modal, content])

    return (
        <>
            <div>

                <Modal isOpen={props.modal} className={style.infoContainer}>
                    {content === 1 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>什麼是任務密碼？</ModalHeader>
                            <ModalBody>
                                <p>家長和小朋友可設定任務並給予獎勵。</p>
                                <div><img src={"./img/info/registrationPinInfo1.png"} width="100%" alt=""/></div>  
                            </ModalBody>
                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(2)} className={style.infoboxButton}>下一頁</Button>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>
                        </>}

                    {content === 2 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>什麼是任務密碼？</ModalHeader>
                            <ModalBody>
                                <p>當小朋友完成時，家長需輸入任務密碼確認完成。</p>
                                <span><img src={"./img/info/registrationPinInfo2.png"} width="100%" alt=""/></span>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>立即設立</Button>
                            </ModalFooter>
                        </>}

                </Modal>

            </div>
        </>
    )
}