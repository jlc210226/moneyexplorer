import React from 'react';
import { useEffect, useState } from 'react';
import { markKnowledgeLoading } from '../redux/knowledge/action';
import style from './Movie.module.css';
import Video from './Video';
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../store';
import { Spinner } from 'reactstrap';

const YOUTUBE_PLAYLIST_ITEM_API = 'https://www.googleapis.com/youtube/v3/playlistItems';

const MY_YOUTUBE_API_KEY = process.env.REACT_APP_MY_YOUTUBE_API_KEY

export default function Movie() {

    const dispatch = useDispatch()
    const isLoading = useSelector((state: RootState) => state.knowledge.isLoading)
    const [videoId, setVideoId] = useState("pzcjk0g34sU")
    const [youtubeData, setyoutubeData] = useState<Array<{
        snippet: {
            channelTitle: string;
            title: string;
            resourceId: {
                videoId: string
            };
            thumbnails: {
                medium: { url: string }
            }
        }
    }> | undefined>()

    console.log(youtubeData)

    const renderVideo = (videoTitle: string, videoId: string, thumbnail: string) =>
        <Video key={videoId} videoTitle={videoTitle} videoId={videoId} thumbnail={thumbnail} videOnClick={() => handleVideoClick(videoId)} />

    const handleVideoClick = (videoId: string) => {
        setVideoId(videoId)
    }

    useEffect(() => {
        async function loadYoutubePlaylist() {
            try {
                dispatch(markKnowledgeLoading(true))

                const res = await fetch(`${YOUTUBE_PLAYLIST_ITEM_API}?part=snippet&playlistId=PLj_UXCi9qE27ygQaDbvUfbd2J5r_gaXbY&maxResults=50&key=${MY_YOUTUBE_API_KEY}`)
                const fetchedData = await res.json()

                setyoutubeData(fetchedData.items)
            } catch (err) {
                console.error(err.message)
            } finally {
                dispatch(markKnowledgeLoading(false))
            }
        }
        loadYoutubePlaylist()
    }, [dispatch])

    return (
        <>
            { isLoading && <div className={style.loading}>
                <Spinner size="l" color="secondary" />
            </div>
            }
            <div className={style.movieContent}>
                <div className={style.movie}>
                    <div className={style.channelTitle}>{youtubeData ? youtubeData[0].snippet.channelTitle : null}</div>
                    <div className={style.videoSection}>
                        <div className={style.player}>
                            <iframe width="320" height="180" src={"https://www.youtube.com/embed/" + videoId} title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        </div>
                        <div className={style.playlist}>
                            {youtubeData?.map((item) => {
                                return renderVideo(item.snippet.title, item.snippet.resourceId.videoId, item.snippet.thumbnails.medium.url)
                            })}</ div>
                    </div>
                </div>
            </div>
        </>
    )
}
