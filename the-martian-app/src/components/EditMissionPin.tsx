import { Container, Row, Col } from 'reactstrap';
import { push } from 'connected-react-router';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { editPin } from '../redux/User/action';
import style from './EditMissionPin.module.css';

export function EditMissionPin() {

    const dispatch = useDispatch();

    const [oldPin, setOldPin] = useState("");
    const [pin, setPin] = useState("");
    const [confirmPin, setConfirmPin] = useState("");

    return (
        <div>
            <Container>
                <Row>
                    <Col>
                        <div className={style.mainTitle}><h3>修改任務密碼</h3></div>
                    </Col>
                </Row>

                <Row>
                    <Col>

                        <form onSubmit={(event) => {
                            event.preventDefault();

                            if (pin !== confirmPin) {
                                alert("新密碼確認密碼不一致");
                                return;
                            }

                            dispatch(editPin(oldPin, pin))

                        }}>

                            <div className={style.editForm}>

                                <div>
                                    <label className={style.editFormLabel}>舊密碼</label>
                                    <input className={style.formInput} type="password" required value={oldPin} onChange={event => { setOldPin(event.currentTarget.value) }} onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} ></input>
                                </div>


                                <div>
                                    <label className={style.editFormLabel}>輸入新密碼</label>
                                    <input className={style.formInput} type="password" required
                                        pattern="(?=.*[0-9]).{4,4}$" title="任務密碼只可輸入數字，密碼長度為4個字元。"
                                        placeholder="請輸入4位數字密碼"
                                        value={pin} onChange={event => { setPin(event.currentTarget.value) }} onBlur={() => {
                                            window.scrollTo(0, 0);
                                            document.body.scrollTop = 0;
                                        }} ></input>
                                </div>


                                <div>
                                    <label className={style.editFormLabel}>重新輸入新密碼</label>
                                    <input className={style.formInput} type="password" required
                                        placeholder="請輸入4位數字密碼"
                                        value={confirmPin} onChange={event => { setConfirmPin(event.currentTarget.value) }} onBlur={() => {
                                            window.scrollTo(0, 0);
                                            document.body.scrollTop = 0;
                                        }} ></input>
                                </div>

                            </div>

                            <div><input className={style.submitButton} type="submit" value={"提交"}></input></div>

                        </form>

                        <div><input onClick={() => {
                            dispatch(push('/base'))
                        }} className={style.submitButton} value={"返回"} readOnly></input></div>

                    </Col>
                </Row>

            </Container>
        </div>
    )
}