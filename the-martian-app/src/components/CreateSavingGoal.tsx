import { push } from 'connected-react-router';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createSavingGoal } from '../redux/savingPlan/action';
import style from './CreateSavingGoal.module.css'

export function CreateSavingGoal() {


    const [goalName, setGoalName] = useState("");
    const [goalAmount, setGoalAmount] = useState("");
    const dispatch = useDispatch();
    

    return (
        <div className={style.main}>
            <h3>建立儲蓄大計</h3>

            <form onSubmit={(event) => {
                event.preventDefault();
                dispatch(createSavingGoal(goalName, goalAmount));
            }}>
                <div className={style.formContainer}>
                    <div>
                        <label>目標名稱</label>
                        <input type="text" required value={goalName} onChange={event => { setGoalName(event.currentTarget.value) }}></input>
                    </div>

                    <div>
                        <label>目標金額</label>
                        <input type="number" required value={goalAmount} onChange={event => { setGoalAmount(event.currentTarget.value) }}></input>
                    </div>
                </div>

                <div><input className={style.button} type="submit" value={"提交"}></input></div>

            </form>

            <div className={style.button} onClick={() => {dispatch(push('/savingPlan'))}}>返回</div>
        </div>
    )
}