import style from './SavingPlanItem.module.css';

export function SavingPlanItem(props: {
    user_id: number;
    goal_id: number;
    goal_name: string;
    goal_amount: number;
    is_complete: boolean;
    onShowDone: boolean;
    onShowDelete: boolean;
    created_at: string;
    onComplete: () => void;
    onDelete: () => void;

}) {

    return (
        <div className={style.savingItem}>
            <div className={style.title} >{props.goal_name}</div>

            <div className={style.box}>
                <div className={style.savingItemDetails}>

                    <div className={style.column}>目標金額：<span className={style.amount}>${props.goal_amount}</span></div>
                    <div className={style.column}>建立日期：{props.created_at}</div>

                </div>

                {props.onShowDone && <div><button className={style.completeButton} onClick={props.onComplete}>達成</button></div>}

                {props.onShowDelete && <div className={style.deleteButton} onClick={props.onDelete}>❌</div>}


            </div>

        </div>
    )
}