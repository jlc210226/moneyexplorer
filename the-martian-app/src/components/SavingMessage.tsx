import React, { useEffect, useState } from 'react'
// import { useSelector } from 'react-redux';
// import { RootState } from '../store';
import style from './Saving.module.css';
import { Spinner } from 'reactstrap';

export function SavingMessage(props: {
  // amount: any;
}) {

  const quotes = [
    '先儲蓄後消費',
    '記得「財分三份」的用錢原則',
    '減低不必要的開支',
    '了解開支屬於需要或想要',
    '每月定立實際可行的儲蓄目標',
    '學習不浪費及「應使得使」'
  ]

  const spins = [ 
    'info',
    'primary',
    'success',
    'danger',
    'warning',
    'info',
  ]
  
  const [selected, setSelected] = useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      let random = Math.floor(Math.random()* quotes.length) 
      setSelected(random)
    }, 4500)

    return () => {
      clearInterval(timer)
    }
  }, [selected, quotes.length])

  return (
    <>

    <div className={style.savingMessageContainer}>

      <Spinner type="grow" color={spins[selected]} className={style.spin} />
       &nbsp; <div className={style.savingMessage}> {quotes[selected]} </div>
    
    </div>
    
    </>
  )
} 

