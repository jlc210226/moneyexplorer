// import React, { useState } from 'react'
import {  useSelector } from 'react-redux';
import { RootState } from '../store';
import style from './SavingChart.module.css';
import CanvasJSReact from '../canvasjs.react';
// import { SavingPieChart } from './SavingPieChart';
// import { SavingBalance } from './SavingBalance';
// import { push } from 'connected-react-router';
// var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export function SavingBarChart(props: {
    // onBack: () => void;
}) {

    // const dispatch = useDispatch();

    const totalIncomeObj = useSelector((state: RootState) => state.totalIncome.totalIncome)
    const totalExpenseObj = useSelector((state: RootState) => state.totalExpense.totalExpense)
    const totalSavingObj = useSelector((state: RootState) => state.totalSaving.totalSaving)
    // console.log(totalIncomeObj.length)

    // const month = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    const months = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
    const monthIncome = totalIncomeObj.map(e => { return (new Date(Date.parse(e.date_trunc)).getMonth()) })
    const totalIncomeData = totalIncomeObj.map(e => { return (parseInt(e.totalIncome)) })

    const monthExpense = totalExpenseObj.map(e => { return (new Date(Date.parse(e.date_trunc)).getMonth()) })
    const totalExpenseData = totalExpenseObj.map(e => { return (parseInt(e.totalExpense)) })

    const monthSaving = totalSavingObj.map(e => { return (new Date(Date.parse(e.date_trunc)).getMonth()) })
    const totalSavingData = totalSavingObj.map(e => { return (parseInt(e.totalSaving)) })

    var compareDateAsc = function (a: any, b: any) {
        var aDate = new Date(a.date).getTime();
        var bDate = new Date(b.date).getTime();
        return aDate > bDate ? -1 : 1;
    }
    var sortedMonthIncomeData = totalIncomeData.sort(compareDateAsc);
    // var sortedMonthExpenseData = totalExpenseData.sort(compareDateAsc);
    var sortedMonthSavingData = totalSavingData.sort(compareDateAsc);

    // console.log(totalIncomeData.length)

    let countIncome = 0;
    var dpsIncome: any[] = [];
    function parseDataPoints() {
        for (var i = 0; i <= (new Date().getMonth()); i++) {
            if (monthIncome.includes(i)) {
                dpsIncome.push({ label: (months[i]), y: sortedMonthIncomeData[countIncome] });
                countIncome += 1;
            } else {
                dpsIncome.push({ label: (months[i]), y: 0 });
            }
        }
    };

    let countExpense = 0;
    var dpsExpense: any[] = [];
    function parseDataPointsExpense() {
        for (var i = 0; i <= (new Date().getMonth()); i++) {
            if (monthExpense.includes(i)) {
                dpsExpense.push({ label: (months[i]), y: totalExpenseData[countExpense] });
                countExpense += 1;
            } else {
                dpsExpense.push({ label: (months[i]), y: 0 });
            }
        }
    };
    //  console.log(dpsExpense)

    let countSaving = 0;
    var dpsSaving: any[] = [];
    function parseDataPointsSaving() {
        for (var i = 0; i <= (new Date().getMonth()); i++) {
            if (monthSaving.includes(i)) {
                dpsSaving.push({ label: (months[i]), y: sortedMonthSavingData[countSaving] });
                countSaving += 1;
                // console.log(countSaving)
            } else {
                dpsSaving.push({ label: (months[i]), y: 0 });
            }
        }
    };

    parseDataPoints();
    parseDataPointsExpense();
    parseDataPointsSaving();

    // Line Chart
    const options = {
        backgroundColor: "white",
        theme: "light2",
        height: 400,
        animationEnabled: true,
        animationDuration: 2000,
        // exportEnabled: true,
        title: {
            text: "按月收支統計",
            fontFamily: "verdana",
            fontWeight: "bold",

        },
        axisY: {
            title: "總計",
            includeZero: true,
            prefix: "$",

        },
        axisX: {
            title: "月份",
            // includeZero: true,
            // prefix: "€",
            // suffix: "k"
        },
        toolTip: {
            shared: true,
            reversed: true,
        },
        legend: {
            verticalAlign: "center",
            horizontalAlign: "right",
            reversed: true,
            cursor: "pointer",
            fontSize: 12,
            // itemclick: this.toggleDataSeries
        },
        data: [
            {
                // type: "stackedColumn",
                type: "area",
                name: "儲蓄",
                showInLegend: true,
                // color: "#170170",
                color: "rgb(245, 49, 0)",
                dataPoints: dpsSaving
            },
            {
                // type: "stackedColumn",
                type: "area",
                name: "收入",
                showInLegend: true,
                // color: "#8000D2",
                color: "rgb(255, 115, 0)",
                dataPoints: dpsIncome
            },
            {
                // type: "stackedColumn",
                type: "area",
                name: "支出",
                showInLegend: true,
                // color: "#4B00B5",
                color: "#ffdd00",
                dataPoints: dpsExpense
            },
        ]

    }


    return (
        <>
        
                    { totalIncomeData.length !== 0 || totalExpenseData.length !== 0 || totalSavingData.length !== 0 
                    ? <CanvasJSChart options = {options} />
                    : <div className={style.chartMessage}>請新増項目以作分析</div> }
                            {/* <CanvasJSChart options = {options} /> */}

        </>
    )
}


