import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import style from './Saving.module.css';
import { SavingChart } from './SavingChart';

export function SavingReview(props: {
    
}) {

    const dispatch = useDispatch();
    const [calendar, setCalendar] = useState(String(new Date()))
    const [page, setPage] = useState('')

    const totalIncomeObj = useSelector((state: RootState) => state.totalIncome.totalIncome)
    const totalExpenseObj = useSelector((state: RootState) => state.totalExpense.totalExpense)
    const totalSavingObj = useSelector((state: RootState) => state.totalSaving.totalSaving)

    const resultTotalIncomeObj = totalIncomeObj.filter(i => new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(i.date_trunc)).getMonth())
    const resultTotalExpenseObj = totalExpenseObj.filter(e => new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(e.date_trunc)).getMonth())
    const resultTotalSavingObj = totalSavingObj.filter(s => new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(s.date_trunc)).getMonth())

    return (
        <>
            <div>
                { page === '' && 
                <>
                <h3>收支列表</h3>
                <div className={style.savingReviewContainer}>
                    
                    <img src={"./img/saving-ball.png"} className={style.imgBall} />
                    <div><input type="month" className={style.savingReviewContainerMonth} onChange={(event) => {
                        setCalendar(event.currentTarget.value)
                        // dispatch(updateTotal())
                        // const timeStamp = Date.parse(event.currentTarget.value);
                        // const dateToFormat = new Date(timeStamp)
                        // const getMonth = dateToFormat.getMonth()
                        // console.log(getMonth)
                    }} value={calendar} ></input></div>

                    <div className={style.savingReviewContainerItem}>

                        <div>總儲蓄
                            {(resultTotalSavingObj.length === 0) ? <div className={style.savingReviewAmount}>$0</div> : <div className={style.savingReviewAmount}>{`$${resultTotalSavingObj[0].totalSaving}`}</div>}
                        </div>

                        <div>總收入
                            {(resultTotalIncomeObj.length === 0) ? <div className={style.savingReviewAmount}>$0</div> : <div className={style.savingReviewAmount}>{`$${resultTotalIncomeObj[0].totalIncome}`}</div>}
                            {/* {totalIncomeObj.map(i => (
                                <>
                                {new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(i.date_trunc)).getMonth() ? 
                                <div className={style.savingReviewAmount}>{`$${i.totalIncome}`}</div>
                                 : '' }
                                </>
                            ))} */}
                        </div>

                        <div>總支出
                            {(resultTotalExpenseObj.length === 0) ? <div className={style.savingReviewAmount}>$0</div> : <div className={style.savingReviewAmount}>{`$${resultTotalExpenseObj[0].totalExpense}`}</div>}
                            {/* {totalExpenseObj.map(e => (
                                <>
                                {new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(e.date_trunc)).getMonth() ? 
                                <div className={style.savingReviewAmount}>{`$${e.totalExpense}`}</div>
                                : '' }
                                </>
                                ))} */}
                        </div>

                    </div>
                    {<div className={style.viewChartContainer}><button className={style.viewChart} onClick={() => setPage('chart')}>圖表分析 →</button></div>}
                </div>
                </>
                }

                {/* {
                page === 'chart' && <div>
                    <SavingChart onBack={() => {
                        setPage('')
                    }} />
                </div>
                } */}
            </div>
        </>
    )
}