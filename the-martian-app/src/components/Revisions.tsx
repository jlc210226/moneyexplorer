import style from './Quiz.module.css'

export default function Revision(props: {
    question: string,
    image: string,
    choice: string,
    isCorrect: boolean,
    index: number,
    key: number,
    modelAnswers: { choice: string; }[]
}) {
    return (
        <div className={style.modelAnwser}>
            <div >Q{props.index + 1}. {props.question}</div>
            { props.image && <img src={"./img/knowledge/" + props.image} alt="" height="500" width="500" />}
            <div className={style.response}>你的答案： {props.choice}   { props.isCorrect ? <i className={style.check}>✓</i> : <><i className={style.cross}>×</i><div className={style.response}>正確答案： {props.modelAnswers[props.index].choice}</div></>}</div>
            
        </div>
    )
}
