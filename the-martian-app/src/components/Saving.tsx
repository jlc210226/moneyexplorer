import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAllFinanceItem, fetchTotalExpenseByMonth, fetchTotalIncomeByMonth, fetchTotalSavingByMonth, markSavingLoading } from '../redux/saving/action';
import { RootState } from '../store';
import style from './Saving.module.css';
import SavingCreate from './SavingCreate';
import SavingList from './SavingList';
import { SavingMessage } from './SavingMessage';
import { SavingInfo } from './SavingInfo';
import { push } from 'connected-react-router';
import { Spinner } from 'reactstrap';

function Saving(props: {

}) {

    const dispatch = useDispatch();

    const isLoading = useSelector((state: RootState) => state.saving.isLoading)

    const [page, setPage] = useState('')
    const [edit, setEdit] = useState(false)
    const [order, setOrder] = useState(false)
    const [visible, setVisible] = useState(true)
    const [menu, setMenu] = useState(false)
    const [calendar, setCalendar] = useState(`${new Date().getFullYear()}-0${new Date().getMonth() + 1}`)

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    // let n = new Date(Date.parse(calendar)).getMonth();
    // let displayMonth: any[] = [
    //     "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
    // ];

    const savingObj = useSelector((state: RootState) => state.saving.saving)
    // console.log(savingObj)

    let checkSavingItems = savingObj.length

    const totalIncomeObj = useSelector((state: RootState) => state.totalIncome.totalIncome)
    const totalExpenseObj = useSelector((state: RootState) => state.totalExpense.totalExpense)
    const totalSavingObj = useSelector((state: RootState) => state.totalSaving.totalSaving)

    useEffect(() => {
        dispatch(fetchTotalIncomeByMonth());
        dispatch(fetchTotalExpenseByMonth());
        dispatch(fetchTotalSavingByMonth());
        dispatch(markSavingLoading(true));
        dispatch(fetchAllFinanceItem(order));
    }, [dispatch, page, edit, order])

    const resultTotalIncomeObj = totalIncomeObj.filter(
        i => new Date(Date.parse(calendar)).getFullYear() === new Date(Date.parse(i.date_trunc)).getFullYear()
            && new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(i.date_trunc)).getMonth()
    )

    const resultTotalExpenseObj = totalExpenseObj.filter(
        e => new Date(Date.parse(calendar)).getFullYear() === new Date(Date.parse(e.date_trunc)).getFullYear()
            && new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(e.date_trunc)).getMonth()
    )
    const resultTotalSavingObj = totalSavingObj.filter(
        s => new Date(Date.parse(calendar)).getFullYear() === new Date(Date.parse(s.date_trunc)).getFullYear()
            && new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(s.date_trunc)).getMonth()
    )

    return (
        <div className={style.main}>

            {
                isLoading && <div className={style.loading}>
                    <Spinner size="l" color="secondary" />
                </div>
            }

            { page === '' &&


                <div className={style.container}>

                    <div  className={style.upperContainer}>

                        <div><h3>收支列表</h3></div>

                        <div className={style.savingReviewContainer}>
                            <img src={"./img/saving-ball.png"} className={style.imgBall} alt="" />

                            <span className={style.infobox} onClick={toggle}>ⓘ</span>
                            {/* <Button color="danger" onClick={toggle}>ⓘ</Button> */}
                            {modal && <SavingInfo modal={modal} onChange={() => setModal(false)} />}

                            <div><input type="month" className={style.savingReviewContainerMonth} onChange={(event) => {
                                setCalendar(event.currentTarget.value)
                                // dispatch(updateTotal())
                                // const timeStamp = Date.parse(event.currentTarget.value);
                                // const dateToFormat = new Date(timeStamp)
                                // const getMonth = dateToFormat.getMonth()
                                // console.log(getMonth)
                            }} value={calendar}></input></div>

                            {visible ?
                                <>
                                    <div className={style.savingReviewContainerItem}>

                                        <div>
                                            <span className={style.eachItemSavingIcon}>▮</span>總儲蓄
                            {(resultTotalSavingObj.length === 0)
                                                ? <div className={style.savingReviewAmount}>$0</div> : <div className={style.savingReviewAmount}>{`$${resultTotalSavingObj[0].totalSaving}`}</div>}
                                        </div>

                                        <div>
                                            <span className={style.eachItemIncomeIcon}>▮</span>總收入
                            {(resultTotalIncomeObj.length === 0)
                                                ? <div className={style.savingReviewAmount}>$0</div> : <div className={style.savingReviewAmount}>{`$${resultTotalIncomeObj[0].totalIncome}`}</div>}
                                        </div>

                                        <div>
                                            <span className={style.eachItemIcon}>▮</span>總支出
                            {(resultTotalExpenseObj.length === 0)
                                                ? <div className={style.savingReviewAmount}>$0</div> : <div className={style.savingReviewAmount}>{`$${resultTotalExpenseObj[0].totalExpense}`}</div>}
                                        </div>

                                    </div>

                                </>
                                : ''}

                            <span>
                                {visible
                                    ? <button className={style.visible} id="DisabledAutoHide" onClick={() => setVisible(false)}>ᐱ</button>
                                    : <button className={style.visible} id="DisabledAutoHide" onClick={() => setVisible(true)}>ᐯ</button>}
                            </span>

                        </div>

                        <div className={style.savingButtonContainer}>

                            {/* Menu Button */}
                            <div>
                                {!menu
                                    ? <button className={style.menu} onClick={() => setMenu(true)}>≡</button>
                                    : <button className={style.menu} onClick={() => { setMenu(false); setEdit(false) }}>＜</button>}
                            </div>

                            {menu ?
                                <>
                                    <div className={style.menuButtonContainter}>
                                        <div>{!edit
                                            ? <button className={style.edit} onClick={() => setEdit(true)}>刪除</button> : <button className={style.edit} onClick={() => setEdit(false)}>完成</button>}</div>

                                        <div>{order === true
                                            ? <button className={style.sort} onClick={() => setOrder(false)}>日期倒序 ▲</button> : <button className={style.sort} onClick={() => setOrder(true)}>日期順序 ▼</button>}</div>
                                    </div>
                                </>
                                : ''}

                            <div>
                                {<div className={style.viewChartContainer}>
                                    <button className={style.viewChart}
                                        // onClick={() => setPage('chart')}
                                        onClick={() => {
                                            dispatch(push('/saving/chart'))
                                        }}
                                    >圖表分析 →</button>
                                </div>}
                            </div>

                            <div>
                                <button className={style.createSubmit}
                                    // onClick={() => setPage('create')}
                                    onClick={() => {
                                        dispatch(push('/saving/form'))
                                    }}
                                >新增項目 ＋</button>
                            </div>

                        </div>

                    </div>

                    <div className={style.lowerContainer}>

                        <>
                            {checkSavingItems === 0
                                ?   <p>
                                        立即開始鼓勵小朋友自行記帳 ☝，<br />學習收支平衡觀念啦！
                                    </p>
                                : <SavingMessage />}
                        </>

                        <div className={visible ? style.tableContainer : style.tableContainerMore}>
                            {!isLoading
                                && <table className={style.table}>
                                    <tbody>
                                        {savingObj.map((s, index) => (

                                            <React.Fragment key={`${s.category}-${s.id}-${index}`}>

                                                {new Date(Date.parse(calendar)).getFullYear() === new Date(Date.parse(s.date)).getFullYear()
                                                    && new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(s.date)).getMonth()
                                                    ?
                                                    <>
                                                        {/* <SavingList id={s.id} category={s.category} date={s.date} item={s.item} amount={s.amount} edit={edit} /> */}
                                                        <SavingList id={s.id} category={s.category} date={s.date} item={s.item} amount={s.amount} edit={edit} menu={menu}></SavingList>
                                                    </>
                                                    : null}

                                            </React.Fragment>

                                        ))}
                                    </tbody>
                                </table>
                            }
                        </div>

                    </div>
                    <>
                        {/* <table className={style.table}>
                        <tbody> */}

                        {/* filter Calendar logic */}
                        {/* {savingObj.map(s => (
                            <>
                                {new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(s.date)).getMonth() ? 
                                <tr>
                                    {edit && <td className={ s.category === 'saving' ? style.eachItemSaving : s.category === 'income' ? style.eachItemIncome : style.eachItem }><SavingEdit onDelete={() => {
                                        dispatch(deleteIncome(s.id, s.category))
                                    }} /></td>}

                                    <td className={ s.category === 'saving' ? style.eachItemSaving : s.category === 'income' ? style.eachItemIncome : style.eachItem }>{`${new Date(Date.parse(s.date)).getFullYear()}-${new Date(Date.parse(s.date)).getMonth()+1}-${new Date(Date.parse(s.date)).getDate()}`}</td>

                                    <td className={ s.category === 'saving' ? style.eachItemSaving : s.category === 'income' ? style.eachItemIncome : style.eachItem }>{s.item}</td>

                                    <td className={ s.category === 'saving' ? style.eachItemSaving : s.category === 'income' ? style.eachItemIncome : style.eachItem }><SavingItem amount={s.amount} /></td>
                                </tr>
                                : '' } 
                            </>
                        ))}

                        </tbody>
                    </table> */}
                    </>

                </div>}


            {/* {
                page === 'chart' && <div>
                    <SavingChart onBack={() => {
                        setPage('')
                    }} />
                </div>
            } */}


            {
                page === 'create' && <div>
                    <SavingCreate
                    // onBack={() => {
                    //     setPage('')
                    // }} 
                    />
                </div>
            }

        </div>

    );
}

export default Saving;


/* {totalExpenseObj.map(e => (
<>
{new Date(Date.parse(calendar)).getMonth() === new Date(Da(e.date_trunc)).getMonth() ?
<div className={style.savingReviewAmount}>{`$${e.totalExpediv>
: '' }
</>
))} */


/* <td>{date.map((date: string) => { return <span> {date} <br /></span> })}</td>
                        <td>{item.map((item: string) => { return <span> {item} <br /></span> })}</td>
                        <td>{amount.map((amount: string) => { return <span> {amount} <br /></span> })}</td> */
/* <td>{amount.map((amount: string) => <SavingItem amount={amount} onDelete={() => {
                            dispatch(removeSaving(amount))
                        }} />)}</td> */


/* {totalSavingObj.map(s => (
     <>
     {new Date(Date.parse(calendar)).getMonth() === new Date(Date.parse(s.date_trunc)).getMonth() ?
     (
         <div className={style.savingReviewAmount}>{`$${s.totalSaving}`}</div>
     )
      :
      (
         <div className={style.savingReviewAmount}>0</div>
      ) }
     </>
 ))}</div> */

