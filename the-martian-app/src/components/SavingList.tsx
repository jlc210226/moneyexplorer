import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { deleteIncome, fetchTotalExpenseByMonth, fetchTotalIncomeByMonth, fetchTotalSavingByMonth } from '../redux/saving/action';
// import { RootState } from '../store';
import styles from './SavingItem.module.css';
import { SavingEdit } from './SavingEdit';
import { SavingItem } from './SavingItem';

export function SavingList(props: {
    id: number,
    category?: any,
    date: any,
    item: string,
    amount: number,
    edit: boolean,
    menu: boolean
}) {

    const dispatch = useDispatch();
    const [edit] = useState(false);
    const [menu] = useState(false);

    useEffect(() => {
        dispatch(fetchTotalIncomeByMonth());
        dispatch(fetchTotalExpenseByMonth());
        dispatch(fetchTotalSavingByMonth()); 
    }, [edit, menu])

    return (
        <React.Fragment>
                    <tr>
                        { props.edit && props.menu
                        && 
                        <td className={ props.category === 'saving' 
                        ? styles.eachItemSaving : props.category === 'income' 
                        ? styles.eachItemIncome : styles.eachItem }>
                            <SavingEdit onDelete={() => {
                            dispatch(
                                deleteIncome(props.id, props.category)
                                )
                        }} />
                        </td> }

                        <td className={props.category === 'saving' 
                        ? styles.eachItemSaving : props.category === 'income' 
                        ? styles.eachItemIncome : styles.eachItem}>
                            {`${new Date(Date.parse(props.date)).getFullYear()}-${new Date(Date.parse(props.date)).getMonth() + 1}-${new Date(Date.parse(props.date)).getDate()}`}
                        </td>

                        <td className={props.category === 'saving' 
                        ? styles.eachItemSaving : props.category === 'income' 
                        ? styles.eachItemIncome : styles.eachItem}>
                            {props.item}
                        </td>

                        <td className={props.category === 'saving' 
                        ? styles.eachItemSaving : props.category === 'income' 
                        ? styles.eachItemIncome : styles.eachItem}>
                            <SavingItem amount={props.amount} />
                        </td>
                    </tr>
        </React.Fragment>
    )
}

export default SavingList;


