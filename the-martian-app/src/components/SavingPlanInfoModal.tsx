
import { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { useEffect } from 'react';
import style from './SavingPlanInfoModal.module.css'

export function SavingPlanModal(props: {
    isOpen: boolean
    toggleinfo: () => void
}) {

    const [page, setPage] = useState(1);

    useEffect(() => {
        setPage(1)
    }, [props.isOpen])

    return (

        <div className={style.main}>
            <Modal isOpen={props.isOpen} className={style.modalBox}>

                {page === 1 &&

                    <div>
                        <ModalHeader className={style.title}>儲蓄大計</ModalHeader>

                        <ModalBody className={style.formContainer}>

                            <p>一鍵建立儲蓄目標，逐步實現計劃。</p>
                            <span><img src={"./img/savingPlan/savingPlan1.png"} width="100%" alt=""/></span>

                        </ModalBody>

                        <ModalFooter>
                            <Button color="info" className={style.infoboxButton} onClick={() => setPage(2)}>更多</Button>
                            <Button color="secondary" className={style.infoboxButton} onClick={props.toggleinfo}>開始使用</Button>
                        </ModalFooter>
                    </div>

                }

                {page === 2 &&

                    <div>
                        <ModalHeader  className={style.title}>儲蓄大計</ModalHeader>

                        <ModalBody className={style.formContainer}>

                            <p>簡易輸入目標和金額，輕鬆訂立儲蓄計劃。</p>
                            <span><img src={"./img/savingPlan/savingPlan2.png"} width="100%" alt=""/></span>

                        </ModalBody>

                        <ModalFooter>
                            <Button color="info" className={style.infoboxButton} onClick={() => setPage(3)}>更多</Button>
                            <Button color="secondary" className={style.infoboxButton} onClick={props.toggleinfo}>開始使用</Button>
                        </ModalFooter>
                    </div>

                }

                {page === 3 &&

                    <div>
                        <ModalHeader className={style.title}>儲蓄大計</ModalHeader>

                        <ModalBody className={style.formContainer}>

                            <p>列表瀏覽儲蓄目標，快捷查看儲蓄狀態。</p>
                            <span><img src={"./img/savingPlan/savingPlan3.png"} width="100%" alt=""/></span>

                        </ModalBody>

                        <ModalFooter>
                            <Button color="info" className={style.infoboxButton} onClick={() => setPage(4)}>更多</Button>
                            <Button color="secondary" className={style.infoboxButton} onClick={props.toggleinfo}>開始使用</Button>
                        </ModalFooter>
                    </div>

                }


                {page === 4 &&

                    <div>
                        <ModalHeader  className={style.title}>儲蓄大計</ModalHeader>

                        <ModalBody className={style.formContainer}>

                            <p>列表瀏覽達成紀錄，了解儲蓄和消費模式。</p>
                            <span><img src={"./img/savingPlan/savingPlan4.png"} width="100%" alt=""/></span>

                        </ModalBody>

                        <ModalFooter>
                            <Button color="secondary" className={style.infoboxButton} onClick={props.toggleinfo}>開始使用</Button>
                        </ModalFooter>
                    </div>

                }

            </Modal>

        </div>
    )
}