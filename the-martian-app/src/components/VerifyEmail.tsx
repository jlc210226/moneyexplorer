import React, { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col, Spinner } from 'reactstrap';
import { sendConfirmationEmail, verifyEmail } from '../redux/auth/action';
import { RootState } from '../store';
import { push } from 'connected-react-router';
import style from './VerifyEmail.module.css';
import useRouter from 'use-react-router';


export function VerifyEmail() {

    const dispatch = useDispatch();
    const [code, setCode] = useState("")
    const [email, setEmail] = useState("")
    const isLoading = useSelector((state: RootState) => state.auth.isLoading)
    const [resendCounter, setResendCounter] = useState(90)
    const [counterBoolean, setCounterBoolean] = useState(true)

    const router = useRouter<{ email?: string }>()
    const emailParam = router.match.params.email;

    // console.log(router)
    // console.log(router.location.search)
    // console.log(emailParam)

    // 冇#號就用下面    
    // const query = new URLSearchParams(window.location.search);
    // const emailParam = query.get('email');

    useEffect(() => {
        if (emailParam) {
            setEmail(emailParam);
        }
    }, [emailParam])

    useEffect(() => {
        setCounterBoolean(true);
        setResendCounter(90);

    }, [counterBoolean])


    useEffect(() => {
        const interval = setInterval(() => {
            setResendCounter(x => x - 1)

        }, 1000);

        return () => clearInterval(interval);

    }, []);

    return (

        <Container>

            {
                isLoading &&
                <div className={style.loading}>
                    <Spinner size="l" color="secondary" />
                </div>
            }

            {!isLoading &&
                <Row>
                    <Col>
                        <h3>謝謝登記！</h3>
                        <p> 我們已透過電郵把驗證碼傳送至：<span className={style.email}>{email} </span></p>

                        <div className={style.formContainer}>
                            <form onSubmit={(event) => {
                                event.preventDefault();
                                dispatch(verifyEmail(email, code));

                            }}>

                                <div className={style.formContent}>
                                    <label> 請輸入驗證碼</label>
                                    <input className={style.formInput} type="string" required value={code} onChange={event => { setCode(event.currentTarget.value) }}></input>
                                </div>

                                <div><input onSubmit={() => {
                                    window.scrollTo(0, 0);
                                    document.body.scrollTop = 0;
                                }} className={style.submitButton} type="submit" value={"提交"}></input></div>
                            </form>
                        </div>
                    </Col>
                </Row>

            }

            {!isLoading &&
                <Row>
                    <Col className={style.resendCol}>
                        <div className={style.resendContainer}>
                            <p>未收到驗證碼？  {resendCounter >= 0 ? resendCounter : '0'} 秒後重新發送 </p>


                            {resendCounter <= 0 &&
                                <div>
                                    <div className={style.link}
                                        onClick={() => { dispatch(sendConfirmationEmail(email)); setCounterBoolean(false) }}>

                                        重新發送驗證碼 </div>
                                </div>
                            }

                            <div>
                                <div className={style.link} onClick={() => {
                                    dispatch(push('/login'))
                                    window.scrollTo(0, 0);
                                    document.body.scrollTop = 0;
                                }
                                }> 前往登入頁面 </div>
                            </div>
                        </div>
                    </Col>
                </Row>}
        </Container>

    )
}