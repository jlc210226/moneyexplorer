import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { RootState } from '../store';
// import style from './Quiz.module.css'

const Popup = (props: {
  buttonLabel: string,
  quizName: string|undefined,
  onSubmit: () => void,
  buttonClassName: string
}) => {

  const questions = useSelector((state: RootState) => state.knowledge.questions)
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button className={props.buttonClassName} onClick={toggle}>{props.buttonLabel}</Button>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>{props.quizName}</ModalHeader>
        <ModalBody>
          {questions?.map((question) => {
            return <div key={question.question_id}>
              <div>題目： {question.question}</div>
              <div>你的答案： {question.selectedOption.choice}</div>
            </div>
          })}
        </ModalBody>
        <ModalFooter>
          <Button className={props.buttonClassName} onClick={props.onSubmit}>確定答案</Button>{' '}
          <Button className={props.buttonClassName} onClick={toggle}>返回</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Popup;