import React, { useState } from "react";
import AddMissionForm from "./AddMissionForm";
import AddMissionSelection from "./AddMissionSelection";
import { RootState } from '../store'
import styles from './AddMissionBoard.module.css'
// import { Link, Route } from "react-router-dom";
// import CustomizeMission from "./CustomizeMission";
import CustomizeMissionBoard from "./CustomizeMissionBoard";
import { useSelector } from "react-redux";
import { useDispatch } from 'react-redux'
import { push } from "connected-react-router";
import { Spinner } from "reactstrap";


export default function AddMissionBoard() {
    const dispatch = useDispatch()
    const isLoading = useSelector((state: RootState) => state.knowledge.isLoading)
    const [page, setPage] = useState('')
    const [id, setId] = useState<number | undefined>();
    return (
        <div className={styles.outerBoard}>
            {
                isLoading && <div className={styles.loading}>
                    <Spinner size="l" color="secondary" />
                </div>
            }
            {page === "" && <div className={styles.board}>
                <div className={styles.header}>
                    <h2 className={styles.page_title}>新增任務</h2>
                    {/* <Link className={styles.linkToCustomize} to="/customizeMission"><button className={styles.customizedMission}>自訂任務<img src={'/img/rocket.png'}></img></button></Link> */}
                    <button onClick={() => { setPage('customizeMission') }} className={styles.customizedMission}>自訂任務<img src={'./img/rocket.png'} alt="customizeMission-rocket"></img></button>
                </div>
                <div className={styles.addMissionBoardBody}>
                    <div className={styles.addMissionFormDiv}>
                        {id ? <AddMissionForm id={id} /> : <div className={styles.addMissionBoard}>按下圖片以選擇任務🚀</div>}
                    </div>
                    <div className={styles.selectionDiv}>
                        <AddMissionSelection updateIdEvent={(id: number) => {
                            setId(id)
                        }} />
                    </div>
                </div>
                <p><button onClick={() => {
                    dispatch(push('/mission'))
                }} className="button">返回</button></p>

            </div>}

            {
                page === 'customizeMission' &&
                <CustomizeMissionBoard onBack={() => {
                    setPage('')
                }} />

            }

        </div>
    )
}