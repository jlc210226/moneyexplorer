import { Container, Row, Col } from 'reactstrap';
import { push } from 'connected-react-router';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { editPassword } from '../redux/User/action';
import style from './EditPassword.module.css';

export function EditPassword() {

    const dispatch = useDispatch();

    const [oldPassword, setOldPassword] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    return (

        <Container>
            <Row>
                <Col>
                    <div className={style.mainTitle}><h3>修改密碼</h3></div>
                </Col>
            </Row>

            <Row>
                <Col>

                    <form onSubmit={(event) => {
                        event.preventDefault();

                        if (password !== confirmPassword) {
                            alert("新密碼與確認密碼不一致");
                            return;
                        }

                        dispatch(editPassword(oldPassword, password))

                    }}>

                        <div className={style.editForm}>

                            <div>
                                <label className={style.editFormLabel}>舊密碼</label>
                                <input className={style.formInput} type="password" required value={oldPassword} onChange={event => { setOldPassword(event.currentTarget.value) }} onBlur={() => {
                                    window.scrollTo(0, 0);
                                    document.body.scrollTop = 0;
                                }} ></input>
                            </div>


                            <div>
                                <label className={style.editFormLabel}>輸入新密碼</label>
                                <input className={style.formInput} type="password" required
                                    pattern="(?!.*\s).{8,}" title="密碼長度不得少於8個字元，及不能含有空格。"
                                    value={password} onChange={event => { setPassword(event.currentTarget.value) }} onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} ></input>
                            </div>


                            <div>
                                <label className={style.editFormLabel}>重新輸入新密碼</label>
                                <input className={style.formInput} type="password" required value={confirmPassword} onChange={event => { setConfirmPassword(event.currentTarget.value) }} onBlur={() => {
                                    window.scrollTo(0, 0);
                                    document.body.scrollTop = 0;
                                }} ></input>
                            </div>

                        </div>

                        <div><input className={style.submitButton} type="submit" value={"提交"}></input></div>

                    </form>

                    <div><input onClick={() => {
                        dispatch(push('/base'))
                    }} className={style.submitButton} value={"返回"} readOnly></input></div>

                </Col>
            </Row>

        </Container>

    )
}