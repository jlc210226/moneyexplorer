import { Container, Row, Col, Spinner } from 'reactstrap';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../store'
import style from './SavingPlan.module.css'
import { deleteGoal, getCompletedSavingPlans, getSavingPlans, getTotalSaving, markGoalComplete } from '../redux/savingPlan/action'
import { SavingPlanItem } from './SavingPlanItem'
import { CompletedPlanItem } from './CompletedPlanItem';
import { useState } from 'react';
import { CreateSavingGoalModal } from './CreateSavingGoalModal';
import { SavingPlanModal } from './SavingPlanInfoModal';


export function SavingPlan() {

    const dispatch = useDispatch();
    const savingPlans = useSelector((state: RootState) => state.savingPlan.savingPlans);
    const totalSaving = useSelector((state: RootState) => state.savingPlan.totalSaving);
    const isLoading = useSelector((state: RootState) => state.savingPlan.isLoading)
    const [showComplete, setshowComplete] = useState(false);
    const [showDelete, setShowDelete] = useState(false);

    const completedPlans = useSelector((state: RootState) => state.savingPlan.completedPlans);

    useEffect(() => {
        dispatch(getCompletedSavingPlans())
    }, [dispatch, showComplete])

    const [showDone, setShowDone] = useState(false);

    useEffect(() => {
        dispatch(getSavingPlans())
        dispatch(getTotalSaving())
    }, [dispatch, totalSaving])

    useEffect(() => {
        const result = savingPlans.find(item => totalSaving! > item.goal_amount)
        if (result) {
            setShowDone(true)
            return;
        }
    }, [savingPlans, totalSaving])

    console.log(showDone)

    const [modal, setModal] = useState(false);
    const [showInfo, setShowInfo] = useState(false);

    const toggle = () => setModal(!modal);
    const toggleInfo = () => setShowInfo(!showInfo);

    return (

        <Container className={style.main}>

            <SavingPlanModal isOpen={showInfo} toggleinfo={toggleInfo} />

            <CreateSavingGoalModal isOpen={modal} toggle={toggle} />

            <Row className={style.titleRow}>
                <div className={style.mainTitle}><h3>儲蓄大計</h3></div>
                <span className={style.infobox} onClick={toggleInfo}>ⓘ</span>
            </Row>

            <Row className={style.topMenu} noGutters>
                <Col xs="6">
                    <div className={style.square + " " + style.firstSquare}>
                        <div className={style.topMenuTitle}>儲蓄金額 </div>
                        {totalSaving! < 0 ? <div className={style.topMenuAmountNegative}>${totalSaving}</div> : <div className={style.topMenuAmountPositive}>${totalSaving}</div>}
                    </div>

                </Col>

                {/* <Col xs="6">
                    <div className={style.square + " " + style.secondSquare} onClick={() => { dispatch(push('/createSavingGoal')) }}>
                        <img src={"/img/saving-ball.png"} className={style.imgBall} />
                        <div className={style.topMenuTitle}>➕</div>
                        <div className={style.topMenuTitle} >建立目標</div>
                    </div>

                </Col> */}

                <Col xs="6">
                    <div className={style.square + " " + style.secondSquare} onClick={toggle}>
                        <img src={"./img/saving-ball.png"} className={style.imgBall} alt="" />
                        <div className={style.topMenuTitle}>➕</div>
                        <div className={style.topMenuTitle} >建立目標</div>
                    </div>

                </Col>

            </Row>


            <Row className={style.menuBar}>

                    <div className={style.deleteButton} onClick={() => setShowDelete(!showDelete)}>{showDelete ? "完成" : "編輯"}</div>

                <Col className={style.tab}>
                    {showComplete ?
                        <div className={style.menu}>
                            <div onClick={() => { setshowComplete(false) }} >進行中</div>
                            <div onClick={() => { setshowComplete(true) }} className={style.clicked}>已完成</div>
                        </div>
                        :
                        <div className={style.menu}>
                            <div onClick={() => { setshowComplete(false) }} className={style.clicked}>進行中</div>
                            <div onClick={() => { setshowComplete(true) }}>已完成</div>
                        </div>
                    }
                </Col>

            </Row>

            {
                isLoading &&
                <div className={style.loading}>
                    <Spinner size="l" color="secondary" />
                </div>
            }

            {!isLoading &&
                <Row className={style.savingRow}>
                    <Col className={style.savingCol}>
                        <div className={style.savingItemContainer}>


                            {!showComplete && savingPlans && totalSaving !== null && savingPlans.map(item => (
                                <div key={item.id}>
                                    <SavingPlanItem user_id={item.user_id} goal_id={item.id} goal_name={item.goal_name} goal_amount={item.goal_amount} created_at={new Date(item.created_at).toLocaleDateString()}
                                        is_complete={item.is_complete}
                                        onShowDone={totalSaving >= item.goal_amount ? true : false}
                                        onShowDelete={showDelete}
                                        onComplete={() => { dispatch(markGoalComplete(item.user_id, item.id, item.goal_amount)) }}
                                        onDelete={() => { dispatch(deleteGoal(item.user_id, item.id)) }} />
                                </div>
                            ))
                            }

                            {showComplete && completedPlans && completedPlans.map(item => (

                                <div key={item.id}>
                                    <CompletedPlanItem user_id={item.user_id} goal_id={item.id} goal_name={item.goal_name} goal_amount={item.goal_amount} updated_at={new Date(item.updated_at).toLocaleDateString()}
                                        onShowDelete={showDelete}
                                        onDelete={() => { dispatch(deleteGoal(item.user_id, item.id)) }} />
                                </div>
                            ))
                            }
                        </div>


                    </Col>
                </Row>}

        </Container>
    )
}