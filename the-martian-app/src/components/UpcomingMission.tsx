import React, { useState } from 'react'
import EnterPin from './EnterPin'
import styles from "./UpcomingMission.module.css"

interface IUpcomingMission {
    mission_image: string,
    mission_name: string,
    reward_stars: number,
    mission_exp_date: string,
    id: number
}
function UpcomingMission(props: IUpcomingMission){
    const [enterPinForm, setEnterPinForm] = useState(false)
    const [changeStatus, setchangeStatus] = useState("")
    // useEffect(() => {
    //     if(!props.mission_exp_date) return 
    //     console.log('check date', props.mission_exp_date.substring(10))
    // }, [props.mission_exp_date])
    // console.log(props.mission_exp_date.toISOString())
    // let formatted_date =  ((props.mission_exp_date).getMonth() + 1) + "-" + (props.mission_exp_date).getDate()
    return (

        <div className={styles.mission_card}>
                    <img className={styles.mission_card_img} src={`./img/${props.mission_image}`} alt=""></img>
                    <div className={styles.mission_details}>
                        <h5 className={styles.mission_title}>{props.mission_name}</h5>
                        <div className={styles.mission_star}>獎勵：⭐️ X {props.reward_stars}</div>
                        <div className={styles.mission_deadline}><p> 限期：{props.mission_exp_date}</p></div>
                           <div className={styles.complete_list_div}> <div className={styles.complete_list}><button onClick={()=> {setEnterPinForm(true); setchangeStatus("complete")}}><img src={`./img/Check.png`} alt=""></img></button></div>
                    <div className={styles.complete_list}><button onClick={()=> {setEnterPinForm(true) ; setchangeStatus("fail")}}><img src={`./img/cross.png`} alt=""></img></button></div></div>
                    </div>
                    {enterPinForm? <EnterPin reward_stars={props.reward_stars} missionId={props.id} changeStatusString={changeStatus} closeEnterPinOnClick={() => {setEnterPinForm(false)}} /> : "" }
                </div>
    )
}

export default UpcomingMission