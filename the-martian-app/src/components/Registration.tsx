import { Container, Row, Col, Spinner } from 'reactstrap';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../redux/auth/action'
import style from './Registration.module.css';
import { push } from 'connected-react-router';
import { RootState } from '../store';
import RegistrationPinInfo from './RegistrationPinInfo';

export function Registration() {

    const today = new Date().toISOString().split("T")[0];
    console.log(today)

    const displayIconImg = {
        1: "./img/profileIcon/astronaut-girl-1-light-skin-tone.png",
        2: "./img/profileIcon/astronaut-girl-2-medium-light-skin-tone.png",
        3: "./img/profileIcon/astronaut-girl-3-medium-dark-skin-tone.png",
        4: "./img/profileIcon/astronaut-boy-1-light-skin-tone.png",
        5: "./img/profileIcon/astronaut-boy-2-medium-light-skin-tone.png",
        6: "./img/profileIcon/astronaut-boy-3-medium-dark-skin-tone.png"
    }

    const [loginName, setLoginName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [nickname, setNickname] = useState('');
    const [gender, setGender] = useState('不透露');
    const [birthday, setBirthday] = useState('不透露');
    const [email, setEmail] = useState('');
    const [pin, setPin] = useState('');
    const [iconId, setIconId] = useState(1);
    const [displayIcon, setDisplayIcon] = useState(`${displayIconImg[1]}`)

    const dispatch = useDispatch();
    const isLoading = useSelector((state: RootState) => state.auth.isLoading)

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    useEffect(() => {

        switch (displayIcon) {
            case `${displayIconImg[1]}`:
                return setIconId(1)


            case `${displayIconImg[2]}`:
                return setIconId(2)


            case `${displayIconImg[3]}`:
                return setIconId(3)


            case `${displayIconImg[4]}`:
                return setIconId(4)


            case `${displayIconImg[5]}`:
                return setIconId(5)


            case `${displayIconImg[6]}`:
                return setIconId(6)

        }

    }, [displayIcon, displayIconImg])

    return (

        <Container className={style.main}>

            {isLoading &&
                <div className={style.loading}>
                    <Spinner size="l" color="secondary" />
                </div>
            }

            {!isLoading &&
                <Row>
                    <Col>
                        <div className={style.mainTitle}><h3 >註冊</h3></div>
                    </Col>
                </Row>

            }

            {!isLoading &&
                <Row>
                    <Col>
                        <form onSubmit={(event) => {
                            event.preventDefault();

                            if (password !== confirmPassword) {
                                alert("兩次輸入的密碼不一致，請重新輸入。")
                                return;
                            }

                            if (birthday === '不透露') {
                                const nullbirthday = '1890-01-01'
                                const date_of_birth = new Date(nullbirthday);
                                dispatch(register(loginName, password, nickname, gender, date_of_birth, email, parseInt(pin), iconId))
                                return
                            }

                            const today = new Date();
                            const date_of_birth = new Date(birthday);

                            if (today < date_of_birth) {
                                alert("出生日期無效");
                                return;
                            }

                            dispatch(register(loginName, password, nickname, gender, date_of_birth, email, parseInt(pin), iconId))

                        }}>

                            <div className={style.registrationForm}>
                                <img className={style.rocketImage} src={"./img/rocket.png"} alt="" />
                                <div>
                                    <label >登入名稱</label>
                                    <input onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} className={style.formInput} type="text"
                                        pattern="(?=.*[a-z])(?!.*\s)[A-Za-z0-9]{4,20}" title="登入名稱必須包含至少一個英文字母，長度應介於4至20個字元；不能含有空格或特殊字元。"
                                        placeholder={"4-20位字母及數字"}
                                        required value={loginName} onChange={event => { setLoginName(event.currentTarget.value) }}></input>
                                </div>
                                <div>
                                    <label>密碼</label>
                                    <input onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} className={style.formInput} type="password"
                                        // pattern="(?=.*\d)(?=.*[a-zA-z])(?!.*\s).{8,}" title="密碼必須包含數字及英文字母，長度不得少於8個字元，及不能含有空格。" 
                                        // pattern="(?!.*\s).{8,}" title="密碼長度不得少於8個字元，及不能含有空格。"
                                        pattern="(?!.*\s).{6,}" title="密碼長度不得少於6個字元，及不能含有空格。" // how about this?
                                        required value={password} onChange={event => { setPassword(event.currentTarget.value) }} placeholder={"請輸入8位數字密碼"}></input>
                                </div>
                                <div>
                                    <label>確認密碼</label>
                                    <input onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} className={style.formInput} type="password" required value={confirmPassword} onChange={event => { setConfirmPassword(event.currentTarget.value) }} placeholder={"請輸入8位數字密碼"}></input>
                                </div>
                                <div>
                                    <label>用戶暱稱</label>
                                    <input onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} className={style.formInput} type="text" required value={nickname} onChange={event => { setNickname(event.currentTarget.value) }}></input>
                                </div>
                                <div>
                                    <label>性別</label>
                                    <select onSelect={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} className={style.genderSelect} required value={gender} onChange={event => { setGender(event.currentTarget.value) }}>
                                        <option value="" disabled selected >Select an option</option>

                                        <option value="male" >男</option>
                                        <option value="female">女</option>
                                        <option value="不透露">不透露</option>
                                    </select>
                                </div>
                                <div>
                                    <label>出生日期</label>
                                    <input onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} className={style.formInput} type="text" onFocus={(e) => { if(e.target.type === 'text'){
                                        e.target.type = 'date' ; e.target.value = today ; setBirthday(today)  
                                    }}} value={birthday} onChange={event => { setBirthday(event.currentTarget.value) }}></input>
                                </div>
                                <div>
                                    <label>電郵</label>
                                    <input onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} className={style.formInput} type="email" required value={email} onChange={event => { setEmail(event.currentTarget.value) }}></input>
                                </div>
                                <div>
                                    <label>任務密碼
                                         {/* <div className={style.pinInfoButton} onClick={() => setShowPinInfo(!showPinInfo)}>ⓘ</div> */}
                                         <span className={style.pinInfoButton} onClick={toggle}>ⓘ</span>
                                            {modal && <RegistrationPinInfo modal={modal} onChange={() => setModal(false)} />}
                                    </label>
                                    <input onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} className={style.formInput} type="password"
                                        pattern="(?=.*[0-9]).{4,4}$" title="任務密碼只可輸入數字，密碼長度為4個字元。"
                                        placeholder="請輸入4位數字密碼"
                                        required value={pin} onChange={event => { setPin(event.currentTarget.value) }}></input>

                                </div>

                                {/* {showPinInfo &&

                                    <div className={style.pinInfoContainer}>
                                        <label></label>
                                        <div className={style.pinInfo}>
                                            <div className={style.pinContent}>登入後，家長和小朋友可設定任務。當小朋友完成任務時，需輸入任務密碼確認完成。</div>
                                        </div>
                                    </div>
                                } */}

                                <div>
                                    <label>頭像</label>

                                    <div className={style.selectCheckbox} >

                                        <div> <img className={style.displayIcon} src={displayIcon} alt="" /></div>
                                        <div>
                                            {/* Girl Icon */}
                                            <label className={style.selectIcon}>
                                                <input onBlur={() => {
                                                    window.scrollTo(0, 0);
                                                    document.body.scrollTop = 0;
                                                }} type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[1]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[1]}`} alt="" />
                                            </label>

                                            <label className={style.selectIcon}>
                                                <input onBlur={() => {
                                                    window.scrollTo(0, 0);
                                                    document.body.scrollTop = 0;
                                                }} type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[2]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[2]}`} alt="" />
                                            </label>
                                            <label className={style.selectIcon}>
                                                <input onBlur={() => {
                                                    window.scrollTo(0, 0);
                                                    document.body.scrollTop = 0;
                                                }} type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[3]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[3]}`} alt="" />
                                            </label>

                                            {/* Boy Icon */}
                                            <label className={style.selectIcon}>
                                                <input onBlur={() => {
                                                    window.scrollTo(0, 0);
                                                    document.body.scrollTop = 0;
                                                }} type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[4]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[4]}`} alt="" />
                                            </label>

                                            <label className={style.selectIcon}>
                                                <input onBlur={() => {
                                                    window.scrollTo(0, 0);
                                                    document.body.scrollTop = 0;
                                                }} type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[5]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[5]}`} alt="" />
                                            </label>
                                            <label className={style.selectIcon}>
                                                <input onBlur={() => {
                                                    window.scrollTo(0, 0);
                                                    document.body.scrollTop = 0;
                                                }} type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[6]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[6]}`} alt="" />
                                            </label>

                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div><input onBlur={() => {
                                window.scrollTo(0, 0);
                                document.body.scrollTop = 0;
                            }} className={style.submitButton} type="submit" value={"提交"}></input></div>
                        </form>

                        <div><input onBlur={() => {
                            window.scrollTo(0, 0);
                            document.body.scrollTop = 0;
                        }} onClick={() => {
                            dispatch(push('/homepage'))
                        }} className={style.submitButton} value={"返回"} readOnly></input></div>
                    </Col>
                </Row>}

            <Row>
                <div className={style.bottomSpace}></div>
            </Row>

        </Container>
    )
}