import { push } from "connected-react-router";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Row, Col } from 'reactstrap';
import { logout } from "../redux/auth/action";
import { clearSavingItemData } from "../redux/saving/action";
import { clearSavingPlanData } from "../redux/savingPlan/action";
import { clearUserInfoState, getUserInfo } from "../redux/User/action"
import { RootState } from "../store";
import style from "./Base.module.css"


export function Base() {
  const dispatch = useDispatch();
  let birthday:string
  let formattedBirthday:string
  const userInfo = useSelector((state: RootState) => state.userInfo.userInfo)
  const avatarId = userInfo.avatar_id;

  if(userInfo.date_of_birth.substring(0,2) !== '18') {
        birthday = new Date(userInfo.date_of_birth).toLocaleDateString();
        formattedBirthday = birthday.replace(/\//g, "-");
  } else {
        formattedBirthday = '不透露'
  }
  
  const joinDate = new Date(userInfo.created_at).toLocaleDateString();
  const formattedJoinDate = joinDate.replace(/\//g, "-");

  const displayIconImg = {
    1: "./img/profileIcon/astronaut-girl-1-light-skin-tone.png",
    2: "./img/profileIcon/astronaut-girl-2-medium-light-skin-tone.png",
    3: "./img/profileIcon/astronaut-girl-3-medium-dark-skin-tone.png",
    4: "./img/profileIcon/astronaut-boy-1-light-skin-tone.png",
    5: "./img/profileIcon/astronaut-boy-2-medium-light-skin-tone.png",
    6: "./img/profileIcon/astronaut-boy-3-medium-dark-skin-tone.png"
  }

  const [displayIcon, setDisplayIcon] = useState("")

  useEffect(() => {

    switch (avatarId) {
      case 1:
        return setDisplayIcon(displayIconImg[1])

      case 2:
        return setDisplayIcon(displayIconImg[2])

      case 3:
        return setDisplayIcon(displayIconImg[3])

      case 4:
        return setDisplayIcon(displayIconImg[4])

      case 5:
        return setDisplayIcon(displayIconImg[5])

      case 6:
        return setDisplayIcon(displayIconImg[6])

    }
  }, [avatarId])

  useEffect(() => {
    dispatch(getUserInfo())
  }, [])

  return (

    <Container className={style.main}>
      <Row>
        <Col>
          <div className={style.mainTitle}><h3>我的基地</h3></div>
        </Col>
      </Row>

      <Row>
        <Col className={style.cardContainer}>

          <div className={style.card}>

            <div className={style.cardContent}>

              <div className={style.avatarContainer}>
                <img className={style.avatar} src={displayIcon} alt="avatarImg" />
              </div>


              {/* User Details */}
              <div className={style.details}>
                <div className={style.nickname}>{userInfo.nickname}</div>

                <div className={style.contentItem}>
                  <div className={style.smallTitle}>電郵</div>
                  <div className={style.item}>{userInfo.email}</div>
                </div>

                <div className={style.contentItem}>
                  <div className={style.smallTitle}>性別</div>
                  <div className={style.item}>{userInfo.gender}</div>
                </div>

                <div className={style.contentItem}>
                  <div className={style.smallTitle}>生日</div>
                  <div className={style.item}>{formattedBirthday}</div>
                </div>

                <div className={style.contentItem}>
                  <div className={style.smallTitle}>註冊日期</div>
                  <div className={style.item}>{formattedJoinDate}</div>
                </div>

                <div className={style.contentItem}>
                  <div className={style.smallTitle}>任務經驗</div>
                  {userInfo.total_star == null ?
                    <div className={style.item}> 0 ⭐️ </div> :
                    <div className={style.item}> {userInfo.total_star} ⭐️ </div>}
                </div>

              </div>

            </div>

          </div>

          <div className={style.cardBottom}></div>

        </Col>
      </Row>

      <Row>
        <Col>
          <div className={style.buttons}>
            {/* <button className={style.logoutButton} onClick={() => (dispatch(push('/savingPlan')))}> 前往儲蓄大計  </button> */}
            <button className={style.logoutButton} onClick={() => (dispatch(push('/editProfile')))}> 修改用戶資料 </button>
            <button className={style.logoutButton} onClick={() => (dispatch(push('/editPassword')))}> 修改密碼 </button>
            <button className={style.logoutButton} onClick={() => (dispatch(push('/editMissionPin')))}> 修改任務密碼 </button>
            <button className={style.logoutButton}
              onClick={() => {
                dispatch(clearSavingPlanData());
                dispatch(clearUserInfoState());
                dispatch(clearSavingItemData());
                dispatch(logout());
              }}>登出</button>
          </div>
        </Col>
      </Row>

    </Container>

  )
}