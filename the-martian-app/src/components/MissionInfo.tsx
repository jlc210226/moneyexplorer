import React, { useEffect, useState } from 'react'
import style from './MissionInfo.module.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default function MissionInfo(props: {
    modal: boolean;
    onChange: () => void;
}) {

    const [modal] = useState(false);
    const [content, setContent] = useState(1);

    
    useEffect(() => {
    }, [modal, content])

    return (
        <>
            <div>

                <Modal isOpen={props.modal} className={style.infoContainer}>
                    {content === 1 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>翻看待辦任務，歷史任務及修改狀態</ModalHeader>
                            <ModalBody>
                                <div><img src={"./img/info/missionInfo1.png"} width="100%" alt=""/></div>  
                                <p>按✓表示完成任務，按X表示任務失敗，家長輸入pin碼後即可以修改任務狀態</p>
                                <div><img src={"./img/info/missionInfo2.png"} width="100%" alt=""/></div>  
                            </ModalBody>
                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(2)} className={style.infoboxButton}>下一頁</Button>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>
                        </>}

                    {content === 2 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>選擇任務</ModalHeader>
                            <ModalBody>
                                <p>從預設模板或自訂模板選擇任務，再輸入獎勵星星數目及完成日期，即可新增任務</p>
                                <span><img src={"./img/info/missionInfo3.png"} width="100%" alt=""/></span>
                                <span><img src={"./img/info/missionInfo4.png"} width="100%" alt=""/></span>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(3)} className={style.infoboxButton}>下一頁</Button>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>
                        </>}

                    {content === 3 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>自訂任務</ModalHeader>
                            <ModalBody>
                                <p>輸入自訂任務內容，下次亦可在自訂任務模板快速選擇</p>
                                <span><img src={"./img/info/missionInfo5.png"} width="100%" alt=""/></span>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(4)} className={style.infoboxButton}>下一頁</Button>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>
                        </>}

                    {content === 4 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>獎品換領</ModalHeader>
                            <ModalBody>
                                <p>小朋友可用完成任務所賺取的星星來兌換想要的目標。先請家長跟小朋友一起訂立目標及兌換所需的星星數目，當賺取的星星數目達到所需星星數目時就會出現兌換按鈕！</p>
                                <span><img src={"./img/info/missionInfo6.png"} width="100%" alt=""/></span>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>
                        </>}

                </Modal>

            </div>
        </>
    )
}