
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createSavingGoal } from '../redux/savingPlan/action';
import style from './CreateSavingGoalModal.module.css'
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

export function CreateSavingGoalModal(props: {
    isOpen: boolean
    toggle: () => void
}) {

    const [goalName, setGoalName] = useState("");
    const [goalAmount, setGoalAmount] = useState("");
    const dispatch = useDispatch();

    return (

        <div className={style.main}>
            <Modal isOpen={props.isOpen} toggle={props.toggle} className={style.modalBox}>
                <ModalHeader toggle={props.toggle} className={style.title}>建立儲蓄目標</ModalHeader>

                <ModalBody className={style.formContainer}>

                    <form className={style.formContainer} onSubmit={(event) => {
                        event.preventDefault();
                        dispatch(createSavingGoal(goalName, goalAmount));
                        setGoalName("");
                        setGoalAmount("");

                    }}>
                        <div className={style.formContent}>
                            <div>
                                <label className={style.formLabel}>目標名稱</label>
                                <input type="text" required value={goalName} onChange={event => { setGoalName(event.currentTarget.value) }} onBlur={() => {
                                    window.scrollTo(0, 0);
                                    document.body.scrollTop = 0;
                                }} ></input>
                            </div>

                            <div>
                                <label className={style.formLabel}>目標金額</label>
                                <input type="number" required value={goalAmount} onChange={event => { setGoalAmount(event.currentTarget.value) }} onBlur={() => {
                                    window.scrollTo(0, 0);
                                    document.body.scrollTop = 0;
                                }} ></input>
                            </div>
                        </div>

                        <div className={style.buttons}>
                            <div><input className={style.submitButton} onClick={props.toggle} value={"取消"} readOnly onBlur={() => {
                                window.scrollTo(0, 0);
                                document.body.scrollTop = 0;
                            }} ></input></div>
                            <div><input className={style.submitButton} onClick={props.toggle} type="submit" value={"提交"} onBlur={() => {
                                window.scrollTo(0, 0);
                                document.body.scrollTop = 0;
                            }} ></input></div>

                        </div>

                    </form>



                    {/* <Button color="secondary" onClick={props.toggle}>Cancel</Button> */}

                </ModalBody>


            </Modal>
        </div>

    )
}