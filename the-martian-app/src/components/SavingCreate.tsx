import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
// import { Input, InputGroup, InputGroupAddon } from 'reactstrap';
import { createSaving } from '../redux/saving/action';
import { RootState } from '../store';
// import { RootState } from '../store';
import style from './Saving.module.css';
import styles from './SavingCreate.module.css';
import { Toast, ToastBody, ToastHeader } from 'reactstrap';
import { push } from 'connected-react-router';

export function SavingCreate(props: {
    // onBack: () => void;
}) {

    const dispatch = useDispatch();

    let [category, setCategory] = useState('')
    let [newID] = useState(0)
    let [newDate, setNewDate] = useState(`${new Date().getFullYear()}-0${new Date().getMonth() + 1}-${new Date().getDate()}`)
    let [newItem, setNewItem] = useState('')
    let [newAmount, setNewAmount] = useState('')

    const totalSavingObj = useSelector(
        (state: RootState) => state.totalSaving.totalSaving)

    let getThisMonthSaving = totalSavingObj.filter(
        (item) => new Date(Date.parse(item.date_trunc)).getMonth() === new Date().getMonth()
    )

    useEffect(() => {
    }, [getThisMonthSaving.length, newDate])

    // const savingObj = useSelector((state: RootState) => state.saving.saving)

    return (
        <div className={style.container}>

            <h3>新增項目</h3>

            <div className={style.createContainer}>

                <img src={"./img/saving-ball.png"} className={style.imgBall} alt="" />

                <form onSubmit={async (event) => {
                    event.preventDefault()
                    await dispatch(createSaving(newID, category, newDate, newItem, parseInt(newAmount)))
                    // const timeStamp = Date.parse(newDate);
                    // const dateToFormat = new Date(timeStamp)
                    // event.currentTarget.reset();
                    setCategory('');
                    setNewItem('');
                    setNewDate(Date());
                    setNewAmount('');
                    // setPage(props.page)
                }} >

                    <p>
                        <input type="date" value={newDate} onChange={
                            (event) => { setNewDate(event.currentTarget.value) }
                        } className={style.createInput} required>
                        </input>
                    </p>

                    {getThisMonthSaving.length === 0
                        ?
                        <div>
                            <p className={style.ToastBox}>
                                <Toast>
                                    <ToastHeader icon="info" className={style.ToastBoxText}>
                                        記得為自己訂立每月儲蓄目標啊！
                        </ToastHeader>
                                </Toast>
                            </p>
                        </div>
                        : ''}

                    <p className={style.createTypeButtonContainer}>
                        <div onClick={() => setCategory('income')}
                            className={category === 'income'
                                ? style.createTypeButtonActive
                                : style.createTypeButton}>
                            收入
                        </div>
                        <div onClick={() => setCategory('expense')}
                            className={category === 'expense'
                                ? style.createTypeButtonActive
                                : style.createTypeButton}>
                            支出
                        </div>
                        <div onClick={() => setCategory('saving')} className={category === 'saving'
                            ? style.createTypeButtonActive
                            : style.createTypeButton}>
                            儲蓄
                        </div>
                    </p>

                    {category === '' &&
                        <div>
                            <p>請選擇項目</p>
                        </div>
                    }

                    {category === 'income' &&
                        <div>
                            <p><select onSelect={() => {
                                setTimeout(function () {
                                    window.scrollTo(0, 0);
                                    document.body.scrollTop = 0;
                                }, 200);
                            }} value={newItem} onChange={(event) => { setNewItem(event.currentTarget.value) }} className={style.createInput} required>
                                <option>請選擇類別</option>
                                <option>零用錢</option>
                                <option>額外獎勵</option>
                                <option>其他</option>
                            </select>
                            </p>
                        </div>
                    }

                    {category === 'expense' &&
                        <div>
                            <p><select onSelect={() => {
                                setTimeout(function () {
                                    window.scrollTo(0, 0);
                                    document.body.scrollTop = 0;
                                }, 200);
                            }} value={newItem} onChange={(event) => { setNewItem(event.currentTarget.value) }} className={style.createInput} required>
                                <option>請選擇類別</option>
                                <option>食物</option>
                                <option>衣服</option>
                                <option>交通</option>
                                <option>娛樂</option>
                                <option>禮物</option>
                                <option>其他</option>
                            </select>
                            </p>
                        </div>}

                    {category === 'saving' &&
                        <div>
                            <p>
                                <select onSelect={() => {
                                    setTimeout(function () {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }, 200);
                                }} value={newItem} onChange={(event) => { setNewItem(event.currentTarget.value) }} className={style.createInput} required>
                                    <option>請選擇類別</option>
                                    <option>儲蓄</option>
                                </select>
                            </p>
                        </div>
                    }

                    <p className={style.createText}>輸入金額</p>

                    <p>

                        <input onBlur={() => {
                            window.scrollTo(0, 0);
                            document.body.scrollTop = 0;
                        }} type="number" min="1" max="2000000000" value={newAmount} onChange={(event) => { setNewAmount(event.currentTarget.value) }} className={style.createInputAmount} required />

                    </p>

                    <button className={style.createSubmit} type="submit">提交</button>

                </form>

            </div>

            {/* <p><button onClick={props.onBack} className="button">返回</button></p> */}

            {<p><button onClick={() => {
                dispatch(push('/saving'))
            }} className="button">返回</button>
            </p>}

        </div>
    )
}

export default SavingCreate;
