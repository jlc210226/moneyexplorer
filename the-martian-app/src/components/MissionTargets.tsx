import React from "react"
import { useSelector } from "react-redux"
import { RootState } from "../store"
import styles from "./MissionTarget.module.css"
import RedeemButton from "./RedeemButton"

export default function MissionTargets() {
    const missionTargets = useSelector((state: RootState) => state.missionTargets.missionTargets)
    const starNum = useSelector((state: RootState) => state.currentStar.currentStars)
    const currentMissionTargets = missionTargets.filter(target => target.is_complete === false)
    console.log(currentMissionTargets)
    return (


        <div className={styles.board}>
            <div className={styles.singleMissionTargetBody}>
                {currentMissionTargets.map(target =>
                    <div className={styles.singleMissionTarget} key={target.id}>
                        <p className={styles.mission_target_name}>{target.target_name}</p>
                        <p className={styles.mission_target_name}><span className={styles.star}>★</span>{target.needed_star}星</p>
                        {target.needed_star <= starNum? <RedeemButton target_id={target.id} needed_star={target.needed_star} /> : ""}
                    </div>)}

            </div>
        </div >
    )
}