import style from './CompletedPlanItem.module.css';

export function CompletedPlanItem(props: {
    user_id: number;
    goal_id: number;
    goal_name: string;
    goal_amount: number;
    updated_at: string;
    onShowDelete: boolean;
    onDelete: () => void;
}) {

    return (
        <div className={style.savingItem}>
            <div className={style.savingItemDetails}>
                <div className={style.title} >{props.goal_name}</div>
                <div className={style.column}>完成金額：<span className={style.amount}>${props.goal_amount}</span></div>
                <div className={style.column}>完成日期：{props.updated_at}</div>
            </div>

            {props.onShowDelete && <div className={style.deleteButton} onClick={props.onDelete}>❌</div>}
        </div>
    )
}