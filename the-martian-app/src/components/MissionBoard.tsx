import styles from './MissionBoard.module.css';
import UpcomingMissions from './UpcomingMissions';
import CurrentStars from './CurrentStars'
import { Route, Switch } from 'react-router-dom';
import MissionBoardNavBar from './MissionBoardNavBar';
import HistoryMissions from './HistoryMissions';
import React, { useEffect, useState } from 'react';
import AddMissionBoard from './AddMissionBoard';
import MissionTargetBoard from './MissionTargetBoard';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUpcomingMissions } from '../redux/upcomingMissions/action';
import MissionInfo from './MissionInfo';
import { RootState } from '../store';
import { push } from 'connected-react-router';
import { Spinner } from 'reactstrap';

export default function MissionBoard() {
    const dispatch = useDispatch()
    const [page, setPage] = useState('')

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const isLoading = useSelector((state: RootState) => state.upcomingMissions.isLoading)
    useEffect(() => {
        dispatch(fetchUpcomingMissions())
    }, [dispatch])
    return (
        <div className={styles.outerBoard}>
            {
                isLoading && <div className={styles.loading}>
                   <Spinner size="l" color="secondary" />
                </div>
            }
            {page === "" && <div className={styles.board}>
                <div className={styles.header}>
                    <div><h3 className={styles.page_title}>任務星球</h3></div>
                    <span className={styles.infobox} onClick={toggle}>ⓘ</span>
                    {modal && <MissionInfo modal={modal} onChange={() => setModal(false)} />}
                    <CurrentStars></CurrentStars>
                </div>
                <MissionBoardNavBar />
                <Switch>
                    <Route path="/mission" exact>
                        <UpcomingMissions />
                    </Route>
                    <Route path="/mission/historyMissions">
                        <HistoryMissions />
                    </Route>
                </Switch>
                <div className={styles.functionButton}><button onClick={() => {
                    dispatch(push('/addMission'))
                }} className={styles.add_mission} >新增任務</button>
                    <button onClick={() => { setPage('missionTarget') }} className={styles.add_mission} >獎品換領</button></div>
            </div>}

            {/* {
                page === 'addMission' &&
                <AddMissionBoard onBack={() => {
                    setPage('')
                }}/>
            } */}
            {
                page === 'missionTarget' &&
                <MissionTargetBoard onBack={() => {
                    setPage('')
                }} />
            }
        </div>
    )
}