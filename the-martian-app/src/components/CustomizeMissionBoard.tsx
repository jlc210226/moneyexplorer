
import CustomizeMission from "./CustomizeMission"
import styles from "./CustomizeMissionBoard.module.css"

export default function CustomizeMissionBoard(props: {
    onBack: () => void;
}) {
    return (
        <div className={styles.board}>
            <h2 className={styles.page_title}>自訂任務</h2>
            {/* <img src={"/img/navbar-1.png"}></img> */}
            <CustomizeMission />
            <p><button onClick={props.onBack} className="button">返回</button></p>
        </div>
    )
}