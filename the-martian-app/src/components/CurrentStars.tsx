import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import styles from "./CurrentStars.module.css";
import {RootState} from '../store'
import { fetchCurrentStar } from '../redux/currentStars/action';
export default function CurrentStars () {
    const starNum = useSelector((state: RootState) => state.currentStar.currentStars)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchCurrentStar())
      }, [dispatch])
    
    return (
        <div>
            <div className={styles.stars}>
            <span className={styles.starSymbol}>⭐️</span> 可用星星： {starNum}
            </div>
        </div>
    )
}