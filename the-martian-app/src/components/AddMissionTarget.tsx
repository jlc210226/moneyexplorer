import styles from './AddMissionTarget.module.css'
import { useForm } from "react-hook-form";
import { addNewMissionTarget } from '../redux/missionTarget/action';
import { useDispatch } from 'react-redux';

interface addMissionTargetData {
    target_name: string,
    needed_star: number
}
export default function AddMissionTarget() {
    const { register, handleSubmit, errors } = useForm<addMissionTargetData>();
    const dispatch = useDispatch()
    const onSubmitHandler = async (data: addMissionTargetData) => {
        dispatch(addNewMissionTarget(data.target_name, data.needed_star))
    }
    return (
        <div className={styles.board}>
            <form className={styles.AddMissionTargetForm} onSubmit={handleSubmit(onSubmitHandler)}>
                <label>任務目標<input onBlur={() => {
                    window.scrollTo(0, 0);
                    document.body.scrollTop = 0;
                }} name="target_name" type="text" ref={register({ required: true, maxLength: 10 })} /></label>
                {errors.target_name && <span className={styles.errorMessage}>請輸入正確任務目標</span>}
                <label>所需星星<input onBlur={() => {
                    window.scrollTo(0, 0);
                    document.body.scrollTop = 0;
                }} name="needed_star" type="number" ref={register({ required: true })} /></label>
                {errors.needed_star && <span className={styles.errorMessage}>請輸入正確所需星星數目</span>}
                <input type="submit" value="提交" onTouchStart={() => {
                    window.scrollTo(0, 0);
                    document.body.scrollTop = 0;
                }}></input>
            </form>
        </div>
    )
}