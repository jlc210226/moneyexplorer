import { useDispatch } from "react-redux"
import { redeemTarget } from "../redux/missionTarget/action"
import styles from "./RedeemButton.module.css"

interface IRedeemButtonProps{
    target_id: number,
    needed_star: number
}


export default function RedeemButton (props: IRedeemButtonProps) {
    const dispatch = useDispatch()
    return (
        <button className={styles.redeemButton} onClick={() => {dispatch(redeemTarget(props.target_id, props.needed_star))}}>✓ 兌換</button>
    )
}