import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Financial from './Financial';
import Investment from './Investment';
import styles from "./Knowledge.module.css";
import { KnowledgeNav } from './KnowledgeNav';
import Movie from './Movie';
import Quiz from './Quiz';

export default function Knowledge() {
    
    return (
        <div className={styles.knowledge}>
            <KnowledgeNav />
            <div className={styles.knowledgeContent}>
                <Switch>
                    <Route path="/knowledge/" exact><Financial /></Route>
                    <Route path="/knowledge/investment"><Investment /></Route>
                    <Route path="/knowledge/quiz"><Quiz /></Route>
                    <Route path="/knowledge/movie"><Movie /></Route>
                </Switch>
            </div>
        </div>
    )
}

