import React, { useEffect, useState } from 'react'
import style from './KnowledgeNav.module.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export function KnowledgeInfo(props: {
    modal: boolean;
    onChange: () => void;
}) {

    const [modal] = useState(false);
    const [content, setContent] = useState(1);

    useEffect(() => {
    }, [modal, content])

    return (
        <>
            <div>

                <Modal isOpen={props.modal} className={style.infoContainer}>
                    {content === 1 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>投資及理財知識</ModalHeader>
                            <ModalBody>
                                <p>每天閱讀投資理財知識。</p>
                                <span><img src={"./img/info/knowledgeInfo1.jpg"} width="100%" alt="knowledgeInfoImg" /></span>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(2)} className={style.infoboxButton}>下一頁</Button>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>
                        </>}

                    {content === 2 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>理財小測驗</ModalHeader>
                            <ModalBody>
                                <p>每天完成理財小測驗可獲得 ⭐️ 獎勵。</p>
                                <span><img src={"./img/info/knowledgeInfo3.jpg"} width="100%" alt="knowledgeInfoImg"/></span>

                            </ModalBody>
                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(3)} className={style.infoboxButton}>下一頁</Button>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>
                        </>}

                    {content === 3 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>教育短片</ModalHeader>
                            <ModalBody>
                                <p>有趣短片，讓學習更有趣</p>
                                <span><img src={"./img/info/knowledgeInfo4.jpg"} width="100%" alt="knowledgeInfoImg"/></span>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>
                        </>}

                </Modal>

            </div>
        </>
    )
}

