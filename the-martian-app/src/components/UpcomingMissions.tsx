import styles from './UpcomingMissions.module.css';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
// import React, { useState } from 'react';
// import EnterPin from './EnterPin';
import UpcomingMission from './UpcomingMission';


export default function UpcomingMissions() {
    // const [enterPinForm, setEnterPinForm] = useState(false)
    // const [changeStatus, setchangeStatus] = useState("")
     const allMissions = useSelector((state: RootState) => state.upcomingMissions.upComingMissions)
     console.log(allMissions)
     const upComingMissions = allMissions.filter(mission => mission.mission_status==="in progress" && mission.expired === false)

    return (
        <>
        <div className={styles.mission_card_board}>
            {
                upComingMissions.map(mission =>  <UpcomingMission id={mission.id} reward_stars={mission.reward_stars} mission_image={mission.mission_image} mission_name={mission.mission_name} mission_exp_date={mission.mission_exp_date} key={`upcomingMission-${mission.id}`} />)
            }
        </div>
        </>
    )
}