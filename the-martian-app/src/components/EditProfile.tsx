import { Container, Row, Col } from 'reactstrap';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { editProfile } from '../redux/User/action';
import { getUserInfo } from '../redux/User/action';
import { RootState } from '../store';
import style from './EditProfile.module.css';
import { push } from 'connected-react-router';

export function EditProfile() {

    const displayIconImg = {
        1: "./img/profileIcon/astronaut-girl-1-light-skin-tone.png",
        2: "./img/profileIcon/astronaut-girl-2-medium-light-skin-tone.png",
        3: "./img/profileIcon/astronaut-girl-3-medium-dark-skin-tone.png",
        4: "./img/profileIcon/astronaut-boy-1-light-skin-tone.png",
        5: "./img/profileIcon/astronaut-boy-2-medium-light-skin-tone.png",
        6: "./img/profileIcon/astronaut-boy-3-medium-dark-skin-tone.png"
    }

    const dispatch = useDispatch();
    const userInfo = useSelector((state: RootState) => state.userInfo.userInfo);
    const currentIcon = userInfo.avatar_id;

    const temp = Date.parse(userInfo.date_of_birth);
    const dateToFormat = new Date(temp);
    const formattedDate = (dateToFormat.getFullYear()) + "/" + ((dateToFormat).getMonth() + 1) + "/" + ((dateToFormat).getDate())

    // Edit Div Show or Hidden
    const [editNickname, setEditNickname] = useState(false);
    const [editGender, setEditGender] = useState(false);
    const [editBirthday, setEditBirthday] = useState(false);
    const [editIcon, setEditIcon] = useState(false);

    // Edit Form Item
    const [nickname, setNickname] = useState(userInfo.nickname);
    const [gender, setGender] = useState(userInfo.gender);
    const [birthday, setBirthday] = useState(formattedDate);
    const [iconId, setIconId] = useState(1);
    const [displayIcon, setDisplayIcon] = useState(`${(displayIconImg as any)[currentIcon!]}`);

    console.log("user birthday",birthday)
    console.log("DFB",userInfo.date_of_birth)


    useEffect(() => {
        setNickname(userInfo.nickname);
        setGender(userInfo.gender);
        setBirthday(formattedDate);
        setDisplayIcon(`${(displayIconImg as any)[currentIcon!]}`);
    }, [userInfo.nickname, userInfo.gender, userInfo.date_of_birth, currentIcon])

    useEffect(() => {
        dispatch(getUserInfo())
    }, [])

    useEffect(() => {

        switch (displayIcon) {
            case `${displayIconImg[1]}`:
                return setIconId(1)

            case `${displayIconImg[2]}`:
                return setIconId(2)

            case `${displayIconImg[3]}`:
                return setIconId(3)

            case `${displayIconImg[4]}`:
                return setIconId(4)

            case `${displayIconImg[5]}`:
                return setIconId(5)

            case `${displayIconImg[6]}`:
                return setIconId(6)
        }

    }, [displayIcon])



    return (

        <Container className={style.main}>
            <Row>
                <Col>
                    <div className={style.mainTitle}><h3>修改用戶資料</h3></div>
                </Col>
            </Row>

            <Row>

                <Col>
                    <form className={style.formContainer} onSubmit={(event) => {
                        event.preventDefault();

                        console.log("form birth", birthday)

                        const today = new Date();
                        // const temp = Date.parse(birthday);
                        const dateToFormat = new Date(birthday);
                        const formattedDate = (dateToFormat.getFullYear()) + "/" + ((dateToFormat).getMonth() + 1) + "/" + ((dateToFormat).getDate() + 1)
                        const date_of_birth = new Date(formattedDate);

                        console.log("dateToFormat", dateToFormat);
                        console.log("formattedDate", formattedDate);
                        console.log("date_of_birth", date_of_birth)

                    
                        if (today < date_of_birth) {
                            alert("出生日期無效");
                            return;
                        }

                        dispatch(editProfile(nickname, gender, date_of_birth, iconId))

                    }}>

                        <div className={style.formContent}>

                            {
                                !editNickname &&
                                <div onClick={() => { setEditNickname(true) }}>
                                    <label>暱稱</label>
                                    <input className={style.inputColumn} type="text" required value={userInfo.nickname} readOnly onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} ></input>
                                </div>
                            }

                            {
                                editNickname && <div>
                                    <label>暱稱</label>
                                    <input className={style.inputColumn} type="text" required value={nickname} placeholder={userInfo.nickname} onChange={event => { setNickname(event.currentTarget.value) }} onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} ></input>
                                </div>
                            }

                            {
                                !editGender &&
                                <div onClick={() => { setEditGender(true) }}>
                                    <label>性別</label>
                                    <input className={style.inputColumn} type="text" required value={userInfo.gender} readOnly onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} ></input>
                                </div>
                            }

                            {
                                editGender && <div>
                                    <label>性別</label>
                                    <select className={style.genderSelect} required value={gender} onChange={event => { setGender(event.currentTarget.value) }} onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} >
                                        <option value="" disabled  >Select an option</option>
                                        <option value="male" >男</option>
                                        <option value="female">女</option>
                                    </select>
                                </div>
                            }


                            {/* {
                                !editEmail &&
                                <div onClick={() => { setEditEmail(true) }}>
                                    <label>電郵</label>
                                    <input className={style.inputColumn} type="text" required value={userInfo.email} readOnly></input>
                                </div>
                            }

                            {
                                editEmail && <div>
                                    <label>電郵</label>
                                    <input className={style.inputColumn} type="email" required value={email} placeholder={userInfo.email} onChange={event => { setEmail(event.currentTarget.value) }}></input>
                                </div>
                            } */}


                            {
                                !editBirthday && <div onClick={() => { setEditBirthday(true) }}>
                                    <label>出生日期</label>
                                    <input className={style.inputColumn} type="text" required value={birthday} readOnly onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} ></input>
                                </div>
                            }

                            {
                                editBirthday && <div>
                                    <label>出生日期</label>
                                    <input className={style.inputColumn} type="date" required value={birthday} onChange={event => { setBirthday(event.currentTarget.value)}} onBlur={() => {
                                        window.scrollTo(0, 0);
                                        document.body.scrollTop = 0;
                                    }} ></input>
                                </div>
                            }


                            {
                                displayIcon !== undefined && !editIcon && <div onClick={() => { setEditIcon(true) }}>
                                    <label>頭像</label>
                                    <div className={style.inputColumn}> <img className={style.displayIcon} src={displayIcon} alt="displayIcon" /></div>
                                </div>
                            }

                            {
                                editIcon && <div >
                                    <label>頭像</label>

                                    <div className={style.selectCheckbox} >

                                        <div> <img className={style.displayIcon} src={displayIcon} alt="displayIcon" /></div>
                                        <div>

                                            <label className={style.selectIcon}>
                                                <input type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[1]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[1]}`} alt="displayIcon" />
                                            </label>

                                            <label className={style.selectIcon}>
                                                <input type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[2]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[2]}`} alt="displayIcon" />
                                            </label>
                                            <label className={style.selectIcon}>
                                                <input type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[3]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[3]}`} alt="displayIcon" />
                                            </label>


                                            <label className={style.selectIcon}>
                                                <input type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[4]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[4]}`} alt="displayIcon" />
                                            </label>

                                            <label className={style.selectIcon}>
                                                <input type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[5]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[5]}`} alt="displayIcon" />
                                            </label>
                                            <label className={style.selectIcon}>
                                                <input type="radio" name="select" value={iconId} onChange={event => { setDisplayIcon(displayIconImg[6]) }} />
                                                <img className={style.profileIcon} src={`${displayIconImg[6]}`} alt="displayIcon" />
                                            </label>

                                        </div>

                                    </div>
                                </div>
                            }

                        </div>

                        <div><input className={style.submitButton} type="submit" value={"提交"} ></input></div>

                    </form>

                    <div><input onClick={() => {
                        dispatch(push('/base'))
                    }} className={style.submitButton} value={"返回"} readOnly></input></div>

                </Col>
            </Row>
        </Container>
    )
}