import React, { useEffect, useState } from 'react'
// import { useSelector } from 'react-redux';
// import { RootState } from '../store';
import style from './Saving.module.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export function SavingInfo(props: {
    modal: boolean;
    onChange: () => void;
}) {

    const [modal] = useState(false);
    const [content, setContent] = useState(1);

    useEffect(() => {
    }, [modal, content])

    return (
        <>
            <div>

                <Modal isOpen={props.modal} className={style.infoContainer}>

                    {content === 1 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>新增記帳項目</ModalHeader>

                            <ModalBody>
                                <p>一鍵記錄每月日常開支、收入，並為自己定立儲蓄目標。</p>
                                <span><img src={"./img/info/savingInfo1.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(2)} className={style.infoboxButton}>下一頁</Button>

                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>

                        </>}

                    {content === 2 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>多個帳目類別</ModalHeader>

                            <ModalBody>
                                <p>將每月零月錢、每日各項開支等紀錄，藉此建立對理財的責任心、紀律及良好習慣。</p>
                                <span><img src={"./img/info/savingInfo2.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(3)} className={style.infoboxButton}>下一頁</Button>

                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>

                        </>}

                    {content === 3 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>每月收支列表</ModalHeader>

                            <ModalBody>
                                <p>每月收支一目了然，輕鬆掌握收支情況、規劃預算。</p>
                                <span><img src={"./img/info/savingInfo3.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(4)} className={style.infoboxButton}>下一頁</Button>

                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>

                        </>}

                    {content === 4 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>豐富精彩報表</ModalHeader>

                            <ModalBody>
                                <p>開支分佈一目了然，每月評估「需要」或「必要」。</p>
                                <span><img src={"./img/info/savingInfo4.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="secondary" 
                                onClick={props.onChange} className={style.infoboxButton}>
                                    開始使用
                                </Button>
                            </ModalFooter>

                        </>}

                </Modal>

            </div>
        </>
    )
}

