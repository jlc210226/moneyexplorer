import React, { useEffect, useState } from 'react'
import style from './Homepage.module.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export function HomepageInfo(props: {
    modal: boolean;
    onChange: () => void;
}) {

    const [modal] = useState(false);
    const [content, setContent] = useState(1);

    useEffect(() => {
    }, [modal, content])

    return (
        <>
            <div>

                <Modal isOpen={props.modal} className={style.infoContainer}>

                    {content === 1 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>Welcome To Money Explorer!</ModalHeader>

                            <ModalBody>
                                <p>這是一個親子理財平台，您可透過 Money Explorer 及早培養小朋友正確的理財行為及態度。</p>
                                <span><img src={"./img/info/homepageInfo6.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(2)} className={style.infoboxButton}>下一頁</Button>

                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>

                        </>}

                    {content === 2 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>開支記帳</ModalHeader>

                            <ModalBody>
                                <p>鼓勵孩子自行記帳，以助他們慢慢掌握收支平衡的概念。</p>
                                <span><img src={"./img/info/homepageInfo1.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(3)} className={style.infoboxButton}>下一頁</Button>

                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>

                        </>}

                    {content === 3 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>建立任務</ModalHeader>

                            <ModalBody>
                                <p>家長可設任務給予獎勵，讓小朋友明白錢來之不易。</p>
                                <span><img src={"./img/info/homepageInfo2.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(4)} className={style.infoboxButton}>下一頁</Button>

                                <Button color="secondary" onClick={props.onChange} className={style.infoboxButton}>開始使用</Button>
                            </ModalFooter>

                        </>}

                    {content === 4 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>理財知識</ModalHeader>

                            <ModalBody>
                                <p>透過每日理財小測驗，讓小朋友邊玩邊學習理財觀念。</p>
                                <span><img src={"./img/info/homepageInfo3.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(5)} className={style.infoboxButton}>下一頁</Button>

                                <Button color="secondary" 
                                onClick={props.onChange} className={style.infoboxButton}>
                                    開始使用
                                </Button>
                            </ModalFooter>

                        </>}

                    {content === 5 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>儲蓄大計</ModalHeader>

                            <ModalBody>
                                <p>一起訂立真正屬於他們的儲蓄目標，鼓勵他們努力完成儲蓄大計。</p>
                                <span><img src={"./img/info/homepageInfo4.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="info" onClick={() => setContent(6)} className={style.infoboxButton}>下一頁</Button>

                                <Button color="secondary" 
                                onClick={props.onChange} className={style.infoboxButton}>
                                    開始使用
                                </Button>
                            </ModalFooter>

                        </>}

                    {content === 6 &&
                        <>
                            <ModalHeader className={style.infoboxHeader}>開始使用</ModalHeader>

                            <ModalBody>
                                <p>立即註冊成為 Money Explorer，一起探索金錢世界啦！</p>
                                <span><img src={"./img/info/homepageInfo5.png"} width="100%" alt=""/></span>
                            </ModalBody>

                            <ModalFooter>

                                <Button color="secondary" 
                                onClick={props.onChange} className={style.infoboxButton}>
                                    開始使用
                                </Button>
                            </ModalFooter>

                        </>}

                </Modal>

            </div>
        </>
    )
}

