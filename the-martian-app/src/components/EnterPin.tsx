import React from "react";
import styles from './EnterPin.module.css';
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { changeMissionStatus } from "../redux/upcomingMissions/action";
import { RootState } from "../store";
interface pinData {
    pin: number
}
interface IEnterPinProps {
    reward_stars: number
    missionId: number,
    changeStatusString: string,
    closeEnterPinOnClick: () => void
}

export default function EnterPin(props: IEnterPinProps) {
    console.log("hi" + props.reward_stars)
    console.log("mission_status" + props.changeStatusString)
    const token = useSelector((state: RootState) => state.auth.token)
    const dispatch = useDispatch();
    const { register, handleSubmit, errors } = useForm<pinData>();
    const onSubmitHandler = async (data: pinData) => {
        console.log("enterPinFormSubmit")
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/checkPin`, {
            method: 'POST',
            headers: {
                Authorization: 'Bearer ' + token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                pin: data.pin
            })
        })
        const result = await res.json();
        console.log(result)
        if (result.message === "failed") {
            alert("wrong pin number")
            return
        } else if (result.message === "success") {
            console.log("success")
            await dispatchMissionStatus()
        }

        props.closeEnterPinOnClick()
        return
    }
    const dispatchMissionStatus = async () => {
        await dispatch(changeMissionStatus(props.reward_stars, props.changeStatusString, props.missionId))
    }

    return (
        <div className={styles.board}>
            <div className={styles.pinboard}>
                <div className={styles.header}>
                    <span>請家長輸入任務密碼</span>
                    <button onClick={props.closeEnterPinOnClick}>X</button>
                </div>
                <form onSubmit={handleSubmit(onSubmitHandler)}>
                    <input name="pin" type="password" ref={register({ required: true })} onBlur={() => {
                        window.scrollTo(0, 0);
                        document.body.scrollTop = 0;
                    }} />
                    {errors.pin && <p className={styles.errorMessage}>請輸入正確PIN碼</p>}
                    <input className={styles.submitButton} type="submit" value="提交"></input>
                </form>
            </div>
        </div>
    )
}