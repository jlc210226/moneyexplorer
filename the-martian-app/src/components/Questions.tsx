import React from 'react'
import style from './Quiz.module.css'

export interface IQuestionProps {
    selectedOption: string,
    key: number,
    question: string,
    image: string,
    options: {
        choice: string,
        choice_id: number
    }[],
    onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void,
}


export default function Question(props: IQuestionProps
    //     {
    //     selectedOption: string,
    //     key: number,
    //     question: string,
    //     image: string,
    //     options: {
    //         choice: string,
    //         choice_id: number
    //     }[],
    //     onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void,
    //  }
) {
    return (
        <div className={style.question}>
            <div>{props.question}</div>
            { props.image && <img src={"./img/knowledge/" + props.image} alt="" height="500" width="500" />}
            <select onSelect={() => {
                    window.scrollTo(0, 0);
                    document.body.scrollTop = 0;
            }} name={style.option} onChange={props.onChange} defaultValue={props.selectedOption}>
                {props.options.map((option) => {
                    return <option data-testid={option.choice_id} key={option.choice} value={JSON.stringify(option)}>{option.choice}</option>
                }
                )}
            </select>
        </div>
    )
}
