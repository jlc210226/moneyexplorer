import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Container, Row, Col } from 'reactstrap';
import useRouter from 'use-react-router';
import { logout, resetPassword } from '../redux/auth/action';
import style from './ResetPassword.module.css';

export function ResetPassword() {

    const dispatch = useDispatch();
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const router = useRouter<{ id?: string, token?: string }>()
    const userId = router.match.params.id;
    const token = router.match.params.token;

    return (
        <Container>
            <Row>
                <Col className={style.main}>
                    <div className={style.mainTitle}><h3>重設密碼</h3></div>

                    <div>請輸入新的密碼以完成登入。</div>

                    <form className={style.passwordForm} onSubmit={(event) => {
                        event.preventDefault();

                        if (password !== confirmPassword) {
                            alert("兩次輸入的密碼不一致，請重新輸入。")
                            return;
                        }

                        if (!userId || !token) {
                            return;
                        }

                        dispatch(

                            resetPassword(password, userId, token)
                        )
                    }}>

                        <div className={style.formContent}>
                            <div className={style.formItem}>
                                <label className={style.formLabel}>新密碼</label>
                                <input className={style.formInput} type="password"
                                    pattern="(?!.*\s).{8,}" title="密碼長度不得少於8個字元，及不能含有空格。"
                                    required value={password} onChange={event => { setPassword(event.currentTarget.value) }}></input>
                            </div>

                            <div >
                                <label className={style.formLabel}>重新輸入新密碼</label>
                                <input className={style.formInput} type="password" required value={confirmPassword} onChange={event => { setConfirmPassword(event.currentTarget.value) }}></input>
                            </div>

                        </div>

                        <div><input className={style.submitButton} type="submit" value={"變更密碼"}></input></div>

                    </form>

                </Col>
            </Row>
        </Container>
    )
}