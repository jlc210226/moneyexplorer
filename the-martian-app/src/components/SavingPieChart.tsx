import React from 'react'
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import style from './SavingChart.module.css';
import CanvasJSReact from '../canvasjs.react';
// var CanvasJSReact = require('./canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export function SavingPieChart(props: {

}) {

    const savingObj = useSelector((state: RootState) => state.saving.saving)
    let getMonth = savingObj.filter((item) => new Date(Date.parse(item.date)).getMonth() === new Date().getMonth())
    let getMonthCat = getMonth.filter((item) => item.category === 'expense')
    // console.log(getMonthCat.length)

    let getFood = getMonth.filter((item) => item.item === '食物')
    let getFoodAmount = getFood.map(f => f.amount)
    let foodTotal = getFoodAmount.reduce((a, b) => a + b, 0)

    let getClothes = getMonth.filter((item) => item.item === '衣服')
    let getClothesAmount = getClothes.map(f => f.amount)
    let ClothesTotal = getClothesAmount.reduce((a, b) => a + b, 0)

    let getTransportation = getMonth.filter((item) => item.item === '交通')
    let getTransportationAmount = getTransportation.map(f => f.amount)
    let transportationAmount = getTransportationAmount.reduce((a, b) => a + b, 0)

    let getEntertainment = getMonth.filter((item) => item.item === '娛樂')
    let getEntertainmentAmount = getEntertainment.map(f => f.amount)
    let entertainmentTotal = getEntertainmentAmount.reduce((a, b) => a + b, 0)

    let getGift = getMonth.filter((item) => item.item === '禮物')
    let getGiftAmount = getGift.map(f => f.amount)
    let giftTotal = getGiftAmount.reduce((a, b) => a + b, 0)

    let getOthers = getMonth.filter((item) => item.item === '其他')
    let getOthersAmount = getOthers.map(f => f.amount)
    let othersTotal = getOthersAmount.reduce((a, b) => a + b, 0)

    let totalArr = [];
    totalArr.push(foodTotal, ClothesTotal, transportationAmount, entertainmentTotal, giftTotal, othersTotal)
    let totalAmounts = totalArr.reduce((a, b) => a + b, 0)

    // let dscOrderTotalArr = dpsExpenseByCategory.sort(function(a, b) {
    //     return b.y - a.y;
    //   });

    var dpsExpenseByCategory:any[] = []; 

    function parseDataPoints() {

        if (foodTotal !== 0) {
            dpsExpenseByCategory.push({label: '食物', y: Math.floor(foodTotal/totalAmounts*100)});
        } 

        if (ClothesTotal !== 0) {
            dpsExpenseByCategory.push({label: '衣服', y: Math.floor(ClothesTotal/totalAmounts*100)});
        }

        if (transportationAmount !== 0) {
            dpsExpenseByCategory.push({label: '交通', y: Math.floor(transportationAmount/totalAmounts*100)});
        }

        if (entertainmentTotal !== 0) {
            dpsExpenseByCategory.push({label: '娛樂', y: Math.floor(entertainmentTotal/totalAmounts*100)});
        }

        if (giftTotal !== 0) {
            dpsExpenseByCategory.push({label: '禮物', y: Math.floor(giftTotal/totalAmounts*100)});
        }

        if (othersTotal !== 0) {
            dpsExpenseByCategory.push({label: '其他', y: Math.floor(othersTotal/totalAmounts*100)});
        }



        // dpsExpenseByCategory.push({label: '食物', y: Math.floor(foodTotal/totalAmounts*100)});
        // dpsExpenseByCategory.push({label: '衣服', y: Math.floor(ClothesTotal/totalAmounts*100)});
        // dpsExpenseByCategory.push({label: '交通', y: Math.floor(transportationAmount/totalAmounts*100)});
        // dpsExpenseByCategory.push({label: '娛樂', y: Math.floor(entertainmentTotal/totalAmounts*100)});
        // dpsExpenseByCategory.push({label: '禮物', y: Math.floor(giftTotal/totalAmounts*100)});
        // dpsExpenseByCategory.push({label: '其他', y: Math.floor(othersTotal/totalAmounts*100)});

        // for (var i = 0; i <= (new Date().getMonth()); i++) {
        // if (monthIncome.includes(i)) {
        //     dpsIncome.push({label: (months[i]), y: totalIncomeData[countIncome]});
        //     countIncome += 1; 
        // } else {
        //     dpsIncome.push({label: (months[i]), y: 0});
        // }
        // }
    };

    parseDataPoints();

    CanvasJS.addColorSet("greenShades",
                [
                "#2F4F4F",
                "#008080",
                "#2E8B57",
                "#3CB371",
                "#90EE90",
                "#00ffc3"
                ]);

    // Pie Chart
    const optionsPie = {
        // exportEnabled: true,
        animationEnabled: true,
        height: 400,
        colorSet: "greenShades",
        animationDuration: 2000,
        title: {
            text: "本月開支分佈",
            fontFamily: "verdana",
            fontWeight: "bold",
            fontSize: 24,

        },
        legend: {
            // horizontalAlign: "right",
            // verticalAlign: "center",
            fontSize: 13,
            maxWidth: 200,
            itemWrap: true,
            dockInsidePlotArea: true,
          },
        data: [{
            type: "pie",
            startAngle: 75,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 15,
            indexLabelFontWeight: "bold",
            // indexLabelPlacement: "outside",
            indexLabel: "{label} - {y}%",
            dataPoints: dpsExpenseByCategory
            // [
            //     { y: 18, label: "Direct" },
            //     { y: 49, label: "Organic Search" },
            //     { y: 9, label: "Paid Search" },
            //     { y: 5, label: "Referral" },
            //     { y: 19, label: "Social" }
            // ]
        }]
    }

    return (
        <>
            <div>
                    
                    { getMonthCat.length !== 0 
                    ? <CanvasJSChart options = {optionsPie} /> 
                    : <div className={style.chartMessage}>請新増項目以作分析</div> }

            </div>
        </>
    )
}