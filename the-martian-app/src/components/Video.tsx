import React from 'react'
import style from './Video.module.css'

export default function Video(props:{
    videoId: string|null ,
    thumbnail: string,
    key: string,
    videoTitle: string,
    videOnClick: () => void
}) {
    return (
        <>
            <div className={style.video} onClick={props.videOnClick}>
                <img className={style.thumbnail} src={props.thumbnail} alt="" width="320" height="180" />
                <div className={style.videoTitle}>{props.videoTitle}</div>
            </div>
        </>
    )
}
