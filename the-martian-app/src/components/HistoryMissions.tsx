
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import styles from './HIstoryMissions.module.css';

export default function HistoryMissions() {
    const allMissions = useSelector((state: RootState) => state.upcomingMissions.upComingMissions)
    const historyMissions = allMissions.filter(mission => mission.mission_status === "complete" || mission.expired === true)
    return (

        <div className={styles.mission_card_board}>
            {
                historyMissions.map(mission => <div className={styles.mission_card} key={`mission-${mission.id}`}>
                    <img className={styles.mission_card_img} src={`./img/${mission.mission_image}`} alt="missionImg"></img>
                    <div className={styles.mission_details}>
                        <h5 className={styles.mission_title}>{mission.mission_name}</h5>
                        <div className={styles.mission_star}>獎勵：⭐️ X {mission.reward_stars}</div>
                        <div className={styles.mission_deadline}><p> 限期：{mission.mission_exp_date}</p></div>
                    </div>
                </div>)
            }
        </div>

    )
}