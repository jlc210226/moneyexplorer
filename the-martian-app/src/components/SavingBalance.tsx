// import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { Table } from 'reactstrap';
import style from './SavingChart.module.css';
import { push } from 'connected-react-router';

export function SavingBalance(props: {

}) {

    const dispatch = useDispatch();

    const totalIncomeObj = useSelector((state: RootState) => state.totalIncome.totalIncome)
    const totalExpenseObj = useSelector((state: RootState) => state.totalExpense.totalExpense)

    let thisMonthIncome = totalIncomeObj?.filter(
        (item) => new Date(Date.parse(item.date_trunc)).getMonth()
            === new Date().getMonth())

    let thisMonthExpense = totalExpenseObj?.filter(
        (item) => new Date(Date.parse(item.date_trunc)).getMonth()
            === new Date().getMonth())

    if (thisMonthIncome.length === 0) {
        thisMonthIncome.push({ 'totalIncome': 0 })
    }

    if (thisMonthExpense.length === 0) {
        thisMonthExpense.push({ 'totalExpense': 0 })
    }

    let thisMonthBalance = thisMonthIncome[0].totalIncome - thisMonthExpense[0].totalExpense

    return (
        <div>

            <>
                <div>

                    <Table borderless hover className={style.balanceTable}>

                        <tbody>

                            <tr>
                                <th scope="row">本月收入</th>
                                <td className={style.balanceAmount}>
                                    {thisMonthIncome[0].totalIncome}
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">本月開支</th>
                                <td className={style.balanceAmount}>
                                    {thisMonthExpense[0].totalExpense}
                                </td>
                            </tr>

                            <tr className={style.balanceth}>
                                <th scope="row" className={style.balanceth}>本月結餘</th>
                                <td className={thisMonthBalance >= 0
                                    ? style.balanceAmountPositive
                                    : style.balanceAmountNegative}>
                                    {thisMonthBalance}
                                </td>
                            </tr>

                        </tbody>

                    </Table>

                </div>
            </>

            <>

                <div className={style.balanceContent}>

                    {thisMonthBalance > 0

                        ? <>
                        <div>👏🏼 &nbsp; 恭喜您！<br />記住繼續培養妥善分配開支
                        <br />以及量入為出的良好理財習慣啊！
                        </div>
                        <p></p>
                        <div>
                        <button className={style.createSavingGoal} 
                                onClick={() => {
                                    dispatch(push('/savingPlan'))
                                }}
                                >實行儲蓄大計</button>
                        </div>
                        </>

                        : thisMonthBalance === 0 ?
                            <div>🧐 &nbsp; 記住新增收支項目，
                        <br />先可以計算到收支分衡㗎！
                        </div>

                            : <div>💪🏼 &nbsp; 記得找出收支不平衡的原因，
                        <br />調整每月收支比例啊！
                        </div>}

                </div>

            </>
        </div>
    )
}

function dispatch(arg0: any) {
    throw new Error('Function not implemented.');
}

    // const savingPlans = useSelector((state: RootState) => state.savingPlan.savingPlans)

    // let savingPlansAmount = savingPlans.map((item) => item.goal_amount)

    // let savingPlansAmountTotal = savingPlansAmount.reduce((a, b) => a + b, 0)
    // console.log(savingPlansAmountTotal)