import styles from "./KnowledgeNav.module.css";
import { NavLink } from 'react-router-dom'
import { useState } from "react";
import { KnowledgeInfo } from './KnowledgeInfo';


export function KnowledgeNav() {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  return (
    <div className={styles.knowledgeNav}>
       <h1>知識寶藏<div className={styles.infoBox} onClick={toggle}>ⓘ</div></h1>
       {
         modal &&  <KnowledgeInfo modal={modal} onChange={() => setModal(false)}/> 
       }
       <ul>
         <li><NavLink activeClassName={styles.knowledgeActive} to="/knowledge/" exact>理財知識</NavLink></li>
         <li><NavLink activeClassName={styles.knowledgeActive} to="/knowledge/investment">投資知識</NavLink></li>
         <li><NavLink activeClassName={styles.knowledgeActive} to="/knowledge/quiz">理財小測驗</NavLink></li>
         <li><NavLink activeClassName={styles.knowledgeActive} to="/knowledge/movie">短片</NavLink></li>
       </ul>
    </div>
  )
}