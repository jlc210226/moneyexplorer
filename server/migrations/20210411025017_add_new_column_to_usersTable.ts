import { Knex } from "knex";
import { tables } from "../utils/tables"

export async function up(knex: Knex): Promise<void> {

    await knex.schema.table(tables.usersTable, (table) => {
        table.string("account_status").notNullable();
    })
}

export async function down(knex: Knex): Promise<void> {

    await knex.schema.table(tables.usersTable, (table) => {
        table.dropColumn("account_status")
    })
}
