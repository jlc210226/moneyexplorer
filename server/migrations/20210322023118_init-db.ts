import { Knex } from "knex";
import { tables } from '../utils/tables';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tables.usersTable, (table) => {
        table.increments();
        table.string("login_name").notNullable().unique();
        table.string("password").notNullable();
        table.integer("unlock_pin");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.avatarTable, (table) => {
        table.integer("id").notNullable().unique();
        table.string("avatar_filename").notNullable().unique();
    });

    await knex.schema.createTable(tables.userProfilesTable, (table) => {
        table.increments();
        table.string("nickname").notNullable();
        table.string("gender").notNullable();
        table.date("date_of_birth").notNullable();
        table.string("email").notNullable().unique();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.integer("avatar_id").unsigned();
        table.foreign("avatar_id").references(`${tables.avatarTable}.id`);
        table.integer("current_star").unsigned();
        table.integer("total_star").unsigned();
        table.integer("total_saving")
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.expenseCategoryTable, (table) => {
        table.increments();
        table.string("expense_category").notNullable().unique();
    });

    await knex.schema.createTable(tables.userExpensesTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.integer("expense_category_id").unsigned();
        table.foreign("expense_category_id").references("expense_category.id");
        table.integer("expense_amount").unsigned();
        table.date("expense_date");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.incomeCategoryTable, (table) => {
        table.increments();
        table.string("income_category").notNullable().unique();
    });

    await knex.schema.createTable(tables.userIncomesTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.integer("income_category_id").unsigned();
        table.foreign("income_category_id").references("income_category.id");
        table.integer("income_amount").unsigned();
        table.date("income_date");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.userSavingTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.integer("saving_amount").unsigned();
        table.date("saving_date");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.userSavingGoalTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.string("goal_name").notNullable();
        table.integer("goal_amount").notNullable();
        table.boolean("is_complete").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.missionTemplateTable, (table) => {
        table.increments();
        table.string("mission_name").notNullable();
        table.string("mission_image").notNullable();
        table.string("mission_type").notNullable();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.missionStatusTable, (table) => {
        table.integer("id").notNullable().unique();
        table.string("mission_status").notNullable().unique();
    });

    await knex.schema.createTable(tables.assignedMissionTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.integer("mission_id").unsigned();
        table.foreign("mission_id").references("mission_template.id");
        table.date("mission_exp_date").notNullable();
        table.integer("mission_status_id").unsigned();
        table.foreign("mission_status_id").references("mission_status.id");
        table.integer("reward_stars");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.missionTargetTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.string("target_name").notNullable();
        table.integer("needed_star").notNullable();
        table.boolean("is_complete").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.knowledgeCategoryTable, (table) => {
        table.increments();
        table.string("knowledge_category").notNullable().unique();
    });

    await knex.schema.createTable(tables.knowledgeTable, (table) => {
        table.increments();
        table.integer("knowledge_category_id").unsigned();
        table.foreign("knowledge_category_id").references("knowledge_category.id");
        table.string("title");
        table.text("content");
        table.string("source");
        table.string("knowledge_image");
        table.string("url");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.quizCategoryTable, (table) => {
        table.increments();
        table.string("quiz_category").notNullable().unique();
    });

    await knex.schema.createTable(tables.quizTable, (table) => {
        table.increments();
        table.string("quiz_name").notNullable();
        table.integer("quiz_category_id").unsigned();
        table.foreign("quiz_category_id").references("quiz_category.id");
        table.boolean("is_active").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.questionTable, (table) => {
        table.increments();
        table.text("question").notNullable();
        table.string("image");
        table.integer("quiz_id").unsigned();
        table.foreign("quiz_id").references("quiz.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.questionChoicesTable, (table) => {
        table.increments();
        table.integer("question_id").unsigned();
        table.foreign("question_id").references("question.id");
        table.string("choice");
        table.boolean("is_correct").notNullable();
    });

    await knex.schema.createTable(tables.quizResponseTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.integer("user_answer_id").unsigned();
        table.foreign("user_answer_id").references("question_choices.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(tables.quizResultTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("users.id");
        table.integer("quiz_id").unsigned();
        table.foreign("quiz_id").references("quiz.id");
        table.integer("score");
        table.timestamps(false, true);
    });

}


export async function down(knex: Knex): Promise<void> {

    await knex.schema.dropTable(tables.quizResultTable);
    await knex.schema.dropTable(tables.quizResponseTable);
    await knex.schema.dropTable(tables.questionChoicesTable);
    await knex.schema.dropTable(tables.questionTable);
    await knex.schema.dropTable(tables.quizTable);
    await knex.schema.dropTable(tables.quizCategoryTable);
    await knex.schema.dropTable(tables.knowledgeTable);
    await knex.schema.dropTable(tables.knowledgeCategoryTable);
    await knex.schema.dropTable(tables.missionTargetTable);
    await knex.schema.dropTable(tables.assignedMissionTable);
    await knex.schema.dropTable(tables.missionStatusTable);
    await knex.schema.dropTable(tables.missionTemplateTable);
    await knex.schema.dropTable(tables.userSavingGoalTable);
    await knex.schema.dropTable(tables.userSavingTable);
    await knex.schema.dropTable(tables.userIncomesTable);
    await knex.schema.dropTable(tables.incomeCategoryTable)
    await knex.schema.dropTable(tables.userExpensesTable);
    await knex.schema.dropTable(tables.expenseCategoryTable);
    await knex.schema.dropTable(tables.userProfilesTable);
    await knex.schema.dropTable(tables.avatarTable);
    await knex.schema.dropTable(tables.usersTable);

}

