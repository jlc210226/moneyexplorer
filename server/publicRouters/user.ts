import express from "express";
import { userController } from './../main';

export const userRoutes = express.Router();

userRoutes.post("/login", userController.login);
userRoutes.get('/login/google',userController.loginGoogle);
userRoutes.post('/register', userController.register);
userRoutes.post('/sendConfirmationEmail', userController.sendConfirmationEmail);
userRoutes.post('/verifyEmail', userController.verifyEmail);
userRoutes.post('/activateAccount', userController.activateAccount);
userRoutes.post('/forgetPasswordEmail', userController.forgetPasswordEmail);
userRoutes.post('/sendForgetPasswordEmail', userController.sendForgetPasswordEmail);
userRoutes.post('/resetPassword', userController.resetPassword)


