import { MissionService } from "../services/MissionService";
import { Request, Response } from 'express';


export class MissionController {
    constructor(private missionService: MissionService) { }

    loadCurrentStar = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            const star = await this.missionService.loadCurrentStar(userID);
            res.json({ star: star.current_star })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    checkpin = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            const pin = parseInt(req.body.pin)
            const result = await this.missionService.getPinNumberbyUserID(userID);
            if (result[0].unlock_pin !== pin) {
                res.json({ message: "failed" })
                return
            }
            res.json({ message: "success" })
            return
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    changeMissionStatus = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            const mission_status_name = req.body.changeStatus
            const mission_id = req.body.mission_id
            const reward_stars = req.body.reward_stars
            const mission_status_id = await this.missionService.getMissionStatusIDByMissionStatusName(mission_status_name);
            await this.missionService.changeMissionStatusByMissionID(mission_id, mission_status_id[0].id)
            if (mission_status_name === "complete") {
                await this.missionService.updateCurrentStar(reward_stars, userID)
                await this.missionService.updateTotalStar(reward_stars, userID)
            }

            res.json({ message: "success" })
            return
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getUpcomingMissions = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            const result = await this.missionService.getUpcomingMissions(userID)
            res.json(result)
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getAllMissionTemplates = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            const result = await this.missionService.getAllMissionTemplates(userID)
            res.json(result)
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    addNewUpcomingMission = async (req: Request, res: Response) => {
        try {

            const { mission_template_id, mission_exp_date, reward_star, mission_status } = req.body
            console.log(mission_exp_date)
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            const mission_id = await this.missionService.getMissionStatusIDByMissionStatusName(mission_status)
            await this.missionService.addNewUpcomingMission(userID, mission_template_id, mission_exp_date, reward_star, mission_id[0].id)
            res.json({ message: "success" })
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    addCustomizedMission = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            const { mission_name, mission_exp_date, reward_star, mission_status } = req.body
            const mission_status_id = await this.missionService.getMissionStatusIDByMissionStatusName(mission_status)
            const mission_id = await this.missionService.addCustomizedMissionTemplate(mission_name, userID)
            await this.missionService.addNewUpcomingMission(userID, mission_id[0], mission_exp_date, reward_star, mission_status_id[0].id)
            res.json({ message: "success" })
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getAllMissionTargets = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            const result = await this.missionService.getAllMissionTargets(userID)
            res.json(result)
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    addNewMissionTarget = async (req: Request, res: Response) => {
        try {
            const { target_name, needed_star } = req.body
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            await this.missionService.addNewMissionTarget(userID,target_name, needed_star)
            res.json({ message: "success" })
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    redeemMissionTarget = async (req: Request, res: Response) => {
        try {
            const {target_id, needed_star} = req.body
            const tempID = req.user?.id as any
            const userID = parseInt(tempID)
            await this.missionService.subtractStarNum(userID, needed_star)
            await this.missionService.redeemMissionTarget(target_id)
            res.json({message: "success"})
        }catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    } 


}