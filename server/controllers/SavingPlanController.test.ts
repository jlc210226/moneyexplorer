import { SavingPlanController } from './SavingPlanController';
import { SavingPlanService } from '../services/SavingPlanService';
import { Request, Response } from 'express';
import { Knex } from 'knex';
jest.mock('express');

describe('SavingPlanController', () => {
    let controller: SavingPlanController;
    let service: SavingPlanService;
    let resJson: jest.SpyInstance;
    let req: Request;
    let res: Response;
    let dateSpy: Date;

    beforeEach(function () {
        service = new SavingPlanService({} as Knex);

        jest.spyOn(service, 'getSavingPlans').mockImplementation(() => Promise.resolve([{
            id: 1,
            user_id: 1,
            goal_name: "Go Ocean Park",
            goal_amount: 500,
            is_complete: false,
            created_at: dateSpy,
            updated_at: dateSpy
        }]));
        jest.spyOn(service, 'getCompletedSavingPlans').mockImplementation(() => Promise.resolve([{
            id: 2,
            user_id: 1,
            goal_name: "Buy a bike",
            goal_amount: 200,
            is_complete: true,
            created_at: dateSpy,
            updated_at: dateSpy
        }]));

        // jest.spyOn(service, 'getTotalSaving').mockImplementation((userId) => Promise.resolve(500))

        jest.spyOn(service, 'markGoalComplete').mockImplementation((userId, goalId) => Promise.resolve({ message: "Success" }));
        jest.spyOn(service, 'deleteGoal').mockImplementation((userId, goalId) => Promise.resolve({ message: "Success" }));
        jest.spyOn(service, 'updateTotalSaving').mockImplementation((userId, goalAmount) => Promise.resolve(1000));
        jest.spyOn(service, 'createSavingGoal').mockImplementation((goalName, goalAmount) => Promise.resolve({ message: "Success" }));

        controller = new SavingPlanController(service);

        req = {
            body: {},
            params: {},
            user: {
                id: 1
            }
        } as any as Request;

        res = {
            json: () => { }
        } as any as Response;

        resJson = jest.spyOn(res, 'json')

    })


    it('it should get all saving plans correctly', async () => {
        await controller.getSavingPlans(req, res);
        expect(service.getSavingPlans).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith([{
            id: 1,
            user_id: 1,
            goal_name: "Go Ocean Park",
            goal_amount: 500,
            is_complete: false,
            created_at: dateSpy,
            updated_at: dateSpy
        }])
    })

    it('it should get all completed saving plans correctly', async () => {
        await controller.getCompletedSavingPlans(req, res);
        expect(service.getCompletedSavingPlans).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith([{
            id: 2,
            user_id: 1,
            goal_name: "Buy a bike",
            goal_amount: 200,
            is_complete: true,
            created_at: dateSpy,
            updated_at: dateSpy
        }])
    })

    it('it should mark goal complete correctly', async () => {
        req.body.userId = 1;
        req.body.goalId = 2;
        await controller.markGoalComplete(req, res);
        expect(service.markGoalComplete).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: "Success" })
    })

    it('it should delete goal complete correctly', async () => {
        req.body.userId = 1;
        req.body.goalId = 2;
        await controller.deleteGoal(req, res);
        expect(service.deleteGoal).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: "Success" })
    })

    it('it should update total saving correctly', async () => {
        req.body.userId = 1;
        req.body.goalAmount = 500;
        await controller.updateTotalSaving(req, res);
        expect(service.updateTotalSaving).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith(1000)
    })

    it('it should return "not enough saving if new saving < 0', async () => {
        jest.spyOn(service, "updateTotalSaving").mockImplementation(() => Promise.resolve({ message: "Not Enough Saving"}))
        req.body.userId = 1;
        req.body.goalAmount = 500;
        await controller.updateTotalSaving(req, res);
        expect(service.updateTotalSaving).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({message: "Not Enough Saving"})
    })

    it('it should create new saving goal correctly', async () => {
        req.body.goalName = "Go Disney";
        req.body.goalAmount = 800;
        await controller.createSavingGoal(req, res);
        expect(service.createSavingGoal).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith("Success")
    })

})