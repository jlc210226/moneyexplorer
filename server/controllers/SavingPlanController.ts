import { Request, Response } from "express";
import { SavingPlanService } from "../services/SavingPlanService";

export class SavingPlanController {
    constructor(private savingPlanService: SavingPlanService) { }

    getSavingPlans = async (req: Request, res: Response) => {
        try {
            const ID = req.user?.id as any
            const userId = parseInt(ID)

            if (!userId || isNaN(userId)) {
                res.status(400).json({ message: 'Invalid User Id' });
                return;
            }

            const savingPlans = await this.savingPlanService.getSavingPlans(userId);

            res.json(savingPlans);

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "internal server error" });
        }
    };

    getCompletedSavingPlans = async (req: Request, res: Response) => {
        try {
            const ID = req.user?.id as any
            const userId = parseInt(ID)

            if (!userId || isNaN(userId)) {
                res.status(400).json({ message: 'Invalid User Id' });
                return;
            }

            const completedPlans = await this.savingPlanService.getCompletedSavingPlans(userId);

            res.json(completedPlans);

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "internal server error" });
        }
    };

    getTotalSaving = async (req: Request, res: Response) => {
        try {

            const ID = req.user?.id as any
            const userId = parseInt(ID)

            if (!userId || isNaN(userId)) {
                res.status(400).json({ message: 'Invalid User Id' });
                return;
            }

            const totalSaving = await this.savingPlanService.getTotalSaving(userId);

            res.json(totalSaving);

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "internal server error" });
        }

    };

    markGoalComplete = async (req: Request, res: Response) => {
        try {
            const {userId, goalId} = req.body;

            if (!userId || isNaN(userId)) {
                res.status(400).json({ message: 'Invalid User Id' });
                return;
            }
            
            const result = await this.savingPlanService.markGoalComplete(userId, goalId);

            if (result.count == '0') { // why === not work
                res.status(400).json({message: 'invalid user id or goal id'});
                return;
            }

            res.json(result);

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "internal server error" });
        }
    }

    deleteGoal = async (req: Request, res: Response) => {
        try {

            const {userId, goalId} = req.body;

            if (!userId || isNaN(userId)) {
                res.status(400).json({ message: 'Invalid User Id' });
                return;
            }

            const result = await this.savingPlanService.deleteGoal(userId, goalId);

            if (result.count == '0') {
                res.status(400).json({message: 'invalid user id or goal id'});
                return;
            }

            res.json(result);

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "internal server error" });
        }
    }

    updateTotalSaving = async (req: Request, res: Response) => {
        try {
            const {userId, goalAmount} = req.body;

            const totalSaving = await this.savingPlanService.getTotalSaving(userId);

            if (totalSaving < goalAmount) {
                res.json(400).json({message: "not enough saving"});
                return;
            }

            const newSaving = totalSaving.total_saving - goalAmount;

            const result = await this.savingPlanService.updateTotalSaving(userId, newSaving);

            if (result.count != '1') {
                res.status(400).json({message: 'invalid user id or goal id'});
                return;
            }

            res.json(result);

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "internal server error" });
        }
    }

    createSavingGoal = async (req: Request, res: Response) => {
        try {
            const ID = req.user?.id as any
            const userId = parseInt(ID)    
            const { goalName, goalAmount } = req.body;
        
            const result = await this.savingPlanService.createSavingGoal(userId, goalName, goalAmount);

            if (result.message === "success" && result.newGoalId !== null) {
                res.json(result.message)
            }

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: "internal server error" });
        }
    }
}