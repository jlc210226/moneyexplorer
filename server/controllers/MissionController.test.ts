import { MissionController } from './MissionController';
import { MissionService } from '../services/MissionService';
import { Request, Response } from 'express';
import { Knex } from 'knex';
jest.mock('express');

describe("MissionController", () => {
    let controller: MissionController;
    let service: MissionService;
    let resJson: jest.SpyInstance;
    // let resStatus: jest.SpyInstance;
    let req: Request;
    let res: Response;
    let resStatusJson: jest.Mock;

    beforeEach(function () {
        service = new MissionService({} as Knex);
        jest.spyOn(service, "loadCurrentStar").mockImplementation(() => Promise.resolve({ current_star: 160 }))
        jest.spyOn(service, "getPinNumberbyUserID").mockImplementation(() => Promise.resolve([{ unlock_pin: 1234 }]))
        jest.spyOn(service, "getMissionStatusIDByMissionStatusName").mockImplementation(() => Promise.resolve([{ id: 1 }]))
        jest.spyOn(service, "changeMissionStatusByMissionID").mockImplementation()
        jest.spyOn(service, "updateCurrentStar").mockImplementation()
        jest.spyOn(service, "updateTotalStar").mockImplementation()
        jest.spyOn(service, "addNewUpcomingMission").mockImplementation()
        jest.spyOn(service, "getUpcomingMissions").mockImplementation(() => Promise.resolve([{
            id: 28,
            mission_status: 'complete',
            mission_name: '整理房間',
            mission_image: '2.jpg',
            mission_type: 'default',
            user_id: 1,
            created_at: "2021-04-08T09:03:21.194Z",
            updated_at: "2021-04-08T09:03:21.194Z",
            assigned_mission_id: 28,
            mission_id: 2,
            mission_exp_date: "2021-04-07T16:00:00.000Z",
            mission_status_id: 2,
            reward_stars: 40
        }]))

        jest.spyOn(service, "getAllMissionTemplates").mockImplementation(() => Promise.resolve([{
            id: 1,
            mission_name: '洗碗',
            mission_image: '1.jpg',
            mission_type: 'default',
            user_id: null,
            created_at: "2021-04-07T05:02:13.073Z",
            updated_at: "2021-04-07T05:02:13.073Z"
        }]))
        jest.spyOn(service, "getMissionStatusIDByMissionStatusName").mockImplementation(() => Promise.resolve([{ id: 1 }]))
        jest.spyOn(service, "addCustomizedMissionTemplate").mockImplementation(() => Promise.resolve([11]))
        jest.spyOn(service, "getAllMissionTargets").mockImplementation(() => Promise.resolve([{
            id: 18,
            user_id: 1,
            target_name: 'disneyland',
            needed_star: 100,
            is_complete: true,
            created_at: "2021-04-09T04:20:03.958Z",
            updated_at: "2021-04-09T04:20:03.958Z"
        }]))
        jest.spyOn(service, "addNewMissionTarget").mockImplementation()
        jest.spyOn(service, "subtractStarNum").mockImplementation()
        jest.spyOn(service, "redeemMissionTarget").mockImplementation()
        controller = new MissionController(service);
        req = {
            body: {},
            params: {},
            user: {
                id: 1
            }

        } as any as Request;
        resStatusJson = jest.fn();
        res = {
            json: () => { },
            status: (statusCode: number) => ({
                json: resStatusJson,
            }),
        } as any as Response;
        resJson = jest.spyOn(res, 'json');
        // resStatus = jest.spyOn(res, 'status');
    })

    it('should get current star number', async () => {
        await controller.loadCurrentStar(req, res);
        expect(service.loadCurrentStar).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ star: 160 })
    })

    it("return success message if pin number is correct", async () => {
        req.body.pin = 1234
        await controller.checkpin(req, res);
        expect(service.getPinNumberbyUserID).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: "success" })
    })
    it("return failed message if pin number is incorrect", async () => {
        req.body.pin = 4567
        await controller.checkpin(req, res);
        expect(service.getPinNumberbyUserID).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: "failed" })
    })
    it("return success if correct", async () => {
        req.body.changeStatus = "complete"
        req.body.mission_id = 3
        req.body.reward_stars = 10
        await controller.changeMissionStatus(req, res);
        expect(service.getMissionStatusIDByMissionStatusName).toBeCalledTimes(1)
        expect(service.changeMissionStatusByMissionID).toBeCalledTimes(1)
        expect(service.updateCurrentStar).toBeCalledTimes(1)
        expect(service.updateTotalStar).toBeCalledTimes(1)
        expect(resJson).toBeCalledWith({ message: "success" })
    })
    it("should get upcoming missions correctly", async () => {
        await controller.getUpcomingMissions(req, res);
        expect(service.getUpcomingMissions).toBeCalledTimes(1)
        expect(resJson).toBeCalledWith([{
            id: 28,
            mission_status: 'complete',
            mission_name: '整理房間',
            mission_image: '2.jpg',
            mission_type: 'default',
            user_id: 1,
            created_at: "2021-04-08T09:03:21.194Z",
            updated_at: "2021-04-08T09:03:21.194Z",
            assigned_mission_id: 28,
            mission_id: 2,
            mission_exp_date: "2021-04-07T16:00:00.000Z",
            mission_status_id: 2,
            reward_stars: 40
        }])
    })

    it("should get mission templates correctly", async () => {
        await controller.getAllMissionTemplates(req, res);
        expect(service.getAllMissionTemplates).toBeCalledTimes(1)
        expect(resJson).toBeCalledWith([{
            id: 1,
            mission_name: '洗碗',
            mission_image: '1.jpg',
            mission_type: 'default',
            user_id: null,
            created_at: "2021-04-07T05:02:13.073Z",
            updated_at: "2021-04-07T05:02:13.073Z"
        }])
    })

    it("should add new upcoming mission correctly", async () => {
        req.body.mission_template_id = 1
        req.body.mission_exp_date = "2021-05-03"
        req.body.reward_star = 10
        req.body.mission_status = "in progress"
        await controller.addNewUpcomingMission(req, res)
        expect(service.getMissionStatusIDByMissionStatusName).toBeCalledTimes(1)
        expect(service.addNewUpcomingMission).toBeCalledTimes(1)
        expect(resJson).toBeCalledWith({ message: "success" })
    })

    it("should add customized mission correctly", async () => {
        req.body.mission_name = "洗碗"
        req.body.mission_exp_date = "2021-05-03"
        req.body.reward_star = 19
        req.body.mission_status = "in progress"
        await controller.addCustomizedMission(req, res)
        expect(service.addCustomizedMissionTemplate).toBeCalledTimes(1)
        expect(service.addNewUpcomingMission).toBeCalledTimes(1)
        expect(resJson).toBeCalledWith({ message: "success" })
    })

    it("should get all mission targets correctly", async () => {
        await controller.getAllMissionTargets(req, res)
        expect(service.getAllMissionTargets).toBeCalledTimes(1)
        expect(resJson).toBeCalledWith([{
            id: 18,
            user_id: 1,
            target_name: 'disneyland',
            needed_star: 100,
            is_complete: true,
            created_at: "2021-04-09T04:20:03.958Z",
            updated_at: "2021-04-09T04:20:03.958Z"
        }])
    })

    it("should add new mission target correctly", async () => {
        await controller.addNewMissionTarget(req, res)
        req.body.target_name = "disneyland"
        req.body.needed_star = 50
        expect(service.addNewMissionTarget).toBeCalledTimes(1)
        expect(resJson).toBeCalledWith({ message: "success" })
    })

    it("should redeem mission target correctly", async () => {
        req.body.target_id = 1
        req.body.needed_star = 50
        await controller.redeemMissionTarget(req, res)
        expect(resJson).toBeCalledWith({message: "success"})
    })
})