
import { Request, Response } from 'express';
import { Knex } from 'knex';
import { SavingService } from '../services/SavingService';
import { SavingController } from './SavingController';
jest.mock('express');

describe("SavingController", () => {
    let controller: SavingController;
    let service: SavingService;
    let resJson: jest.SpyInstance;
    let req: Request;
    let res: Response;

    beforeEach(function () {
        service = new SavingService({} as Knex);
        jest.spyOn(service, 'getTotalIncomeByMonth').mockImplementation(
            () => Promise.resolve
            ([{ date_trunc: "2021-01-31T16:00:00.000Z", totalIncome: "25" }])
        );

        jest.spyOn(service, 'getTotalExpenseByMonth').mockImplementation(
            () => Promise.resolve
            ([{ date_trunc: "2021-01-31T16:00:00.000Z", totalExpense: "88" }])
        );

        jest.spyOn(service, 'getTotalSavingByMonth').mockImplementation(
            () => Promise.resolve
            ([{ date_trunc: "2021-01-31T16:00:00.000Z", totalSaving: "10" }])
        );

        jest.spyOn(service, 'createIncome').mockImplementation()

        controller = new SavingController(service);
        req = {
            body: {},
            params: {},
            user: {
                id: 1,
            }
        } as any as Request;
        res = {
            json: () => { }
        } as any as Response;
        resJson = jest.spyOn(res, 'json');

    })

    it('should get total income by month correctly', async () => {
        await controller.loadTotalIncomeByMonth(req, res);
        expect(service.getTotalIncomeByMonth).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({"getTotalIncome": [{"date_trunc": "2021-01-31T16:00:00.000Z", "totalIncome": "25"}]})
    })

    it('should get total expense by month correctly', async () => {
        await controller.loadTotalExpenseByMonth(req, res);
        expect(service.getTotalExpenseByMonth).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith([{"date_trunc": "2021-01-31T16:00:00.000Z", "totalExpense": "88"}])
    })

    it('should get total saving by month correctly', async () => {
        await controller.loadTotalSavingByMonth(req, res);
        expect(service.getTotalSavingByMonth).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith([{"date_trunc": "2021-01-31T16:00:00.000Z", "totalSaving": "10"}])
    })

    // it('should add new income item successfully', async () => {
    //     // const newSubmit = req.body;
    //     req.body.income_date = new Date ();
    //     req.body.income_category = "零用錢";
    //     req.body.income_amount = 500;
    //     await controller.createIncome(req, res);
    //     expect(service.createIncome).toBeCalledWith(1);
    //     expect(resJson).toBeCalledWith([
    //         {
    //           id: 10,
    //           user_id: 2,
    //           income_category_id: 2,
    //           income_amount: 10,
    //           income_date: new Date(),
    //           created_at: new Date(),
    //           updated_at: new Date()
    //         }
    //       ])
    // })



})