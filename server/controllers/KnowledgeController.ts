import { Request, Response } from "express";
import { KnowledgeService } from "../services/KnowledgeService";

export class KnowledgeController {
    constructor(private knowledgeService: KnowledgeService) { }

    getQuizzes = async (req: Request, res: Response) => {
        try {
            const userId = req.user?.id

            if (!userId || isNaN(userId)) {
                res.status(400).json({ message: 'Invalid User Id' });
                return;
            }

            //count completed Quizzes today
            const [countCompletedQuizToday] = await this.knowledgeService.countCompletedQuizToday(userId)

            //get completed quizzes
            const completedQuizzes = await this.knowledgeService.getCompletedQuizzes(userId);

            const completedQuizzesCount = countCompletedQuizToday.count

            //return completedQuizzes only if there are quiz completed today
            if (completedQuizzesCount > 0) {
                res.json({ message: "success", quizzes: { completedQuizzes, completedQuizzesCount } })
                return
            }

            //completed quizzes Ids
            const completedIds : number[] = [];

            for (const completedQuizze of completedQuizzes ){
                const quizId = parseInt(completedQuizze.quiz_id)

                completedIds.push(quizId)
            }
        
            //get quizzes that are not completed by that user
            const quizzes = await this.knowledgeService.getQuizzes(completedIds);

            res.json({ message: "success", quizzes: { quizzes, completedQuizzes, completedQuizzesCount } })

            return

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    getQuestions = async (req: Request, res: Response) => {
        try {
            const quizId = parseInt(req.params.quizId)

            const choices = (await this.knowledgeService.getQuestionChoicesByquizId(quizId)) as any

            let questionsMap = new Map();

            for (let item of choices) {
                const choice = {
                    choice_id: item.id,
                    choice: item.choice
                };

                if (questionsMap.has(item.question_id)) {
                    questionsMap.get(item.question_id).choices.push(choice) 
                } else {
                    const question = {
                        question_id: item.question_id,
                        question: item.question,
                        choices: [choice],
                        image: item.image
                    };
                    questionsMap.set(item.question_id, question)
                }
            }

            const questions = Array.from(questionsMap.values())

            res.json({ message: "success", questions })

            return

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    getRevisions = async (req: Request, res: Response) => {
        try {
            const quizId = parseInt(req.params.quizId)

            const userId = req.user?.id

            if (!userId || isNaN(userId)) {
                res.status(400).json({ message: 'Invalid User Id' });
                return;
            }

            const modelAnswers = await this.knowledgeService.getModelAnswers(quizId)

            const userResponse = await this.knowledgeService.getUserResponse(quizId, userId)

            res.json({ message: "success", modelAnswers, userResponse })

            return


        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    submitQuiz = async (req: Request, res: Response) => {
        try {
            const questions = req.body.questions
            const quizId = parseInt(req.params.quizId)

            const userId = req.user?.id

            if (!userId) {
                res.status(400).json({ message: 'Invalid User Id' });
                return;
            }

            const anwserIds = [];

            for (let question of questions) {
                const anwserId = question.selectedOption.choice_id
                anwserIds.push(anwserId)
            }

            //insert Reponses
            await this.knowledgeService.submitResponse(userId, anwserIds)

            //check Score

            const checkScore = await this.knowledgeService.checkScore(anwserIds)

            let score = 0;

            for (const choice of checkScore) {
                if (choice.is_correct) {
                    score += 1
                }
            }

            const submittedQuiz = await this.knowledgeService.submitQuizResult(userId, quizId, score)

            //reward stars

            const quizStarRewards = await this.knowledgeService.quizStarRewards(userId, score)

            res.json({ message: "success", submittedQuiz, quizStarRewards })


        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getInvestmentKnowledge = async (req:Request, res: Response) => {
        try{
            const investmentKnowledge = await this.knowledgeService.getInvestmentKnowledge()

            res.json({ message: "success", investmentKnowledge })

        }catch(err){
            console.error(err.message)
        }
    }

    getFinancialKnowledge = async (req:Request, res: Response) => {
        try{
            const financialKnowledge = await this.knowledgeService.getFinancialKnowledge()

            res.json({ message: "success", financialKnowledge })

        }catch(err){
            console.error(err.message)
        }
    }
}
