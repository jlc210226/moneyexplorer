import { Request, Response } from "express";
import { UserService } from "../services/UserService";
import { checkPassword } from "../utils/hash";
import jwtsimple from 'jwt-simple';
import { Bearer } from "permit";
import { twilioClient } from "../main";
import sgMail from '@sendgrid/mail';

// Initialize SendGrid Mail
sgMail.setApiKey(process.env.SENDGRID_API_KEY!)

const permit = new Bearer({
    query: "access_token"
})

const serviceID = 'VAb06d560f96aac20335f593d86ca82188';

export class UserController {
    constructor(private userService: UserService) { }

    login = async (req: Request, res: Response) => {
        try {

            if (!req.body.loginName || !req.body.password) {
                res.status(401).json({ message: 'invalid login name or password' });
                return;
            }

            const { loginName, password } = req.body;

            const user = await this.userService.getUser(loginName)

            if (!user) {
                res.status(401).json({ message: 'invalid login name or password' });
                return;
            }

            if (user.account_status === "pending") {
                res.status(401).json({ message: 'please confirm email before login', email: user.email });
                return;
            }

            const match = await checkPassword(password, user.password);

            if (!match) {
                res.status(401).json({ message: 'invalid login name or password' });
                return;
            }

            const payload = {
                id: user.id,
                loginName: user.login_name,
                exp: Date.now() / 1000 + 36000
            };

            const jwtSecret = process.env.jwtSecret!

            const token = jwtsimple.encode(payload, jwtSecret);

            res.json({
                id: user.id,
                token: token
            });

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    loginGoogle = async (req: Request, res: Response) => {
        const accessToken = req.session?.['grant'].response.access_token;
        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            method: "get",
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        });
        const result = await fetchRes.json();
        const user = await this.userService.getUser(result.email)

        if (!user) {
            return res.status(401).redirect('/login.html?error=Incorrect+Username')
        }
        if (req.session) {
            req.session["user"] = {
                id: user.id
            };
        }
        return res.redirect('/')
    }

    getCurrentUser = async (req: Request, res: Response) => {
        res.json(req['user']);
    }

    getUserInfo = async (req: Request, res: Response) => {

        try {
            const token = permit.check(req);

            if (!token) {
                res.status(401).json({ message: 'Unauthorized' });
                return;
            }

            const jwtSecret = process.env.jwtSecret;

            const payload = jwtsimple.decode(token, jwtSecret!);

            const userId = payload.id;

            const userInfo = await this.userService.getUserInfo(userId);

            res.json(userInfo)

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    register = async (req: Request, res: Response) => {
        try {
            const loginName = req.body.loginName;
            const password = req.body.password;
            const pin = req.body.pin;
            const email = req.body.email;

            // Check if there is any existed login name
            const checkLoginName = await this.userService.getUser(loginName);

            if (checkLoginName) {
                res.status(400).json({ message: "Login Name Existed" })
                return;
            }

            // Check if there is any existed email
            const checkEmail = await this.userService.getUserByEmail(email);

            if (checkEmail) {
                res.status(400).json({ message: "Email Existed" })
                return;
            }

            // Create new record in USERS table
            const result = await this.userService.registerNewUser(loginName, password, pin);

            const userId = result[0];

            if (!userId) {
                res.status(400).json({ message: 'Register Fail' });
                return;
            }

            // Create new record in USER PROFILE table
            const nickname = req.body.nickname;
            const date_of_birth = req.body.birthday;
            const gender = req.body.gender;
            const iconId = req.body.iconId;

            const userProfileId = await this.userService.createUserProfile(nickname, gender, date_of_birth, email, userId, iconId);

            if (!userProfileId) {

                // Delete record from USER table
                const deleteId = await this.userService.deleteUser(loginName);

                if (deleteId) {
                    res.status(400).json({ message: 'Register Fail' });
                    return;
                } else {
                    res.status(500).json({ message: 'internal server error' })
                }

            }

            res.json({message: "success", userProfileId: userProfileId})

        } catch (err) {
            console.error(err)
            res.status(500).json({ message: 'internal server error' })
        }
    }

    editProfile = async (req: Request, res: Response) => {
        try {

            const ID = req.user?.id as any
            const userId = parseInt(ID)

            const nickname = req.body.nickname;
            const gender = req.body.gender;
            const birthday = req.body.birthday;
            const iconId = req.body.iconId;

            const result = await this.userService.editProfile(userId, nickname, gender, birthday, iconId);

            if (!result) {
                res.status(400).json({ message: 'update fail' });
                return;
            }

            res.json({ message: "success" });

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    editPassword = async (req: Request, res: Response) => {
        try {

            const ID = req.user?.id as any
            const userId = parseInt(ID)

            const oldPassword = req.body.oldPassword;
            const password = req.body.password;

            const user = await this.userService.getUserById(userId);

            if (!user) {
                res.status(401).json({ message: 'invalid login name or password' });
                return;
            }

            const match = await checkPassword(oldPassword, user.password);

            if (!match) {
                res.status(401).json({ message: 'invalid login name or password' });
                return;
            }

            const result = await this.userService.updatePassword(userId, password)

            if (!result) {
                res.status(400).json({ message: "update fail" });
                return;
            }

            res.json({ message: "success" });

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    sendConfirmationEmail = async (req: Request, res: Response) => {
        try {

            const email = req.body.email;

            // const result = await twilioClient.verify.services(serviceID).verifications.create({to: email, channel: 'email'});

            const result = await twilioClient.verify.services(serviceID).verifications.create({
                channelConfiguration: {
                    "substitutions": {
                        "email": email
                    }
                },
                to: email, channel: 'email'
            });

            res.json(result)

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    verifyEmail = async (req: Request, res: Response) => {
        try {
            const { email, code } = req.body;

            const user = await this.userService.getUserByEmail(email);

            if (!user) {
                res.status(400).json({ message: "User not exist" });
                return;
            }

            if (user.account_status === "active") {
                res.json({ message: "Account activated" });
                return;
            }

            const result = await twilioClient.verify.services(serviceID).verificationChecks.create({ to: email, code: code });

            res.json(result)

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    activateAccount = async (req: Request, res: Response) => {
        try {

            const email = req.body.email;

            const result = await this.userService.activateAccount(email);

            if (!result) {
                res.status(401).json({ message: 'invalid email or code' });
                return;
            }

            res.json({ message: "account activated" })

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    forgetPasswordEmail = async (req: Request, res: Response) => {
        try {

            const email = req.body.email;

            const user = await this.userService.getUserByEmail(email);

            if (!user) {
                res.status(400).json({ message: "user not exist" });
                return;
            }

            const payload = {
                id: user.id,
                email: email,
                exp: Date.now() / 1000 + 3600
            }

            const secret = user.password + '-' + user.created_at.getTime();

            const token = jwtsimple.encode(payload, secret);

            const emailURL = `resetpassword/${payload.id}/${token}`;

            res.json({ message: "success", emailURL: emailURL, nickname: user.nickname })

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' })
        }
    }


    sendForgetPasswordEmail = async (req: Request, res: Response) => {
        try {

            const { email, emailURL, nickname } = req.body;

            const content = {
                to: email,
                from: 'moneyexplorerhk@gmail.com',
                subject: 'Money Explorer 重設密碼',
                // html: `<body>
                //         <p>Click to set a new password : <a href="http://localhost:3000/${emailURL}">Reset password</a></p>
                //        </body>`

                html: `
                <html>
                    <head>
                    <style type="text/css">
                     body, p, div {
                        font-family: Helvetica, Arial, sans-serif;
                        font-size: 14px;
                     }

                     a {
                      text-decoration: none;
                     }
                    </style>
                    <title></title>
                    </head>

                    <body>
                 
                        <p>
                            ${nickname} 你好，Money Explorer 已收到你提交的重設賬戶密碼申請，現在你可以透過以下連結設定新密碼。
                        </p>

                        <p> <a href="https://moneyexplorer.xyz/#/${emailURL}">重設密碼</a> </p>

                    </body>

                </html>
                `
            }

            const response = await sgMail.send(content);

            res.json({ message: "email sent successfully", response: response })

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'error sending email' })
        }
    }

    resetPassword = async (req: Request, res: Response) => {
        try {
            const { password, userId, token } = req.body;

            const user = await this.userService.getUserById(parseInt(userId));

            if (!user) {
                res.status(400).json({ message: "invalid user" });
                return;
            }

            const secret = user.password + '-' + user.created_at.getTime();

            const payload = jwtsimple.decode(token, secret);

            if (Date.now() / 1000 >= payload.exp) {
                res.status(400).json({ message: "token expired" });
            }

            const id = await this.userService.updatePassword(userId, password);

            res.json({id: id})

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'error resetting password' })
        }
    }

    editPin = async (req: Request, res: Response) => {
        try {

            const ID = req.user?.id as any
            const userId = parseInt(ID)

            const oldPassword = parseInt(req.body.oldPassword);
            const password = parseInt(req.body.password);

            const user = await this.userService.getUserById(userId);

            if (!user) {
                res.status(401).json({ message: 'invalid login name or password' });
                return;
            }

            if (user.unlock_pin != oldPassword) {
                res.status(401).json({ message: 'invalid pin' });
                return;
            }

            const result = await this.userService.editPin(userId, password);

            if (!result) {
                res.status(401).json({ message: 'update fail' });
                return;
            }

            res.json({ message: "success" });

        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' })
        }
    }
}
