import { KnowledgeController } from './KnowledgeController';
import { KnowledgeService } from '../services/KnowledgeService';
import { Request, Response } from 'express';
import { Knex } from 'knex';
jest.mock('express');

describe("KnowledgeController", () => {
    let controller: KnowledgeController;
    let service: KnowledgeService;
    let resJson: jest.SpyInstance;
    let req: Request;
    let res: Response;

    beforeEach(function () {
        service = new KnowledgeService({} as Knex);
        jest.spyOn(service, 'countCompletedQuizToday').mockImplementation(() => Promise.resolve([{ count: 0 }]));
        jest.spyOn(service, 'getCompletedQuizzes').mockImplementation(() => Promise.resolve([{ quiz_id: 1, quiz_name: "quiz 1", score: 4 }, { quiz_id: 2, quiz_name: "quiz 2", score: 5 }]));
        jest.spyOn(service, 'getQuizzes').mockImplementation(() => Promise.resolve([{ quiz_id: 3, quiz_name: "quiz 3" }, { quiz_id: 4, quiz_name: "quiz 4" }]));
        jest.spyOn(service, 'getQuestionChoicesByquizId').mockImplementation(() => Promise.resolve([{ id: 1, question: "question 1", image: "test.jpg", quiz_id: 1, question_id: 1, choice: "choice 1" }, { id: 2, question_id: 1, question: "question 1", image: "test.jpg", quiz_id: 1, choice: "choice 2" }, { id: 3, question_id: 2, question: "question 2", image: "test.jpg", quiz_id: 1, choice: "choice 3" }, { id: 4, question_id: 2, question: "question 4", image: "test.jpg", quiz_id: 1, choice: "choice 4" }]));
        jest.spyOn(service, 'getModelAnswers').mockImplementation(() => Promise.resolve([{ id: 1, question: "question 1", image: "test.jpg", quiz_id: 1, question_id: 1, choice: "choice 1", is_correct: true }, { id: 3, question: "question 2", image: "test.jpg", quiz_id: 1, question_id: 2, choice: "choice 3", is_correct: true }]));
        jest.spyOn(service, 'getUserResponse').mockImplementation(() => Promise.resolve([{ id: 1, user_id: 1, quiz_id: 1, user_answer_id: 1 }, { id: 2, quiz_id: 1, user_id: 1, user_answer_id: 4 }]));
        jest.spyOn(service, 'submitResponse').mockImplementation(() => Promise.resolve([3, 6, 9, 12]));
        jest.spyOn(service, 'checkScore').mockImplementation(() => Promise.resolve([{ is_correct: true }, { is_correct: true } ,{ is_correct: true } ,{ is_correct: true } ,{ is_correct: false }, { is_correct: false }]));
        jest.spyOn(service, 'submitQuizResult').mockImplementation(() => Promise.resolve([{ id: 1, score: 3 }]));
        jest.spyOn(service, 'quizStarRewards').mockImplementation(() => Promise.resolve(55));
        jest.spyOn(service, 'getFinancialKnowledge').mockImplementation(() => Promise.resolve([{ id: 1, knowledge_category_id: 1, knowledge_category: "理財",  content: "content", source: "google", knowledge_image: "test.jpg", url: "www.google.com", title: "title" }, { id: 2, knowledge_category_id: 1, knowledge_category: "理財", content: "content", source: "google", knowledge_image: "test.jpg", url: "www.google.com", title: "title" }]));
        jest.spyOn(service, 'getInvestmentKnowledge').mockImplementation(() => Promise.resolve([{ id: 3, knowledge_category_id: 2, knowledge_category: "投資", content: "content", source: "google", knowledge_image: "test.jpg", url: "www.google.com", title: "title" }, { id: 4, knowledge_category_id: 2, knowledge_category: "投資", content: "content", source: "google", knowledge_image: "test.jpg", url: "www.google.com", title: "title" }]));

        controller = new KnowledgeController(service);
        req = {
            body: {},
            params: {},
            user: {
                id: 1,
            }
        } as any as Request;
        res = {
            json: () => { }
        } as any as Response;
        resJson = jest.spyOn(res, 'json');

    })

    it('should get quizzes correctly if no completed quiz today', async () => {
        await controller.getQuizzes(req, res);
        expect(service.countCompletedQuizToday).toBeCalledTimes(1);
        expect(service.getCompletedQuizzes).toBeCalledTimes(1);
        expect(service.getQuizzes)
        expect(resJson).toBeCalledWith({ message: 'success', quizzes: { quizzes: [{ quiz_id: 3, quiz_name: "quiz 3" }, { quiz_id: 4, quiz_name: "quiz 4" }], completedQuizzes: [{ quiz_id: 1, quiz_name: "quiz 1", score: 4 }, { quiz_id: 2, quiz_name: "quiz 2", score: 5 }], completedQuizzesCount: 0 } })
    })

    it('should not load quizzes if there is completed quiz today', async () => {
        jest.spyOn(service, 'countCompletedQuizToday').mockImplementation(() => Promise.resolve([{ count: 1 }]));
        await controller.getQuizzes(req, res);
        expect(service.countCompletedQuizToday).toBeCalledTimes(1);
        expect(service.getCompletedQuizzes).toBeCalledTimes(1);
        expect(service.getQuizzes)
        expect(resJson).toBeCalledWith({ message: 'success', quizzes: { completedQuizzes: [{ quiz_id: 1, quiz_name: "quiz 1", score: 4 }, { quiz_id: 2, quiz_name: "quiz 2", score: 5 }], completedQuizzesCount: 1 } })
    })

    it('should get questions correctly', async () => {
        req.params.quizId = "1"
        await controller.getQuestions(req, res);
        expect(service.getQuestionChoicesByquizId).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: 'success', questions: [{ question_id: 1, image: "test.jpg", question: "question 1", choices: [{ choice_id: 1, choice: "choice 1" }, { choice_id: 2, choice: "choice 2" }] }, { question_id: 2, image: "test.jpg", question: "question 2", choices: [{ choice_id: 3, choice: "choice 3" }, { choice_id: 4, choice: "choice 4" }] }] })
    })

    it('shoud get revivions correctly', async () => {
        req.params.quizId = "1"
        await controller.getRevisions(req, res);
        expect(service.getModelAnswers).toBeCalledTimes(1);
        expect(service.getUserResponse).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: 'success', modelAnswers: [{ id: 1, question: "question 1", image: "test.jpg", quiz_id: 1, question_id: 1, choice: "choice 1", is_correct: true }, { id: 3, question: "question 2", image: "test.jpg", quiz_id: 1, question_id: 2, choice: "choice 3", is_correct: true }], userResponse: [{ id: 1, quiz_id: 1, user_id: 1, user_answer_id: 1 }, { id: 2, quiz_id: 1, user_id: 1, user_answer_id: 4 }] })
    })

    it('should handle submit correctly', async () => {
        req.body.questions = [
            {
                question_id: 1,
                question: "question 1",
                selectedOption: { choice_id: 1, choice: "choice 1" },
            },
            {
                question_id: 2,
                question: "question 2",
                selectedOption: { choice_id: 3, choice: "choice 3" },
            },
            {
                question_id: 3,
                question: "question 3",
                selectedOption: { choice_id: 5, choice: "choice 5" },
            },
        ]

        await controller.submitQuiz(req, res);
        expect(service.submitResponse).toBeCalledTimes(1);
        expect(service.checkScore).toBeCalledTimes(1);
        expect(service.submitQuizResult).toBeCalledTimes(1);
        expect(service.quizStarRewards).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: "success", submittedQuiz: [{ id: 1, score: 3 }], quizStarRewards: 55 })
    })

    it('should get Financial knowledge correctly', async () => {
        await controller.getFinancialKnowledge(req, res);
        expect(service.getFinancialKnowledge).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: 'success', financialKnowledge: [{ id: 1, knowledge_category_id: 1, knowledge_category: "理財",  content: "content", source: "google", knowledge_image: "test.jpg", url: "www.google.com", title: "title" }, { id: 2, knowledge_category_id: 1, knowledge_category: "理財", content: "content", source: "google", knowledge_image: "test.jpg", url: "www.google.com", title: "title" }]})
    })

    it('should get Investment knowledge correctly', async () => {
        await controller.getInvestmentKnowledge(req, res);
        expect(service.getInvestmentKnowledge).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({ message: 'success', investmentKnowledge: [{ id: 3, knowledge_category_id: 2, knowledge_category: "投資", content: "content", source: "google", knowledge_image: "test.jpg", url: "www.google.com", title: "title" }, { id: 4, knowledge_category_id: 2, knowledge_category: "投資", content: "content", source: "google", knowledge_image: "test.jpg", url: "www.google.com", title: "title" }]})
    })
})