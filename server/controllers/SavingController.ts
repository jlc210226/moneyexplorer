import { Request, Response } from "express";
import { SavingService } from "../services/SavingService";

export class SavingController {
    constructor(private savingService: SavingService) { }

    loadTotalIncomeByMonth = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const user_id = parseInt(tempID)
            // const month = 3;
            const getTotalIncome = await this.savingService.getTotalIncomeByMonth(user_id);

            if (!user_id) {
                res.status(400).json({ message: 'Invalid User ID' });
                return;
            }

            if (!getTotalIncome) {
                res.status(400).json({ message: 'Data does not exist!' });
                return;
            }
            
            // console.log(getTotalIncome[0].totalIncome)
            res.json({ getTotalIncome });

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loadTotalExpenseByMonth = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const user_id = parseInt(tempID)
            // const month = 3;
            const getTotalExpense = await this.savingService.getTotalExpenseByMonth(user_id);

            if (!user_id) {
                res.status(400).json({ message: 'Invalid User ID' });
                return;
            }

            if (!getTotalExpense) {
                res.status(400).json({ message: 'Data does not exist!' });
                return;
            }
            
            // console.log(getTotalExpense)
            res.json( getTotalExpense );

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loadTotalSavingByMonth = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const user_id = parseInt(tempID)
            // const month = 3;
            const getTotalSaving = await this.savingService.getTotalSavingByMonth(user_id);

            if (!user_id) {
                res.status(400).json({ message: 'Invalid User ID' });
                return;
            }

            if (!getTotalSaving) {
                res.status(400).json({ message: 'Data does not exist!' });
                return;
            }
            
            res.json( getTotalSaving );

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    createIncome = async (req: Request, res: Response) => {
        try {
            // console.log('[info] controller')
            const newSubmit = req.body;
            const tempID = req.user?.id as any
            const user_id = parseInt(tempID)
            const income_date = newSubmit.income_date;
            const income_category = newSubmit.income_category;
            const income_amount = newSubmit.income_amount;
            // const formData = { income_date, income_category, income_amount }
            // console.log(formData)
            const createIncome = await this.savingService.createIncome(
                user_id,
                income_date,
                income_category,
                income_amount
            );

            if (!user_id) {
                res.status(400).json({ message: 'Invalid User ID' });
                return;
            }

            if (!createIncome) {
                res.status(400).json({ message: 'Please submit your data!' });
                return;
            }
            
            res.json( createIncome );
            // console.log(createIncome)

        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    createExpense = async (req: Request, res: Response) => {
        try {
            // console.log('[info] controller')
            const newSubmit = req.body;
            const tempID = req.user?.id as any
            const user_id = parseInt(tempID)
            const expense_date = newSubmit.expense_date;
            const expense_category = newSubmit.expense_category;
            const expense_amount = newSubmit.expense_amount;
            // const formData = { expense_date, expense_category, expense_amount }
            // console.log(formData)
            const createExpense = await this.savingService.createExpense(
                user_id,
                expense_date,
                expense_category,
                expense_amount
            );

            if (!user_id) {
                res.status(400).json({ message: 'Invalid User ID' });
                return;
            }

            if (!createExpense) {
                res.status(400).json({ message: 'Please submit your data!' });
                return;
            }
            
            res.json( createExpense );
            return;

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    createSaving = async (req: Request, res: Response) => {
        try {
            // console.log('[info] controller')
            const newSubmit = req.body;
            const tempID = req.user?.id as any
            const user_id = parseInt(tempID)
            const saving_date = newSubmit.saving_date;
            const saving_amount = newSubmit.saving_amount;
            // const formData = { saving_date, saving_amount }
            // console.log(formData)
            const createSaving = await this.savingService.createSaving(
                user_id,
                saving_date,
                saving_amount
            );

            if (!user_id) {
                res.status(400).json({ message: 'Invalid User ID' });
                return;
            }

            if (!createSaving) {
                res.status(400).json({ message: 'Please submit your data!' });
                return;
            }
            
            res.json( createSaving );

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loadAllFinanceItem = async (req: Request, res: Response) => {
        try {
            const tempID = req.user?.id as any
            const user_id = parseInt(tempID)
            // console.log(user_id)
            const getAllIncomeItem = await this.savingService.getAllIncomeItem(user_id);
            const getAllExpenseItem = await this.savingService.getAllExpenseItem(user_id);
            const getAllSavingItem = await this.savingService.getAllSavingItem(user_id);

            if (!user_id) {
                res.status(400).json({ message: 'Invalid User ID' });
                return;
            }

            // if (!getAllIncomeItem || !getAllExpenseItem || !getAllSavingItem) {
            //     res.status(400).json({ message: 'Data does not exist' });
            //     return;
            // }
            // console.log({income: getAllIncomeItem, expense: getAllExpenseItem, saving: getAllSavingItem})
            res.json({income: getAllIncomeItem, expense: getAllExpenseItem, saving: getAllSavingItem});

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    deleteIncome = async (req: Request, res: Response) => {
        try {
            const newDelete = req.params.id;
            // console.log(newDelete)
            // const id = newDelete;
            await this.savingService.deleteIncome(newDelete);
            if (!newDelete) {
                res.status(400).json({ message: 'Please click to delete item' });
                return;
            }
            
            res.json({ message: 'success' });

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    deleteExpense = async (req: Request, res: Response) => {
        try {
            const newDelete = req.params.id;
            // console.log(newDelete)
            // const id = newDelete;
            await this.savingService.deleteExpense(newDelete);
            if (!newDelete) {
                res.status(400).json({ message: 'Please click to delete item' });
                return;
            }
            
            res.json({ message: 'success' });

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    deleteSaving = async (req: Request, res: Response) => {
        try {
            const newDelete = req.params.id;
            const tempID = req.user?.id as any
            const user_id = parseInt(tempID)
            // console.log(newDelete)
            // const id = newDelete;
            await this.savingService.deleteSaving(newDelete, user_id);
            if (!newDelete) {
                res.status(400).json({ message: 'Please click to delete item' });
                return;
            }
            
            res.json({ message: 'success' });

        } catch (err) {
            // console.error(err.message);
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

}
