import Knex from 'knex';
import { tables } from "../utils/tables";
import * as knexfile from '../knexfile';
const knex = Knex(knexfile["test"]);
import { SavingService } from './SavingService';
// import { userService } from '../main';

describe("SavingService", () => {
    let savingService: SavingService;

    beforeAll(async () => {
        await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();
    });

    beforeEach(async () => {
        savingService = new SavingService(knex);
        // let income_category_id: number;
        // let income_amount: number;
        // let income_date: Date;
        // let expense_category_id: number;
        // let expense_amount: number;
        // let expense_date: Date;
        // let saving_amount: number;
        // let saving_date: Date;


        await knex(tables.userExpensesTable).del();
        await knex(tables.userIncomesTable).del();
        await knex(tables.userSavingTable).del();
        await knex(tables.expenseCategoryTable).del();
        await knex(tables.incomeCategoryTable).del();

        const seededexpenseCategoryTable = (await knex.insert(
            [
                { expense_category: '食物' },
                { expense_category: '衣服' },
                { expense_category: '交通' },
                { expense_category: '娛樂' },
                { expense_category: '禮物' },
                { expense_category: '其他' },
            ]
        )
            .into(tables.expenseCategoryTable)
            .returning(["id", "expense_category"])) as Array<{ id: number, expense_category: string }>

        const expenseCategoryMapping = seededexpenseCategoryTable.reduce(
            (acc, expenseCat) => acc.set(expenseCat.expense_category, expenseCat.id),
            new Map<string, number>()
        );

        await knex.insert([{
            expense_category_id: expenseCategoryMapping.get("食物"),
            expense_amount: 100,
            expense_date: new Date(),
        }, {
            expense_category_id: expenseCategoryMapping.get("衣服"),
            expense_amount: 50,
            expense_date: new Date(),
        }, {
            expense_category_id: expenseCategoryMapping.get("交通"),
            expense_amount: 70,
            expense_date: new Date(),
        }, {
            expense_category_id: expenseCategoryMapping.get("娛樂"),
            expense_amount: 80,
            expense_date: new Date(),
        }, {
            expense_category_id: expenseCategoryMapping.get("禮物"),
            expense_amount: 120,
            expense_date: new Date(),
        }, {
            expense_category_id: expenseCategoryMapping.get("其他"),
            expense_amount: 10,
            expense_date: new Date(),
        }
        ]).into(tables.userExpensesTable);

        const seededincomeCategoryTable = (await knex.insert(
            [
                { income_category: '零用錢' },
                { income_category: '額外獎勵' },
                { income_category: '其他' },
            ]
        )
            .into(tables.incomeCategoryTable)
            .returning(["id", "income_category"])) as Array<{ id: number, income_category: string }>

        const incomeCategoryMapping = seededincomeCategoryTable.reduce(
                (acc, incomeCat) => acc.set(incomeCat.income_category, incomeCat.id),
                new Map<string, number>()
            );

        await knex.insert([{
                income_category_id: incomeCategoryMapping.get("零用錢"),
                income_amount: 100,
                income_date: new Date(),
            }, {
                income_category_id: incomeCategoryMapping.get("額外獎勵"),
                income_amount: 50,
                income_date: new Date(),
            }, {
                income_category_id: incomeCategoryMapping.get("其他"),
                income_amount: 10,
                income_date: new Date(),
            }
            ]).into(tables.userIncomesTable);

        await knex.insert([{
                saving_amount: 100,
                saving_date: new Date(),
            }, {
                saving_amount: 50,
                saving_date: new Date(),
            }
            ]).into(tables.userSavingTable);

    })

    it("should add new income item correctly", async () => {
        const id = (await savingService.createIncome(1, new Date(), '零用錢', 500))
        expect(id.length).toBe(1)
    })

    it("should add new expense item correctly", async () => {
        const id = (await savingService.createExpense(1, new Date(), '娛樂', 99))
        expect(id.length).toBe(1)
    })

    it("should add new saving item correctly", async () => {
        const id = (await savingService.createSaving(1, new Date(), 200))
        expect(id.length).toBe(1)
    })

    it("should get all income items", async() => {
        const allIncomeItems = (await savingService.getAllIncomeItem(1))
        expect (allIncomeItems.length).toBe(0)
    })

    it("should get all expense items", async() => {
        const allExpenseItems = (await savingService.getAllExpenseItem(1))
        expect (allExpenseItems.length).toBe(0)
    })

    it("should get all saving items", async() => {
        const allSavingItems = (await savingService.getAllSavingItem(1))
        expect (allSavingItems.length).toBe(0)
    })

    it("should get total income by month", async() => {
        const incomeByMonthArr = (await savingService.getTotalIncomeByMonth(1))
        expect (incomeByMonthArr.length).toBe(0)
    })

    it("should get total expense by month", async() => {
        const expenseByMonthArr = (await savingService.getTotalExpenseByMonth(1))
        expect (expenseByMonthArr.length).toBe(0)
    })

    it("should get total saving by month", async() => {
        const savingByMonthArr = (await savingService.getTotalSavingByMonth(1))
        expect (savingByMonthArr.length).toBe(0)
    })
    
    afterAll(async () => {
        await knex.destroy();
    });

})