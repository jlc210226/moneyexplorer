import { UserProfiles } from './../utils/models';
import { Knex } from "knex";
import { User } from "../utils/models"
import { tables } from "../utils/tables";
import { hashPassword } from "../utils/hash"

export class UserService {
    constructor(private knex: Knex) { }

    async getUser(loginName: string) {
        const user = await this.knex<User>(tables.usersTable)
            .select("*", { id: "users.id" })
            .join('user_profiles', 'users.id', 'user_profiles.user_id')
            .where("login_name", loginName)
            .first();

        return user;
    }

    async getUserById(id: number) {
        const user = await this.knex<User>(tables.usersTable)
            .where("id", id)
            .first();

        return user;
    }

    async getUserByEmail(email: string) {
        const user = await this.knex<User>(tables.userProfilesTable)
            .join('users', 'users.id', 'user_profiles.user_id')
            .where("email", email)
            .first();

        return user;
    }

    async registerNewUser(loginName: string, password: string, pin: number) {

        const hashedPassword = await hashPassword(password)

        const userId = await this.knex<User>(tables.usersTable)
            .insert({
                login_name: loginName,
                password: hashedPassword,
                unlock_pin: pin,
                account_status: "pending"
            })
            .returning('id')

        return userId

    }

    async deleteUser(loginName: string) {

        const [userId] = await this.knex<User>(tables.usersTable)
            .del()
            .where('login_name', loginName)
            .returning('id');

        return userId
    }

    async createUserProfile(nickname: string, gender: string, birthday: Date, email: string, userId: number, iconId: number) {

        const [id] = await this.knex<UserProfiles>(tables.userProfilesTable)
            .insert({
                nickname: nickname,
                gender: gender,
                date_of_birth: birthday,
                email: email,
                user_id: userId,
                avatar_id: iconId,
                current_star: 0,
                total_star: 0,
                total_saving: 0,
            }).returning("id")

        return id

    }

    async getUserInfo(userId: number) {
        const result = await this.knex<UserProfiles>(tables.userProfilesTable).select('*').where("user_id", userId);

        const userInfo = result[0]

        return userInfo

    }

    async editProfile(userId: number, nickname: string, gender: string, birthday: Date, iconId: number) {

        const date = new Date();

        const [id] = await this.knex<UserProfiles>(tables.userProfilesTable)
            .update({
                nickname: nickname,
                gender: gender,
                date_of_birth: birthday,
                avatar_id: iconId,
                updated_at: date,
            }).where("user_id", userId)
            .returning("id")

        return id

    }

    async updatePassword(userId: number, password: string) {

        const hashedPassword = await hashPassword(password)

        const [id] = await this.knex<User>(tables.usersTable)
            .update({ password: hashedPassword, updated_at: new Date() })
            .where("id", userId)
            .returning("id")

        return id

    }

    async activateAccount(email: string) {

        const user = await this.knex<UserProfiles>(tables.userProfilesTable).select("user_id")
            .where("email", email).first();

        const userId = user?.user_id;

        const [id] = await this.knex<User>(tables.usersTable)
            .update({ account_status: "active" }).where("id", userId)
            .returning("id")

        return id

    }

    async editPin(userId: number, pin: number) {

        const [id] = await this.knex<User>(tables.usersTable)
            .update({ unlock_pin: pin })
            .where("id", userId)
            .returning("id")

        return id
    }

}