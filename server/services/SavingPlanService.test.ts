import Knex from 'knex';
import { tables } from '../utils/tables';
import * as knexfile from '../knexfile';
const knex = Knex(knexfile["test"]);
import { SavingPlanService } from './SavingPlanService';

describe("SavingPlanService", () => {
    let savingPlanService: SavingPlanService;
    let dateSpy: Date;

    let userMapping: Map<string, number>

    beforeAll(async () => {
        await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();

        const seededUsers = await knex(tables.usersTable).insert([{
            login_name: "lulu",
            password: "abcd",
            unlock_pin: 1234,
            created_at: dateSpy,
            updated_at: dateSpy,
            account_status: "active",
        },{
            login_name: "momo",
            password: "abcd",
            unlock_pin: 1234,
            created_at: dateSpy,
            updated_at: dateSpy,
            account_status: "active",
        },]).returning(["id", "login_name"]) as Array<{id: number, login_name: string}>

        userMapping = seededUsers.reduce((acc, user) => acc.set(user.login_name, user.id), new Map<string, number>())

    })

    beforeEach(async () => {
        
        savingPlanService = new SavingPlanService(knex);

        await knex(tables.userSavingGoalTable).del();
       
        await knex.insert([{
            user_id: userMapping.get("lulu"),
            goal_name: "Buy story books",
            goal_amount: 1000,
            is_complete: false,
            created_at: dateSpy,
            updated_at: dateSpy
        }, {
            user_id: userMapping.get("lulu"),
            goal_name: "GrandPa Birthday Gift",
            goal_amount: 200,
            is_complete: true,
            created_at: dateSpy,
            updated_at: dateSpy
        }, {
            user_id: userMapping.get("lulu"),
            goal_name: "GrandMa Birthday Gift",
            goal_amount: 200,
            is_complete: true,
            created_at: dateSpy,
            updated_at: dateSpy
        },{
            user_id: userMapping.get("momo"),
            goal_name: "Go DisneyLand",
            goal_amount: 2000,
            is_complete: false,
            created_at: dateSpy,
            updated_at: dateSpy
        }, {
            user_id: userMapping.get("momo"),
            goal_name: "Go Ocean Park",
            goal_amount: 500,
            is_complete: true,
            created_at: dateSpy,
            updated_at: dateSpy
        }]).into(tables.userSavingGoalTable)
    })

    it("should get correct saving plan", async() => {
        const result = await savingPlanService.getSavingPlans(4); // means Lulu
        expect(result.length).toBe(1);
    })

    it("should get correct completed plan", async() => {
        const result = await savingPlanService.getCompletedSavingPlans(4);
        expect(result.length).toBe(2);
    })

    it("should get correct total saving", async() => {
        const result = await savingPlanService.getTotalSaving(3);
        expect (result.total_saving).toBe(1000);
    })

    // problematic...
    it("should mark goal complete correctly", async()=> {
        const result = await savingPlanService.markGoalComplete(4, 22);
        expect(result.message).toBe("Success")
    })

    // problematic...
    it("should delete goal correctly", async () => {
        const result = await savingPlanService.deleteGoal(4, 37);
        expect(result.message).toBe("Success")
    })

    it("should create goal correctly", async()=> {
        const result = await savingPlanService.createSavingGoal(4, "GO TO PICNIC", 300);
        expect(result.message).toBe("Success")
    })

    afterAll(async () => {
        await knex.destroy();
    })

})