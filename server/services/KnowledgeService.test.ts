import Knex from 'knex';
import { tables } from "../utils/tables";
import * as knexfile from '../knexfile'; // Assuming you test case is inside `services/ folder`
const knex = Knex(knexfile["test"]); // Now the connection is a testing connection.
import { KnowledgeService } from './KnowledgeService';
import { userService } from '../main';

describe("KnowledgeService", () => {
    let knowledgeService: KnowledgeService;
    let quizId: number;
    let choicesMapping: Map<string, number>;

    beforeAll(async () => {
        await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();
    });

    beforeEach(async () => {
        knowledgeService = new KnowledgeService(knex);

        await knex(tables.quizResponseTable).del();
        await knex(tables.questionChoicesTable).del();
        await knex(tables.questionTable).del();
        await knex(tables.quizResultTable).del();
        await knex(tables.quizTable).del();
        await knex(tables.quizCategoryTable).del();
        await knex(tables.knowledgeTable).del();
        await knex(tables.knowledgeCategoryTable).del();

        const seededKnowledgeCategory = (await knex.insert([{
            knowledge_category: "投資"
        }, {
            knowledge_category: "理財"
        }
        ]).into(tables.knowledgeCategoryTable).returning(["id", "knowledge_category"])) as Array<{ id: number, knowledge_category: string }>;

        const knowledgeCatMapping = seededKnowledgeCategory.reduce(
            (acc, knowledgeCat) => acc.set(knowledgeCat.knowledge_category, knowledgeCat.id),
            new Map<string, number>()
        );

        await knex.insert([{
            knowledge_category_id: knowledgeCatMapping.get("投資"),
            title: "Investment Title",
            content: "Investment content",
            source: "Investment source",
            knowledge_image: "test.jpg",
            url: "www.google.com"
        }, {
            knowledge_category_id: knowledgeCatMapping.get("理財"),
            title: "Financial Title",
            content: "Financial content",
            source: "Financial source",
            knowledge_image: "test.jpg",
            url: "www.google.com"
        }
        ]).into(tables.knowledgeTable);

        const seededQuizCategory = (await knex.insert([{
            quiz_category: "投資"
        }, {
            quiz_category: "理財"
        }
        ]).into(tables.quizCategoryTable).returning(["id", "quiz_category"])) as Array<{ id: number, quiz_category: string }>;

        const quizCatMapping = seededQuizCategory.reduce(
            (acc, quizCat) => acc.set(quizCat.quiz_category, quizCat.id),
            new Map<string, number>()
        );

        const seededQuiz = await knex.insert([{
            quiz_category_id: quizCatMapping.get("投資"),
            quiz_name: "quiz",
            is_active: true
        }]).into(tables.quizTable).returning(["id", "quiz_name"]) as Array<{ id: number, quiz_name: string }>;

        const quizMapping = seededQuiz.reduce(
            (acc, quiz) => acc.set(quiz.quiz_name, quiz.id),
            new Map<string, number>()
        );

        quizId = (quizMapping.get("quiz")) as number


        const seededQuestions = (await knex.insert([{
            question: "question 1",
            image: "test.jpg",
            quiz_id: quizMapping.get("quiz")
        }, {
            question: "question 2",
            image: "test.jpg",
            quiz_id: quizMapping.get("quiz")
        }, {
            question: "question 3",
            image: "test.jpg",
            quiz_id: quizMapping.get("quiz")
        }
        ]).into(tables.questionTable).returning(["id", "question"])) as Array<{ id: number, question: string }>;

        const questionMapping = seededQuestions.reduce(
            (acc, q) => acc.set(q.question, q.id),
            new Map<string, number>()
        );

        //questionIds = [ questionMapping.get("question 1") , questionMapping.get("question 2") , questionMapping.get("question 3") ] as number[];
        const seededChoices = await knex.insert([{
            question_id: questionMapping.get("question 1"),
            is_correct: true,
            choice: "choice 1"
        }, {
            question_id: questionMapping.get("question 1"),
            is_correct: false,
            choice: "choice 2"
        }, {
            question_id: questionMapping.get("question 2"),
            is_correct: true,
            choice: "choice 3"
        }, {
            question_id: questionMapping.get("question 2"),
            is_correct: false,
            choice: "choice 4"
        }, {
            question_id: questionMapping.get("question 3"),
            is_correct: true,
            choice: "choice 5"
        }, {
            question_id: questionMapping.get("question 3"),
            is_correct: false,
            choice: "choice 6"
        },
        ]).into(tables.questionChoicesTable).returning(["id", "choice"]) as Array<{ id: number, choice: string }>;

        choicesMapping = seededChoices.reduce(
            (acc, choice) => acc.set(choice.choice, choice.id),
            new Map<string, number>()
        )

        await knex.insert([{
            user_id: 1,
            user_answer_id: choicesMapping.get("choice 1"),
        }, {
            user_id: 1,
            user_answer_id: choicesMapping.get("choice 3"),
        }, {
            user_id: 1,
            user_answer_id: choicesMapping.get("choice 6"),
        }]).into(tables.quizResponseTable)

        await knex.insert([{
            user_id: 1,
            quiz_id: quizMapping.get("quiz"),
            score: 2,
        }]).into(tables.quizResultTable)

    })

    it("should count Completed quiz today correctly", async () => {
        const countCompletedQuizToday = (await knowledgeService.countCompletedQuizToday(1)) as Array<{ count: string }>
        expect(parseInt(countCompletedQuizToday[0].count)).toBe(1)
    })

    it("should get completed quizzes correctly", async () => {
        const completedQuizzes = await knowledgeService.getCompletedQuizzes(1)
        expect(completedQuizzes.length).toBe(1);
    })

    it("should get all quizzes correctly", async () => {
        const quizzes = await knowledgeService.getQuizzes([])
        expect(quizzes.length).toBe(1);
    })

    it("should exclude quizz correctly", async () => {
        const quizzes = await knowledgeService.getQuizzes([quizId])
        expect(quizzes.length).toBe(0);
    })

    it("should get choices by quiz Id correctly", async () => {
        const questions = await knowledgeService.getQuestionChoicesByquizId(quizId)
        expect(questions.length).toBe(6);
    })

    it("should get modelAnwsers by quiz Id correctly", async () => {
        const answers = await knowledgeService.getModelAnswers(quizId)
        expect(answers.length).toBe(3);

        for (let ans of answers) {
            expect(ans).toBeTruthy();
        }
    })

    it("should get user response corectly", async () => {
        const userResponse = await knowledgeService.getUserResponse(quizId, 1)
        expect(userResponse.length).toBe(3);
    })

    it("should handle submit response correctly", async () => {
        const anwserIds = ([choicesMapping.get("choice 1"), choicesMapping.get("choice 3"), choicesMapping.get("choice 6")]) as number[]
        const submittedResponse = await knowledgeService.submitResponse(1, anwserIds)
        expect(submittedResponse.length).toBe(3);
    })

    it("should checkScore correctly", async () => {
        const anwserIds = ([choicesMapping.get("choice 1"), choicesMapping.get("choice 3"), choicesMapping.get("choice 6")]) as number[]
        const checkScore = await knowledgeService.checkScore(anwserIds)

        let score = 0;
        for (let choice of checkScore) {
            if (choice.is_correct) {
                score += 1;
            }
        }
        expect(score).toBe(2);
    })

    it("should submit quiz result and insert score correctly", async () => {
        const submitQuizResult = (await knowledgeService.submitQuizResult(1, quizId, 2)) as Array<{ id: number, score: number }>;

        expect(submitQuizResult[0].score).toBe(2);
        expect(submitQuizResult.length).toBe(1);
    })

    it("should reward stars correctly", async () => {
        const userProfiles = await userService.getUserInfo(1)
        console.log(userProfiles.current_star)
        console.log(userProfiles.total_star)

        await knowledgeService.quizStarRewards(1, 2)

        const rewardedUserProfiles = await userService.getUserInfo(1)

        expect(rewardedUserProfiles.current_star).toBe(userProfiles.current_star + 2)
        expect(rewardedUserProfiles.total_star).toBe(userProfiles.total_star + 2)

    })

    it("should get all Financial Knowledge", async () => {
        const financialKnowledge = (await knowledgeService.getFinancialKnowledge());
        expect(financialKnowledge.length).toBe(1);
    });

    it("should get all Investment Knowledge", async () => {
        const investmentKnowledge = (await knowledgeService.getInvestmentKnowledge());
        expect(investmentKnowledge.length).toBe(1);
    });


    afterAll(async () => {
        await knex.destroy();
    });

})