import { UserSavingGoals, UserProfiles } from './../utils/models';
import { Knex } from "knex";
import { tables } from "../utils/tables";

export class SavingPlanService {
    constructor(private knex: Knex) { }

    async getSavingPlans(userId: number) {
        const result = await this.knex<UserSavingGoals>(tables.userSavingGoalTable)
            .where("user_id", userId)
            .andWhere("is_complete", false)
            .orderBy("created_at", "desc")

        return result
    }

    async getCompletedSavingPlans(userId: number) {
        const result = await this.knex<UserSavingGoals>(tables.userSavingGoalTable)
            .where("user_id", userId)
            .andWhere("is_complete", true)
            .orderBy("updated_at", "desc")

        return result
    }

    async getTotalSaving(userId: number) {
        const [result] = await this.knex<UserProfiles>(tables.userProfilesTable)
            .column('total_saving')
            .where("user_id", userId)

        return result;
    }

    async markGoalComplete(userId: number, goalId: number) {

        // update status
        const count = await this.knex<UserSavingGoals>(tables.userSavingGoalTable)
            .update({ is_complete: true, updated_at: new Date() })
            .where("user_id", userId)
            .andWhere("id", goalId)
            .count("*")

        return { count: count }

    }

    async deleteGoal(userId: number, goalId: number) {

        const count = await this.knex<UserSavingGoals>(tables.userSavingGoalTable)
            .where("user_id", userId)
            .andWhere("id", goalId)
            .del()
            .count("*")

        return { count: count }
    }

    async updateTotalSaving(userId: number, newSaving: number) {

        const count = await this.knex<UserProfiles>(tables.userProfilesTable)
            .update({ total_saving: newSaving })
            .where("user_id", userId)
            .count("*");

        return {count: count}
    }

    async createSavingGoal(userId: number, goalName: string, goalAmount: number) {

        const [newGoalId] = await this.knex<UserSavingGoals>(tables.userSavingGoalTable)
            .insert({
                user_id: userId,
                goal_name: goalName,
                goal_amount: goalAmount,
                is_complete: false,
            }).returning("id")

        return { message: "success", newGoalId: newGoalId }

    }
}