import {Knex} from "knex";

export class MissionService {
 
    constructor (private knex: Knex) {}

    async loadCurrentStar(userID: number) {
        const star = await this.knex("user_profiles")
        .select('current_star')
        .where('id', userID)
        return star[0]
    }

    async getPinNumberbyUserID (userID: number) {
        const result = await this.knex("users")
        .select('unlock_pin')
        .where('id', userID)
        return result
    }

    async getMissionStatusIDByMissionStatusName (mission_status_name: string) {
        const result = await this.knex("mission_status")
        .select ("id")
        .where ("mission_status", mission_status_name)
        return result;
    }

    async changeMissionStatusByMissionID (mission_id:number, mission_status_id: number) {
        const result = await this.knex("assigned_mission")
        .update ({"mission_status_id": mission_status_id})
        .where("id" , mission_id)
        .returning("mission_status_id")
        return result;
    }
    async updateCurrentStar (reward_stars: number, userID: number) {
        const result = await this.knex("user_profiles")
        .where("user_id", userID)
        .increment("current_star", reward_stars)
        .returning("current_star")
        return result
    }

    async updateTotalStar (reward_stars:number, userID:number) {
        const result = await this.knex("user_profiles")
        .where("user_id", userID)
        .increment("total_star", reward_stars)
        .returning("total_star")
        return result
    }

    async getUpcomingMissions ( userID: number ) {
        const result = await this.knex("assigned_mission")
        .where("assigned_mission.user_id", userID)
        .innerJoin("mission_template", "assigned_mission.mission_id" ,"mission_template.id")
        .innerJoin("mission_status", "assigned_mission.mission_status_id", "mission_status.id")
        .select('mission_status.*', 'mission_template.*', {assigned_mission_id: 'assigned_mission.id'}, 'assigned_mission.*')
        .orderBy('mission_exp_date')
        console.log("upcoming", result)
        return result;
    }

    async getAllMissionTemplates ( userID: number ) {
        const result = await this.knex ("mission_template") 
        .where ("user_id", userID)
        .orWhere("mission_type", "default")
        .select("*")
        return result;
    }

    async addNewUpcomingMission ( userID: number, mission_template_id: number,  mission_exp_date: Date, reward_star: number,mission_status_id:number  ) {
        const result = await this.knex("assigned_mission") 
        .insert({"user_id": userID, "mission_id": mission_template_id, "mission_exp_date": mission_exp_date, "mission_status_id": mission_status_id, "reward_stars": reward_star})
        .returning("user_id")
        return result;
    }

    async addCustomizedMissionTemplate (mission_name:string, userID: number) {
        const mission_id = await this.knex("mission_template")
        .insert({"mission_name": mission_name, "mission_image": "astronautlyingdown.jpg", "mission_type": "customized", "user_id": userID})
        .returning("id")
        return mission_id;
    }

    async getAllMissionTargets(userID:number) {
        const result = await this.knex("mission_target")
        .select("*")
        .where("user_id", userID) 
        return result
    }

    async addNewMissionTarget (userID:number, target_name: string, needed_star: number) {
        const result = await this.knex("mission_target")
        .insert({"user_id": userID, "target_name": target_name, "needed_star":needed_star, "is_complete": false})
        .returning("id")
        return result
    }

    async redeemMissionTarget (target_id: number) {
        const result = await this.knex("mission_target")
        .update({"is_complete": true})
        .where("id", target_id)
        .returning("id")
        return result
    }

    async subtractStarNum (userID:number,needed_star:number) {
        const result = await this.knex("user_profiles")
        .where("user_id", userID)
        .decrement("current_star", needed_star)
        .returning("current_star")
        return result
    }
}