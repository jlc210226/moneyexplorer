import { Knex } from "knex";
import { UserExpenses, UserIncomes, UserProfiles, UserSavings } from "../utils/models"
import { tables } from "../utils/tables";

export class SavingService {
    constructor(private knex: Knex) {}

    async getTotalIncomeByMonth(user_id: number) {

        const totalIncome = await this.knex<UserIncomes>(tables.userIncomesTable)
            .select(this.knex.raw(`date_trunc('month', income_date)`))
            .sum({ totalIncome: 'income_amount' })
            .from('user_incomes')
            .where('user_id', user_id)
            .groupByRaw(`date_trunc('month', income_date)`)
            // .andWhereRaw(`EXTRACT(MONTH FROM income_date) = ?`, month)
            // .groupBy('income_amount')
            // const totalIncome = await this.knex.raw(`SELECT SUM (income_amount) AS total FROM user_incomes WHERE EXTRACT(MONTH FROM income_date) = 1`)
            // console.log(totalIncome)
        return totalIncome;
    }

    async getTotalExpenseByMonth(user_id: number) {

        const totalExpense = await this.knex<UserExpenses>(tables.userExpensesTable)
            .select(this.knex.raw(`date_trunc('month', expense_date)`))
            .sum({ totalExpense: 'expense_amount' })
            .from('user_expenses')
            .where('user_id', user_id)
            .groupByRaw(`date_trunc('month', expense_date)`)
            // console.log(totalExpense)
        return totalExpense;
    }

    async getTotalSavingByMonth(user_id: number) {

        const totalSaving = await this.knex<UserSavings>(tables.userSavingTable)
            .select(this.knex.raw(`date_trunc('month', saving_date)`))
            .sum({ totalSaving: 'saving_amount' })
            .from('user_saving')
            .where('user_id', user_id)
            .groupByRaw(`date_trunc('month', saving_date)`)
            // console.log(totalExpense)
        return totalSaving;
    }

    async createIncome(user_id: number, income_date: Date, income_category: string, income_amount: number) {

        const incomeCategoryID: number = await this.knex
        .select('id')
        .from(tables.incomeCategoryTable)
        .where('income_category', income_category)
        .returning('id')
        // console.log(incomeCategoryID[0].id)

        const createIncome = await this.knex<UserIncomes>(tables.userIncomesTable)
            .insert({
                'user_id': user_id,
                'income_date': income_date,
                'income_category_id': incomeCategoryID[0].id,
                'income_amount': income_amount,
             })
            .returning('*')

        return createIncome;
    }

    async createExpense(user_id: number, expense_date: Date, expense_category: string, expense_amount: number) {

        const expenseCategoryID: number = await this.knex
        .select('id')
        .from(tables.expenseCategoryTable)
        .where('expense_category', expense_category)
        .returning('id')
        // console.log(expenseCategoryID[0].id)

        const createExpense = await this.knex<UserExpenses>(tables.userExpensesTable)
            .insert({
                'user_id': user_id,
                'expense_date': expense_date,
                'expense_category_id': expenseCategoryID[0].id,
                'expense_amount': expense_amount,
             })
            .returning('*')

        return createExpense;
    }

    async createSaving(user_id: number, saving_date: Date, saving_amount: number) {

        const createSaving = await this.knex<UserSavings>(tables.userSavingTable)
            .insert({
                'user_id': user_id,
                'saving_date': saving_date,
                'saving_amount': saving_amount,
             })
            .returning('*')
        
        const getSavingTotal = await this.knex<UserProfiles>(tables.userProfilesTable)
            .select('total_saving')
            .where('user_id', user_id)
        
        const addSavingTotal = await this.knex<UserProfiles>(tables.userProfilesTable)
            .update({
                'total_saving': saving_amount + getSavingTotal[0].total_saving,
             })
             .where('user_id', user_id)

        console.log(addSavingTotal)
        return createSaving;
    }

    async getAllIncomeItem(user_id: number) {

        const allIncomeItem = await this.knex<UserIncomes>(tables.userIncomesTable)
            .column('user_incomes.id', { amount: 'user_incomes.income_amount' }, { item: 'income_category.income_category' }, { date: 'user_incomes.income_date' })
            .innerJoin(tables.incomeCategoryTable, 'income_category.id', 'user_incomes.income_category_id')
            .where('user_id', user_id)
        return allIncomeItem;
    }

    async getAllExpenseItem(user_id: number) {

        const allExpenseItem = await this.knex<UserExpenses>(tables.userExpensesTable)
            .column('user_expenses.id', { amount: 'user_expenses.expense_amount' }, { item: 'expense_category.expense_category' }, { date: 'user_expenses.expense_date' })
            .innerJoin(tables.expenseCategoryTable, 'expense_category.id', 'user_expenses.expense_category_id')
            .where('user_id', user_id)
        return allExpenseItem;
    }

    async getAllSavingItem(user_id: number) {

        const allSavingItem = await this.knex<UserSavings>(tables.userSavingTable)
            .column('user_saving.id', { amount: 'user_saving.saving_amount' }, { date: 'user_saving.saving_date' })
            .where('user_id', user_id)
        return allSavingItem;
    }
 
    async deleteIncome(id: any) {
        const deleteIncome = await this.knex<UserIncomes>(tables.userIncomesTable)
            .del()
            .where('id', id)
        return deleteIncome;
    }
     
    async deleteExpense(id: any) {
        const deleteExpense = await this.knex<UserExpenses>(tables.userExpensesTable)
            .del()
            .where('id', id)
        return deleteExpense;
    }

    async deleteSaving(id: any, user_id: any) {
        const deleteSaving = await this.knex<UserSavings>(tables.userSavingTable)
            .del()
            .where('id', id)
            .returning('*')
        // return deleteSaving;
        // console.log(deleteSaving[0].saving_amount)

        const getSavingTotal = await this.knex<UserProfiles>(tables.userProfilesTable)
        .select('total_saving')
        .where('user_id', user_id)
    
        const minusSavingTotal = await this.knex<UserProfiles>(tables.userProfilesTable)
        .update({
            'total_saving': getSavingTotal[0].total_saving - deleteSaving[0].saving_amount
         })
         .where('user_id', user_id)
         console.log(minusSavingTotal)
         return deleteSaving;
    }

} 