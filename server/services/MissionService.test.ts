import Knex from 'knex';
import { tables } from '../utils/tables'
import * as knexfile from '../knexfile'
const knex = Knex(knexfile["test"]);
import { MissionService } from './MissionService';
// import { userService } from '../main';

describe("MissionService", () => {
    let missionService: MissionService;
    let assigned_mission_id: any;
    let change_mission_status_id: number;
    let mission_template_id: number;
    let inprogress_missionID : number;
    let exp_date: Date;
    let target_id: number;
    let missionTemplateMapping: Map<string, number>;
    let missionStatusMapping: Map<string, number>;
    beforeAll(async () => {
        await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();
    });

    beforeEach(async () => {
        missionService = new MissionService(knex);

        await knex(tables.missionTargetTable).del();
        await knex(tables.assignedMissionTable).del();
        await knex(tables.missionStatusTable).del();
        await knex(tables.missionTemplateTable).del();
        const seededMissionTemplate = (await knex.insert([{ mission_name: '洗碗', mission_image: '1.jpg', mission_type: 'default' }, { mission_name: '整理房間', mission_image: '2.jpg', mission_type: 'default' }]).into(tables.missionTemplateTable).returning(["id", "mission_name"])) as Array<{ id: number, mission_name: string }>

        missionTemplateMapping = seededMissionTemplate.reduce((acc, missionTemplate) => acc.set(missionTemplate.mission_name, missionTemplate.id),
            new Map<string, number>())
        mission_template_id = (missionTemplateMapping.get('洗碗')) as number
        const seededMissionStatus = (await knex.insert([{ id: 1, mission_status: "in progress" }, { id: 2, mission_status: "complete" }, { id: 3, mission_status: "fail" }]).into(tables.missionStatusTable).returning(["id", "mission_status"])) as Array<{ id: number, mission_status: string }>

        missionStatusMapping = seededMissionStatus.reduce((acc, missionStatus) => acc.set(missionStatus.mission_status, missionStatus.id), new Map<string, number>())

        const seededAssignedMission = (await knex.insert([{
            user_id: 1,
            mission_id: missionTemplateMapping.get("洗碗"),
            mission_exp_date: "2021-05-04",
            mission_status_id: missionStatusMapping.get("in progress"),
            reward_stars: 10
        }, {
            user_id: 2,
            mission_id: missionTemplateMapping.get("整理房間"),
            mission_exp_date: "2021-05-10",
            mission_status_id: missionStatusMapping.get("complete"),
            reward_stars: 10
        }, {
            user_id: 3,
            mission_id: missionTemplateMapping.get("整理房間"),
            mission_exp_date: "2021-05-1",
            mission_status_id: missionStatusMapping.get("fail"),
            reward_stars: 10
        }]).into(tables.assignedMissionTable).returning(["id", "user_id"])) as Array<{ id: number, user_id: number }>
        const assignedMissionMapping = seededAssignedMission.reduce((acc, assignedMission) => acc.set(assignedMission.user_id, assignedMission.id), new Map<number, number>())
       const seededMissionTarget = (await knex.insert([{
            user_id: 1,
            target_name: "disneyland",
            needed_star: 5,
            is_complete: false
        }, {
            user_id: 2,
            target_name: "play",
            needed_star: 10,
            is_complete: false
        }, {
            user_id: 3,
            target_name: "換電話",
            needed_star: 100,
            is_complete: true
        }]).into(tables.missionTargetTable).returning(["id", "target_name"])) as Array <{ id: number, target_name: string }>
        const missionTargetMapping = seededMissionTarget.reduce((acc, missionTarget) => acc.set( missionTarget.target_name, missionTarget.id), new Map<string, number>())
        assigned_mission_id = assignedMissionMapping.get(1)
        change_mission_status_id = (missionStatusMapping.get("complete")) as number
        inprogress_missionID = (missionStatusMapping.get("in progress")) as number
        exp_date = new Date()
        target_id = (missionTargetMapping.get("play")) as number
    })


    it("should get current star number correctly", async () => {
        const current_star = (await missionService.loadCurrentStar(1))
        expect(current_star.current_star).toBe(30)
    })
    it("should get pin number correctly", async () => {
        const pinNumber = (await missionService.getPinNumberbyUserID(1))
        expect(pinNumber[0].unlock_pin).toBe(1234)
    })
    it("should get mission status id by mission status name correctly", async () => {
        const mission_status_id = (await missionService.getMissionStatusIDByMissionStatusName("complete"))
        expect(mission_status_id[0].id).toBe(2)
    })
    it("should change mission status by mission id", async () => {
        const mission_status_id = (await missionService.changeMissionStatusByMissionID(assigned_mission_id, change_mission_status_id))
        expect(mission_status_id[0]).toBe(change_mission_status_id)
    })
    it("should update current star correctly", async () => {
        const current_star = (await missionService.updateCurrentStar(10, 1))
        expect(current_star[0]).toBe(30 + 10)
    })
    it("should update total star correctly", async () => {
        const total_star = (await missionService.updateTotalStar(10, 1))
        expect(total_star[0]).toBe(50 + 10)
    })
    it("should get upcoming missions correctly", async () => {
        const upcomingMissions = (await missionService.getUpcomingMissions(1))
        expect(upcomingMissions.length).toBe(1)
    })
    it("should get all mission templates correctly", async () => {
        const mission_templates = (await missionService.getAllMissionTemplates(1))
        expect(mission_templates.length).toBe(2)
    })
    it("should add new upcoming mission correctly", async() => {
        const user_id = (await missionService.addNewUpcomingMission(1, mission_template_id, exp_date , 10, inprogress_missionID))
        expect(user_id[0]).toBe(1)
    })
    it("should add customized mission template correctly", async () => {
        const id = (await missionService.addCustomizedMissionTemplate("study", 1))
        expect(id.length).toBe(1)
    })
    it("should get all mission targets", async() => {
        const mission_targets = (await missionService.getAllMissionTargets(1))
        expect (mission_targets.length).toBe(1)
    })
    it("should add new mission target correctly", async () => {
        const id = (await missionService.addNewMissionTarget(1, "disneyland", 100))
        expect (id.length).toBe(1)
    })
    it("should redeem mission target correctly", async() => {
        const id = (await missionService.redeemMissionTarget(target_id))
        expect(id[0]).toBe(target_id)
    })
    it("should subtract star num correctly", async () => {
        const current_star = (await missionService.subtractStarNum(1, 5))
        expect(current_star[0]).toBe(40-5)
    })
    afterAll(async () => {
        await knex.destroy();
    });
})