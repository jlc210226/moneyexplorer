import { Knex } from "knex";
import { } from "../utils/models"
import { tables } from "../utils/tables";

export class KnowledgeService {
    constructor(private knex: Knex) { }

    async countCompletedQuizToday(userId: number) {
        //check if there are completed quiz today first
        const countCompletedQuizToday = await this.knex(tables.quizResultTable)
            .count("*")
            .where("created_at", ">", "today")
            .andWhere("user_id", userId)

        return countCompletedQuizToday
    }

    async getCompletedQuizzes(userId: number) {

        const completedQuizzes = await this.knex(tables.quizResultTable)
            .select("quiz_id", "quiz_name", "score")
            .leftJoin(tables.quizTable, "quiz_result.quiz_id", "quiz.id")
            .where('user_id', userId)
            .andWhere('is_active', true)
            .orderBy('quiz_result.created_at', 'desc')

        return completedQuizzes
    }

    async getQuizzes(quizIds: number[]) {

        const quizzes = await this.knex(tables.quizTable)
            .column({ quiz_id: "id" }, "quiz_name")
            .whereNotIn("id", quizIds)

        return quizzes
    }

    async getQuestionChoicesByquizId(quizId: number) {
        const choices = await this.knex(tables.questionTable)
            .innerJoin(tables.questionChoicesTable, `${tables.questionChoicesTable}.question_id`, `${tables.questionTable}.id`)
            .where("quiz_id", quizId)

        return choices
    }

    async getModelAnswers(quizId: number) {
        const modelAnswers = await this.knex(tables.questionTable)
            .innerJoin(tables.questionChoicesTable, "question.id", "question_choices.question_id")
            .where("question.quiz_id", quizId)
            .andWhere("question_choices.is_correct", true)

        return modelAnswers
    }

    async getUserResponse(quizId: number, userId: number) {
        const userResponse = await this.knex(tables.quizResponseTable)
            .innerJoin(tables.questionChoicesTable, "question_choices.id", "quiz_response.user_answer_id")
            .innerJoin(tables.questionTable, "question.id", "question_choices.question_id")
            .where("question.quiz_id", quizId)
            .where("quiz_response.user_id", userId)

        return userResponse
    }

    async submitResponse(userId: number, anwserIds: number[]) {

        const responseArray = []
        for (let anwserId of anwserIds) {
            const responseObj = {
                user_id: userId,
                user_answer_id: anwserId
            }
            responseArray.push(responseObj)
        }

        const submitedResponse = await this.knex(tables.quizResponseTable)
            .insert(responseArray)
            .returning('user_answer_id')

        return submitedResponse

    }

    async checkScore(anwserIds: number[]) {
        const checkScore = await this.knex(tables.questionChoicesTable)
            .whereIn('id', anwserIds)

        return checkScore

    }

    async submitQuizResult(user_id: number, quiz_id: number, score: number) {
        const submittedQuiz = await this.knex(tables.quizResultTable)
            .insert({
                user_id: user_id,
                quiz_id: quiz_id,
                score: score
            })
            .returning(['id', 'score'])

        return submittedQuiz
    }

    async quizStarRewards(user_id: number, score: number) {
        const quizStarRewards = await this.knex(tables.userProfilesTable)
            .where("user_id", user_id)
            .increment("current_star", score)
            .increment("total_star", score)

        return quizStarRewards
    }

    async getFinancialKnowledge() {
        const financialKnowledge = await this.knex(tables.knowledgeTable)
            .innerJoin("knowledge_category", "knowledge_category_id", "knowledge_category.id")
            .where("knowledge_category", "理財")
            .orderBy("created_at", "desc")

        return financialKnowledge
    }

    async getInvestmentKnowledge() {
        const investmentKnowledge = await this.knex(tables.knowledgeTable)
            .innerJoin("knowledge_category", "knowledge_category_id", "knowledge_category.id")
            .where("knowledge_category", "投資")
            .orderBy("created_at", "desc")

        return investmentKnowledge
    }
}