import { Request, Response, NextFunction } from "express";
import jwtsimple from 'jwt-simple'
import {Bearer} from 'permit'
import { userService } from "../main"

const permit = new Bearer({
    query: "access_token"
})

export async function isLoggedIn(req: Request, res: Response, next: NextFunction) {

    try {
        const token = permit.check(req);

        if (!token) {
            res.status(401).json({ message: 'Unauthorized' });
            return;
        }

        const jwtSecret = process.env.jwtSecret!

        const payload = jwtsimple.decode(token, jwtSecret);

        // if (Date.now() / 1000 >= payload.exp) {
        //     res.status(400).json({ message: "token expired" });
        //     return
        // }

        const user =  await userService.getUserById(payload.id);

        if (!user) {
            res.status(401).json({message: "Permission Denied"});
            return;
        }

        req.user = user;

        return next();

    } catch (err) {
        console.error(err);
        res.status(401).json({ msg: "Permission Denied" });
    }

}