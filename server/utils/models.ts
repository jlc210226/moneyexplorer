
export interface User {
    id: number;
    login_name: string;
    password: string;
    unlock_pin: number;
    created_at: Date;
    updated_at: Date;
    account_status: string;
}

export interface Avatar {
    id: number;
    filename: string;
}

export interface UserProfiles {
    id: number;
    nickname: string;
    gender: string;
    date_of_birth: Date;
    email: string;
    user_id: number;
    avatar_id: number;
    current_star: number;
    total_star: number;
    total_saving: number;
    created_at: Date;
    updated_at: Date;
}

export interface ExpenseCategory {
    id: number;
    expense_category: string;
}

export interface UserExpenses {
    id: number;
    user_id: number;
    expense_category_id: number;
    expense_amount: number;
    expense_date: Date;
    created_at: Date;
    updated_at: Date;
}

export interface IncomeCategory {
    id: number;
    income_category: string;
}

export interface UserIncomes {
    id: number;
    user_id: number;
    income_category_id: number;
    income_amount: number;
    income_date: Date;
    created_at: Date;
    updated_at: Date;
}

export interface UserSavings {
    id: number;
    user_id: number;
    saving_amount: number;
    saving_date: Date;
    created_at: Date;
    updated_at: Date;
}

export interface UserSavingGoals {
    id: number;
    user_id: number;
    goal_name: string;
    goal_amount: number;
    is_complete: boolean;
    created_at: Date;
    updated_at: Date;
}

export interface MissionTemplate {
    id: number;
    user_id: number;
    mission_name: string;
    mission_image: number;
    mission_interface: string;
    created_at: Date;
    updated_at: Date;
}

export interface MissionStatus {
    id: number;
    mission_status: string;
}

export interface AssignedMission {
    id: number;
    user_id: number;
    mission_id: number;
    mission_exp_date: Date;
    mission_status_id: number;
    reward_stars: number;
    created_at: Date;
    updated_at: Date;
}

export interface MissionTarget {
    id: number;
    user_id: number;
    target_name: string;
    needed_star: string;
    is_complete: boolean;
    created_at: Date;
    updated_at: Date;
}

export interface KnowledgeCategory {
    id: number;
    knowledge_category: string;
}

export interface Knowledge {
    id: number;
    knowledge_category_id: number;
    title: string;
    content: string;
    source: string;
    knowledge_image: string;
    url: string,
    created_at: Date;
    updated_at: Date;
}

export interface QuizCategory {
    id: number;
    quiz_category: string;

}

export interface Quiz {
    id: number;
    quiz_name: string;
    quiz_category_id: string;
    is_active: boolean;
    created_at: Date;
    updated_at: Date;
}

export interface Question {
    id: number;
    question: string;
    image: string;
    quiz_id: number;
    created_at: Date;
    updated_at: Date;
}

export interface QuestionChoices {
    id: number;
    question_id: number;
    choice: string;
    is_correct: boolean;
}

export interface QuizResponse {
    id: number;
    user_id: number;
    user_asnwer_id: number;
    created_at: Date;
    updated_at: Date;
}

export interface QuizResult {
    id: number;
    user_id: number;
    quiz_id: number;
    score: number;
    created_at: Date;
    updated_at: Date;
}

declare global{
    namespace Express{
        interface Request{
            user?: User
        }
    }
} 