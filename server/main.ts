import express from "express";
// import expressSession from "express-session";
import Knex from "knex";
import grant from "grant";
import dotenv from "dotenv";
import cors from 'cors';
import { Twilio } from 'twilio';



const app = express();
dotenv.config();

app.use(cors({
    origin: [process.env.FRONTEND_URL!, 'null', 'http://localhost:3000']
}))

// app.use(cors({
//     origin: process.env.FRONTEND_URL
// }))

// Initialize SendGrid Mail
// sgMail.setApiKey(process.env.SENDGRID_API_KEY!)


const twilioConfig = {
    TWILIO_ACCOUNT_SID: process.env.TWILIO_ACCOUNT_SID,
    TWILIO_AUTH_TOKEN: process.env.TWILIO_AUTH_TOKEN, 
}

// Initialize Twilio client
const getTwilioClient = () => {
  if (!twilioConfig.TWILIO_ACCOUNT_SID || !twilioConfig.TWILIO_AUTH_TOKEN) {
    throw new Error(`Unable to initialize Twilio client`);
  }
  return new Twilio(twilioConfig.TWILIO_ACCOUNT_SID, twilioConfig.TWILIO_AUTH_TOKEN)
}

export const twilioClient = getTwilioClient();

const grantExpress = grant.express({
    "defaults": {
        "origin": "http://localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile", "email"],
        "callback": "/login/google"
    }
});

const knexConfig = require("./knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

app.use(grantExpress as express.RequestHandler);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

import { UserService } from "./services/UserService";
import { UserController } from "./controllers/UserController";
import { SavingService } from "./services/SavingService";
import { SavingController } from "./controllers/SavingController";
import { MissionService } from "./services/MissionService";
import { MissionController } from "./controllers/MissionController";
import { KnowledgeService } from "./services/KnowledgeService";
import { KnowledgeController } from "./controllers/KnowledgeController";
import { SavingPlanService } from "./services/SavingPlanService"; './services/SavingPlanService'
import { SavingPlanController } from "./controllers/SavingPlanController"

// Create Service and Controller
export const userService = new UserService(knex);
export const userController = new UserController(userService);
const savingService = new SavingService(knex);
export const savingController = new SavingController(savingService);
const missionService = new MissionService(knex);
export const missionController = new MissionController(missionService);
const knowledgeService = new KnowledgeService(knex);
export const knowledgeController = new KnowledgeController(knowledgeService);
export const savingPlanService = new SavingPlanService(knex);
export const savingPlanController = new SavingPlanController(savingPlanService);

// API
import { routes } from "./routes";
import { isLoggedIn } from "./utils/guard";
import { publicRoutes } from "./publicRoutes";


const API_VERSION = process.env.API_VERSION ?? "/api/v1";
app.use(API_VERSION, publicRoutes);
app.use(API_VERSION, isLoggedIn, routes);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`[info] listening to port: [${PORT}]`);
});