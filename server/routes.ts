import express from "express";
import { missionRoutes } from "./routers/mission";
import { knowledgeRoutes } from "./routers/knowledge";
import { savingRoutes } from "./routers/saving";
import { userRoutes } from "./routers/user";
import { savingPlanRoute } from "./routers/savingPlans";

export const routes = express.Router();

routes.use("/", userRoutes);
routes.use("/", savingRoutes);
routes.use("/", missionRoutes)
routes.use("/", knowledgeRoutes)
routes.use("/", savingPlanRoute)
