import express from "express";
import { missionController } from "../main";

export const missionRoutes = express.Router();

missionRoutes.get('/loadCurrentStar', missionController.loadCurrentStar)
missionRoutes.post('/checkPin', missionController.checkpin)
missionRoutes.post('/changeMissionStatus', missionController.changeMissionStatus)
missionRoutes.get('/getUpcomingMissions', missionController.getUpcomingMissions)
missionRoutes.get('/getAllMissionTemplates', missionController.getAllMissionTemplates)
missionRoutes.post('/addNewUpcomingMission', missionController.addNewUpcomingMission)
missionRoutes.post('/addCustomizedMissions', missionController.addCustomizedMission)
missionRoutes.get('/getMissionTargets', missionController.getAllMissionTargets)
missionRoutes.post('/addNewMissionTarget', missionController.addNewMissionTarget)
missionRoutes.post('/redeemMissionTarget', missionController.redeemMissionTarget)