import { userController } from './../main';
import express from "express";
// import { isLoggedIn } from "../utils/guard"

export const userRoutes = express.Router();

// userRoutes.get('/currentUser', isLoggedIn, userController.getCurrentUser);
userRoutes.get('/currentUser', userController.getCurrentUser);
userRoutes.get('/getUserInfo', userController.getUserInfo);
userRoutes.put('/editProfile', userController.editProfile);
userRoutes.put('/editPassword', userController.editPassword);
userRoutes.put('/editPin', userController.editPin);
