import { savingPlanController } from './../main';
import express from "express";

export const savingPlanRoute = express.Router();

savingPlanRoute.get("/getSavingPlans", savingPlanController.getSavingPlans);
savingPlanRoute.get("/getTotalSaving", savingPlanController.getTotalSaving);
savingPlanRoute.put("/markGoalComplete", savingPlanController.markGoalComplete);
savingPlanRoute.put("/updateTotalSaving", savingPlanController.updateTotalSaving);
savingPlanRoute.post("/createSavingGoal", savingPlanController.createSavingGoal);
savingPlanRoute.get('/getCompletedSavingPlans', savingPlanController.getCompletedSavingPlans);
savingPlanRoute.put('/deleteGoal', savingPlanController.deleteGoal);
