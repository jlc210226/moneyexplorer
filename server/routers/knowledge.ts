import express from "express";
import { knowledgeController } from "../main";

export const knowledgeRoutes = express.Router();

knowledgeRoutes.get('/quiz', knowledgeController.getQuizzes);
knowledgeRoutes.get('/quiz/:quizId', knowledgeController.getQuestions);
knowledgeRoutes.post('/quiz/:quizId', knowledgeController.submitQuiz);
knowledgeRoutes.get('/revision/:quizId', knowledgeController.getRevisions);
knowledgeRoutes.get('/knowledge/investment', knowledgeController.getInvestmentKnowledge);
knowledgeRoutes.get('/knowledge/financial', knowledgeController.getFinancialKnowledge);

// knowledgeRoutes.post('/quiz', knowledgeController);