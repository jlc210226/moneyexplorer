import express from "express";
import { savingController } from "../main";

export const savingRoutes = express.Router();

savingRoutes.get('/totalincome', savingController.loadTotalIncomeByMonth);
savingRoutes.get('/totalexpense', savingController.loadTotalExpenseByMonth);
savingRoutes.get('/totalsaving', savingController.loadTotalSavingByMonth);
savingRoutes.post('/createincome', savingController.createIncome);
savingRoutes.post('/createexpense', savingController.createExpense);
savingRoutes.post('/createsaving', savingController.createSaving);
savingRoutes.get('/allfinanceitem', savingController.loadAllFinanceItem);
savingRoutes.delete('/deleteincome/:id', savingController.deleteIncome);
savingRoutes.delete('/deleteexpense/:id', savingController.deleteExpense);
savingRoutes.delete('/deletesaving/:id', savingController.deleteSaving);