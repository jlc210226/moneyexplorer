import { Knex } from "knex";
import path from "path";
import xlsx from "xlsx";
import { tables } from "../utils/tables";
import * as tableContentType from "../utils/models";
import { hashPassword } from "../utils/hash";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries

    // await knex(tables.quizResultTable).del();
    // await knex(tables.quizResponseTable).del();
    // await knex(tables.questionChoicesTable).del();
    // await knex(tables.questionTable).del();
    // await knex(tables.quizTable).del();
    // await knex(tables.quizCategoryTable).del();
    // await knex(tables.knowledgeTable).del();
    // await knex(tables.knowledgeCategoryTable).del();

    await knex(tables.missionTargetTable).del();
    await knex(tables.assignedMissionTable).del();
    await knex(tables.missionStatusTable).del();
    await knex(tables.missionTemplateTable).del();
    await knex(tables.userSavingGoalTable).del();
    await knex(tables.userSavingTable).del();
    await knex(tables.userIncomesTable).del();
    await knex(tables.incomeCategoryTable).del();
    await knex(tables.userExpensesTable).del();
    await knex(tables.expenseCategoryTable).del();
    await knex(tables.userProfilesTable).del();
    await knex(tables.avatarTable).del();
    
    //quiz
    await knex(tables.quizResponseTable).del();
    await knex(tables.questionChoicesTable).del();
    await knex(tables.questionTable).del();
    await knex(tables.quizResultTable).del();
    await knex(tables.quizTable).del();
    await knex(tables.quizCategoryTable).del();
    await knex(tables.knowledgeTable).del();
    await knex(tables.knowledgeCategoryTable).del();

    await knex(tables.usersTable).del();

    // Read workbook

    const workbook = xlsx.readFile(
        path.join(__dirname, "init_data.xlsx")
    )

    // Read workbook
    const workbookQuiz = xlsx.readFile(
        path.join(__dirname, "init_data_quiz.xlsx")
    )

    // Read workbook sheets

    const users: tableContentType.User[] = xlsx.utils.sheet_to_json(workbook.Sheets["users"],{ raw: false });
    const avatars: tableContentType.Avatar[] = xlsx.utils.sheet_to_json(workbook.Sheets["avatar"]);
    const usersProfiles: Array<{ login_name: string, nickname: string, gender: string, date_of_birth: Date, email: string, user_id: number, avatar_filename: string, current_star: number, total_star: number, total_saving: number, created_at: Date, updated_at: Date }> = xlsx.utils.sheet_to_json(workbook.Sheets["users_profiles"], { raw: false });
    const expenseCategory: tableContentType.ExpenseCategory[] = xlsx.utils.sheet_to_json(workbook.Sheets["expense_category"]);
    const userExpenses: Array<{ login_name: string, expense_category: string, expense_amount: number; expense_date: Date, created_at: Date, updated_at: Date }> = xlsx.utils.sheet_to_json(workbook.Sheets["user_expenses"], { raw: false });
    const incomeCategory: tableContentType.IncomeCategory[] = xlsx.utils.sheet_to_json(workbook.Sheets["income_category"]);
    const userIncomes: Array<{ login_name: string, income_category: string, income_amount: number; income_date: Date, created_at: Date, updated_at: Date }> = xlsx.utils.sheet_to_json(workbook.Sheets["user_incomes"], { raw: false });
    const userSavings: Array<{ login_name: string, saving_amount: number, saving_date: Date, created_at: Date, updated_at: Date }> = xlsx.utils.sheet_to_json(workbook.Sheets["user_saving"], { raw: false });
    const userSavingGoals: Array<{ login_name: string, goal_name: string, goal_amount: number, is_complete: boolean, created_at: Date, updated_at: Date }> = xlsx.utils.sheet_to_json(workbook.Sheets["user_saving_goals"], { raw: false });
    const missionTemplates: Array<{ login_name: string, mission_name: string, mission_image: number, mission_type: string, created_at: Date, updated_at: Date }> = xlsx.utils.sheet_to_json(workbook.Sheets["mission_template"], { raw: false });
    const missionStatus: tableContentType.MissionStatus[] = xlsx.utils.sheet_to_json(workbook.Sheets["mission_status"]);
    const assignMissions: Array<{ login_name: string, mission_name: string, mission_exp_date: Date, mission_status: string, reward_stars: number, created_at: Date, updated_at: Date }> = xlsx.utils.sheet_to_json(workbook.Sheets["assigned_mission"], { raw: false });
    const missionTargets: Array<{ login_name: string, target_name: string, needed_star: number, is_complete: boolean, created_at: Date, updated_at: Date }> = xlsx.utils.sheet_to_json(workbook.Sheets["mission_target"], { raw: false });

    const knowledge: Array<{ knowledge_category: string, content: string, source: string, knowledge_image: string, url: string, title: string }> = xlsx.utils.sheet_to_json(workbookQuiz.Sheets["knowledge"]);
    const knowledgeCategory: Array<tableContentType.KnowledgeCategory> = xlsx.utils.sheet_to_json(workbookQuiz.Sheets["knowledge_category"]);
    const quiz: Array<{ quiz_name: string, quiz_category: string, is_active: boolean, quiz_key: number }> = xlsx.utils.sheet_to_json(workbookQuiz.Sheets["quiz"]);
    const quizCategory: Array<tableContentType.QuizCategory> = xlsx.utils.sheet_to_json(workbookQuiz.Sheets["quiz_category"]);
    const question: Array<{ question: string, image: string, quiz_key: number, question_key: number }> = xlsx.utils.sheet_to_json(workbookQuiz.Sheets["question"]);
    const questionChoices: Array<{ question_key: number, question_choice_key: number , is_correct: boolean, choice: string }> = xlsx.utils.sheet_to_json(workbookQuiz.Sheets["question_choices"]);
    const quizResponse: Array<{login_name:string, question_choice_key: number}> = xlsx.utils.sheet_to_json(workbookQuiz.Sheets["quiz_response"]);
    const quizResult: Array<{ login_name:string, quiz_key: number, score: number }> = xlsx.utils.sheet_to_json(workbookQuiz.Sheets["quiz_result"]);

    const trx = await knex.transaction();

    try {

        for (const row of users) {
            row.password = await hashPassword(row.password);
        }

        const seededUsers = await trx(tables.usersTable).insert(users).returning(["id", "login_name"]) as Array<{ id: number, login_name: string }>;

        const seededAvatar = await trx(tables.avatarTable).insert(avatars).returning(["id", "avatar_filename"]) as Array<{ id: number, avatar_filename: string }>;

        const seededExpenseCategory = await trx(tables.expenseCategoryTable).insert(expenseCategory).returning(["id", "expense_category"]) as Array<{ id: number, expense_category: string }>;

        const seededIncomeCatergory = await trx(tables.incomeCategoryTable).insert(incomeCategory).returning(["id", "income_category"]) as Array<{ id: number, income_category: string }>;

        const seededMissionStatus = await trx(tables.missionStatusTable).insert(missionStatus).returning(["id", "mission_status"]) as Array<{ id: number, mission_status: string }>;

        const userMapping = seededUsers.reduce(
            (acc, user) => acc.set(user.login_name, user.id),
            new Map<string, number>()
        );

        const avatarMapping = seededAvatar.reduce(
            (acc, avatar) => acc.set(avatar.avatar_filename, avatar.id),
            new Map<string, number>()
        );

        const expenseCatMapping = seededExpenseCategory.reduce(
            (acc, expenseCat) => acc.set(expenseCat.expense_category, expenseCat.id),
            new Map<string, number>()
        );

        const incomeCatMapping = seededIncomeCatergory.reduce(
            (acc, incomeCat) => acc.set(incomeCat.income_category, incomeCat.id),
            new Map<string, number>()
        );

        const missionStatusMapping = seededMissionStatus.reduce(
            (acc, status) => acc.set(status.mission_status, status.id),
            new Map<string, number>()
        )

        // Map & Insert User Profile Data

        const usersProfilesData = usersProfiles.map((userProfile) => {
            return {
                nickname: userProfile["nickname"],
                gender: userProfile["gender"],
                date_of_birth: userProfile["date_of_birth"],
                email: userProfile["email"],
                user_id: userMapping.get(userProfile["login_name"]),
                avatar_id: avatarMapping.get(userProfile["avatar_filename"]),
                current_star: userProfile["current_star"],
                total_star: userProfile["total_star"],
                total_saving: userProfile["total_saving"]
            }
        });

        await trx(tables.userProfilesTable).insert(usersProfilesData);

        // Map & Insert User Expenses Data

        const userExpensesData = userExpenses.map((userExpense) => {
            return {
                user_id: userMapping.get(userExpense["login_name"]),
                expense_category_id: expenseCatMapping.get(userExpense["expense_category"]),
                expense_amount: userExpense["expense_amount"],
                expense_date: userExpense["expense_date"]
            }
        });

        await trx(tables.userExpensesTable).insert(userExpensesData);

        // Map & Insert User Incomes Data

        const userIncomesData = userIncomes.map((userIncome) => {
            return {
                user_id: userMapping.get(userIncome["login_name"]),
                income_category_id: incomeCatMapping.get(userIncome["income_category"]),
                income_amount: userIncome["income_amount"],
                income_date: userIncome["income_date"]
            }
        });

        await trx(tables.userIncomesTable).insert(userIncomesData);

        // Map & Insert User Saving

        const userSavingsData = userSavings.map((userSaving) => {
            return {
                user_id: userMapping.get(userSaving["login_name"]),
                saving_amount: userSaving["saving_amount"],
                saving_date: userSaving["saving_date"]
            }
        });

        await trx(tables.userSavingTable).insert(userSavingsData);

        // Map & Insert User Saving Goals

        const userSavingGoalsData = userSavingGoals.map((userSavingGoal) => {
            return {
                user_id: userMapping.get(userSavingGoal["login_name"]),
                goal_name: userSavingGoal["goal_name"],
                goal_amount: userSavingGoal["goal_amount"],
                is_complete: userSavingGoal["is_complete"]
            }
        });

        await trx(tables.userSavingGoalTable).insert(userSavingGoalsData);

        // Map & Insert Mission Template

        const missionTemplateData = missionTemplates.map((missionTempate) => {
            return {
                user_id: userMapping.get(missionTempate["login_name"]),
                mission_name: missionTempate["mission_name"],
                mission_image: missionTempate["mission_image"],
                mission_type: missionTempate["mission_type"],
            }
        });

        const seededMissionTemplate = await trx(tables.missionTemplateTable).insert(missionTemplateData).returning(["id", "mission_name"]) as Array<{ id: number, mission_name: string }>;

        const missionTemplateMapping = seededMissionTemplate.reduce(
            (acc, template) => acc.set(template.mission_name, template.id),
            new Map<string, number>()
        );

        // Map & Insert Assigned Mission

        const assignedMissionData = assignMissions.map((assignedMission) => {
            return {
                user_id: userMapping.get(assignedMission["login_name"]),
                mission_id: missionTemplateMapping.get(assignedMission["mission_name"]),
                mission_exp_date: assignedMission["mission_exp_date"],
                mission_status_id: missionStatusMapping.get(assignedMission["mission_status"]),
                reward_stars: assignedMission["reward_stars"],
            }
        });

        await trx(tables.assignedMissionTable).insert(assignedMissionData);

        // Map & Insert Mission Target

        const missionTargetData = missionTargets.map((missionTarget) => {
            return {
                user_id: userMapping.get(missionTarget["login_name"]),
                target_name: missionTarget["target_name"],
                needed_star: missionTarget["needed_star"],
                is_complete: missionTarget["is_complete"],
            }
        })

        await trx(tables.missionTargetTable).insert(missionTargetData)


        //Quiz

        const seededKnowledgeCategory = await trx(tables.knowledgeCategoryTable).insert(knowledgeCategory).returning(["id", "knowledge_category"]) as Array<{ id: number, knowledge_category: string }>;

        const knowledgeCatMapping = seededKnowledgeCategory.reduce(
            (acc, knowledgeCat) => acc.set(knowledgeCat.knowledge_category, knowledgeCat.id),
            new Map<string, number>()
        )

        const knowledgeData = knowledge.map((knowledgeItem) => {
            return {
                knowledge_category_id: knowledgeCatMapping.get(knowledgeItem.knowledge_category),
                content: knowledgeItem.content,
                source: knowledgeItem.source,
                knowledge_image: knowledgeItem.knowledge_image,
                url: knowledgeItem.url,
                title: knowledgeItem.title
            }
        })

        await trx(tables.knowledgeTable).insert(knowledgeData);

        const seededQuizCategory = await trx(tables.quizCategoryTable).insert(quizCategory).returning(["id", "quiz_category"]) as Array<{ id: number, quiz_category: string }>;

        const quizCatMapping = seededQuizCategory.reduce(
            (acc, quizCat) => acc.set(quizCat.quiz_category, quizCat.id),
            new Map<string, number>()
        )

        const quizData = quiz.map((quizItem) => {
            return {
                quiz_name: quizItem.quiz_name,
                quiz_category_id: quizCatMapping.get(quizItem.quiz_category),
                is_active: quizItem.is_active
            }
        })

        const seededQuiz = await trx(tables.quizTable).insert(quizData).returning(["id"]) as Array<{ id: number }>;

        //mapping the quiz_id by quiz_key
        const quizIdKeyMapping = seededQuiz.reduce(
            (acc, quizItem, i) => acc.set(quiz[i].quiz_key, quizItem.id),
            new Map<number, number>()
        )

        const questionData = question.map((questionItem) => {
            return {
                question: questionItem.question,
                image: questionItem.image,
                quiz_id: quizIdKeyMapping.get(questionItem.quiz_key)
            }
        })

        const seededQuestion = await trx(tables.questionTable).insert(questionData).returning(["id"]) as Array<{ id: number }>;

        //mapping the question_id by question_key
        const questionIdKeyMapping = seededQuestion.reduce(
            (acc, questionItem, i) => acc.set(question[i].question_key, questionItem.id),
            new Map<number, number>()
        )

        const questionChoicesData = questionChoices.map((questionChoicesItem)=>{
            return {
                choice: questionChoicesItem.choice,
                is_correct: questionChoicesItem.is_correct,
                question_id: questionIdKeyMapping.get(questionChoicesItem.question_key),
            }
        })

        const seededQuestionChoices = await trx(tables.questionChoicesTable).insert(questionChoicesData).returning(["id"]) as Array<{ id: number }>;

        //mapping the question_choices_id by question_choice_key
        const questionChoicesIdKeyObj = {};

        for (let i=0; i < seededQuestionChoices.length ; i++){
            const id = seededQuestionChoices[i].id;
            const questionChoicesKey = questionChoices[i].question_choice_key;
            questionChoicesIdKeyObj[questionChoicesKey.toString()] = id
        }

        // const usernameIdMapping = usernameId.reduce(
        //     (acc, usernameIdItem)=> acc.set(usernameIdItem.login_name, usernameIdItem.id),
        //     new Map<string, number>()
        // )

        // console.log(usernameId)

        const quizResponseData = quizResponse.map((quizResponseItem, i)=>{
            return {
                user_id: userMapping.get(quizResponseItem.login_name),
                user_answer_id: questionChoicesIdKeyObj[quizResponseItem.question_choice_key],
            }
        })

        await trx(tables.quizResponseTable).insert(quizResponseData)
       
        const quizResultData = quizResult.map((quizResultitem)=>{
            return {
                user_id: userMapping.get(quizResultitem.login_name),
                quiz_id: quizIdKeyMapping.get(quizResultitem.quiz_key),
                score: quizResultitem.score
            }
        })

        await trx(tables.quizResultTable).insert(quizResultData)

        await trx.commit();

    } catch (err) {
        console.log("ERROR " + err.message);
        await trx.rollback();
    }

};
