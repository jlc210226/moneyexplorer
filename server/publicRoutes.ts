import express from "express";
import { userRoutes } from "./publicRouters/user"

export const publicRoutes = express.Router();

publicRoutes.use("/", userRoutes)

